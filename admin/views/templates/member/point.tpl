{include file="head.tpl"}
<script type="text/javascript">
{literal}


{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>ユーザー</h3>

<h4>{$memArr.name}さんのポイント履歴</h4>
{include file="messages.tpl"}

		←<a href="/member/list">ユーザー一覧へ</a>

		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>


			<table class="admins_m clear">
			<tr>
				<th width="80">ポイント種別</th>
				<th width="50">ポイント</th>
				<th width="50">日付</th>
				<th width="50">没収</th>
			</tr>
			{foreach from=$arr item="item"}
				<tr>
					<td>
						{assign var="id" value=$item.point_flg}
						{$pointflgArr.$id}
					</td>
					<td>{$item.point}pt</td>
					<td>{$item.insert_date}</td>
					<td>
					{if $item.point_flg<4}
					<form name="fm_list" id="fm_list" method="POST" action="member/point/">
						<input type="hidden" name="member_id" value="{$memArr.member_id}" />
						<input type="hidden" name="no" value="{$item.no}" />
						<input type="hidden" name="point" value="{$item.point}" />
						<input type="hidden" name="page" value="{$curPage}" />
						<input type="submit" name="del_point" value="ポイント没収" onclick="return confirm('このポイントを没収します。良いですか？');">
					</form>
					{/if}
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="3">ポイント履歴はありません。</td>
				</tr>
			{/foreach}
			</table>


			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

