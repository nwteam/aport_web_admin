{include file="head.tpl"}
<script type="text/javascript">
{literal}

function editcancel()
{
	document.regForm.action="master/category2/";
	document.regForm.submit();
}

{/literal}
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>業種管理</h3>


{if $exec=="middle"}
	<h2>中カテゴリー追加</h2>

{elseif $exec=="sub"}
	<h2>小カテゴリー追加</h2>

{elseif $exec=="main"}
	<h2>メインカテゴリ追加</h2>
{else}
	<h2>カテゴリ編集</h1>
{/if}

{if $msg }

	{if $exec=="middle"}
		中カテゴリを追加しました。
	{elseif $exec=="sub"}
		小カテゴリを追加しました。
	{elseif $exec=="main"}
		大カテゴリを追加しました。
	{else}
		カテゴリを編集しました。
	{/if}

{else}
<form name="regForm" method="post" action="master/category2edit">
<input type="hidden" name="ctitle" value="{$ctitle}" />
<input type="hidden" name="id" value="{$id}" />
<input type="hidden" name="parentid" value="{$parentid}" />
<input type="hidden" name="exec" value="{$exec}" />
<input type="hidden" name="cflag" value="{$cflag}" />
<table class="admin_list01">
</td>
</tr>
<tr>
<th>※カテゴリ名</th>
{if $errmsg==1}
<td class="red">
カテゴリは必須項目です。<br />
{else}
<td>
{/if}
<input type="text" name="name" value="{$name}" style="ime-mode:active;width:350px" />
</td>
</tr>

</table>
<p class="center">
	<input type="submit" name="regist" value="OK" />
	<input name="cancel" type="button" value="キャンセル" onclick="editcancel();" />
</p>
</form>

{/if}
{include file="footer.tpl"}
</body>
</html>
