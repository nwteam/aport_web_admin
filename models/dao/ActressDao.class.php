<?php
class ActressDAO extends DAOBase {

	/**
	 * 起案者情報取得
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function getActressData($actress_no, $langcode=MAIN_LANGUAGE) {

		if($langcode){
			$lang=" and d.lang='".addslashes($langcode)."' ";
		}

		$sql="select a.*,d.* ,e.email as mem_email from sf_actress a 
			 left join sf_actress_detail d on a.actress_no = d.actress_no 
			 left join sf_member e on a.actress_no = e.user_id 
			 and d.lang='".addslashes($langcode)."'
			 where a.actress_no=".addslashes($actress_no);

	//	print $sql."<br>";
		$tmp=$this->executeQuery($sql);

		$Arr=array();
		foreach ($tmp[0] as $key => $val){
			$data{$key} = stripslashes(rtrim($val));
		}

		return $data;
	}

	/**
	 * 起案者情報一覧取得
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function getActressList($where="", $limit="", $order="") {
		if ($order == "") $order = " ORDER BY a.create_date ASC ";
		$sql = "SELECT
		            a.*
				FROM
					sf_actress  a
				{$where}
				{$order}
				{$limit}
			 ";

		$actress_src = $this->executeQuery($sql);
		return $actress_src;
	}

	/**
	 * 会員登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$fi2,$dt2,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into sf_actress($ins) values($valu)";

		$this->db->beginTransaction();
		try {
		//	print $sql."<br>";
			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();

			//起案者詳細の登録
			$tmp1=array();
			$tmp2=array();

			$fi2[]="actress_no";
			$dt2[]=$lastInsertId;
			if(is_array($fi2)){
		           	for ($i=0;$i<count($fi2);$i++){
				    	$tmp1[]=$fi2[$i];
						$va=trim($dt2[$i]);
						$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

			    	}
			}else if($fi2){
			    	$tmp1[]=$fi2;
				$dt2=trim($dt2);
				$dt2=htmlspecialchars($dt2, ENT_QUOTES);
				$tmp2[]="'".$dt2."'";
			}
			$ins=implode(",",$tmp1);
			$valu=implode(",",$tmp2);

			$sql="insert into sf_actress_detail($ins) values($valu)";
		//	print $sql."<br>";
			// 実行
			$this->executeUpdate($sql);

			//画像の登録
			if($item[new_profile_img]){

				$ikey[]="image_type";
				$ival[]="PROFILE_IMG";
				$ikey[]="parent_no";
				$ival[]=$lastInsertId;
				$ikey[]="parent_type";
				$ival[]="actress";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_ACTRESS";
				$ikey[]="file_name";
				$ival[]=$lastInsertId."_pf_".$item[new_profile_img];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

			}

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'sf_actress'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;

	}
	/**
	 * 会員 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$fi2,$dt2,$wfi,$wdt,$wfi2,$wdt2,$item=array()) {
		$commonDao = new CommonDAO();

		//基本データ
		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_actress set $ins $where";

		//詳細データ
		$tmp1=array();
		$tmp2=array();
		if(is_array($fi2)){
	       for ($i=0;$i<count($fi2);$i++){
					$va=trim($dt2[$i]);
					$tmp2[]=$fi2[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi2){
			$dt2=trim($dt2);
			$dt2=htmlspecialchars(addslashes($dt2));
			$tmp2[]=$fi2."='".$dt2."'";
		}

		$ins=implode(",",$tmp2);

		if(is_array($wfi2)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi2);$i++){
		    	$tmp[]=$wfi2[$i]."='".$wdt2[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi2){
			$where=" where ".$wfi2."='".$wdt2."'";
		}

		$sql2="update sf_actress_detail set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
//			print $sql."<br>";
			$this->executeUpdate($sql);	//基本情報
			$this->executeUpdate($sql2);	//詳細情報

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Actress'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 人材検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="",$langcode="") {

		list($where,$tbl)=$this->makeSearch($search);

		if($langcode){
			$lang=" and d.lang='".addslashes($langcode)."' ";
		}

		$sql="select count(*) as cnt from sf_actress a
			left join sf_actress_detail d on a.actress_no = d.actress_no"
			.$lang.$tbl.$where;

		//print $sql."<br>";
		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//
		if($search["member_id"]){
			$whTmp[]="member_id =".addslashes($search["member_id"]);
		}
		//email
		if($search["email"]){
			$whTmp[]="email ='".addslashes($search["email"])."'";
		}
		//pass
		if($search["password"]){
			$whTmp[]="password ='".addslashes(to_hash($search["password"]))."'";
		}
		//名前
		if($search["name"]){
			$tmp[]="name like '%".addslashes($search["name"])."%'";
			$tmp[]="public_name like '%".addslashes($search["name"])."%'";

			$whTmp[]="(".implode(" or ",$tmp).")";


		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		return array($where,$tbl);

	}

	/**
	 * 会員検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="",$langcode="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {

			if(!$search["page"]) $search["page"]=1;

			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}

		//言語コード
		if($langcode){
			$lang=" and d.lang='".addslashes($langcode)."' ";
		}


		$sql="select a.*,d.* ,a.create_date as reg_date from sf_actress as a
			left join sf_actress_detail as d on a.actress_no = d.actress_no"
		.$lang.$where.$ord.$limit_str;

		//print $sql."<br>";

		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;

	}



	/**
	 * Actressの削除
	 */
	public function delete($actress_no) {
		$commonDao = new CommonDAO();

		$this->db->beginTransaction();
		try{

			$sql = "DELETE FROM sf_actress WHERE actress_no = {$actress_no}";
			// 実行
			$this->executeUpdate($sql);

			//関連のあるものすべて削除
			//多言語情報の削除
			$sql = "DELETE FROM sf_actress_detail WHERE actress_no = {$actress_no}";
			// 実行
			$this->executeUpdate($sql);


			// 画像の削除

			$tmp=$commonDao->get_data_tbl("general_images",array("parent_type","parent_no"),array("actress",$actress_no));

			for($i=0;$i<count($tmp);$i++){
				unlink(constant($tmp[$i][dir]).$tmp[$i][file_name]);
			}
			$sql = "DELETE FROM general_images WHERE parent_type ='actress' and parent_no = {$actress_no}";
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MSG_LEVEL_ERROR, "Failed to delete 'Actress'. " . $e);
			$this->db->rollBack();
			throw new Exception($e);
			return false;
		}
		return true;
	}

	/**
	 * 新規起案者noの取得

	 */

	public function getNewActressNo() {
		$sql = "SELECT
		            max(actress_no) as last_no
				FROM
					sf_actress
			 ";
		//print $sql."<br>";
		$ret = $this->executeQuery($sql);
		if($ret){
			$new_no=$ret[0][last_no]+1;
		}else{
			$new_no=1;
		}
		return $new_no;
	}

	/**
	 * 起案者 プルダウン用の配列取得

	 */

	public function getActressOptionArr() {
		$sql = "SELECT
		            actress_no,public_name
				FROM
					sf_actress
				WHERE status=1 and del_flg=0
			 ";
		//print $sql."<br>";

		$ret = $this->executeQuery($sql);

		$Arr[""]="選択してください";
		if($ret){
			foreach($ret as $data){
				$Arr[$data[actress_no]]=$data[public_name];
			}
		}
		return $Arr;
	}


}
?>