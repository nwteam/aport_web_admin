<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="ja">
<head>
<base href="{$smarty.const.ROOT_URL_ADMIN}" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="content-language" content="ja" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-script-type" content="text/javascript" />
<title>{$smarty.const.STR_SITE_TITLE}</title>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jMenu.jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.tablefix_1.0.1.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js"></script>

<!-- timepicker slider -->
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-ja.js"></script>
<script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>


<!-- HTML エディタ -->
<script type="text/javascript" src="js/nicEdit.js"></script>


<!--
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="js/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>

<script type="text/javascript" src="js/jquery.ui.datetimepicker.js"></script>
<script type="text/javascript" src="js/jquery.ui.datetimepicker-jp.js"></script>
<script type="text/javascript" src="js/jquery.timepicker.js"></script>
-->
<!--
<script type="text/javascript" src="js/jquery-upload.js"></script>
<script type="text/javascript" src="libs/CLEditor1_3_0/jquery.cleditor.min.js"></script>
<script type="text/javascript" src="libs/CLEditor1_3_0/jquery.cleditor.table.min.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
  -->
<link href='http://fonts.googleapis.com/css?family=Roboto|Ubuntu' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />

</head>