{* メッセージ表示 *}
<div class="header_msg_base">
{foreach from=$msg_list|smarty:nodefaults item="msg" name="loop"}
	{if $msg->getMessageLevel()=="info"}
	<div class="header_msg_{$msg->getMessageLevel()}">
		<div>・{$msg->getMessageBody()}</div>
	</div>
	{/if}
{/foreach}
</div>
