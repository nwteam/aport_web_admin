{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#s_name").val("");
	$("#s_email").val("");
}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>問い合わせ一覧</h4>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="inquiry/list">
		<table class="search">
		<tr>
			<th>名前：</th>
			<td>
				<input type="text" name="s_name" id="s_name"  value="{$search.s_name}" size="40" />
			</td>
		</tr>
		<tr>
			<th>メールアドレス：</th>
			<td>
				<input type="text" name="s_email" id="s_email"  value="{$search.s_email}" size="40" />
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>

		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="inquiry/list/">

			<table class="admins clear">
			<tr>
				<th width="20">No</th>
				<th width="120">名前</th>
				<th width="60">メールアドレス</th>
				<th width="200">問い合わせ内容</th>
				<th width="60">登録日</th>
			</tr>
			{foreach from=$inquiry_list item="inquiry"}
				<tr>
					<td>{$inquiry.no}</a></td>
					<td>{$inquiry.name}</td>
					<td>{$inquiry.email}</td>
					<td>{$inquiry.message|nl2br}</td>
					<td>{$inquiry.create_date|date_format:"%Y/%m/%d %H:%M:%S"}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="12">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

