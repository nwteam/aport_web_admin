{include file="head.tpl"}

<script type="text/javascript">
{literal}

$(function(){
	$("#start").datepicker({
		dateFormat: "yy/mm/dd"
	});


});



{/literal}
</script>

<body>

<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>お知らせ詳細</h3>
←<a href="/news/list/">お知らせ管理に戻る</a>
<br /><br />
		<font color="red">{$msg}</font>

		<form id="fm" name="fm" method="post" enctype="multipart/form-data" >

	{if count($result_messages) >0}
	<div><font color="red">入力エラーがあります</font></div><br />
	{/if}
		<table class="detail">
		<tr>
			<th>お知らせタイトル</th>
			<td>
						{if $result_messages.title!=""}
							<font color="red">{$result_messages.title}<br /></font>
						{/if}
						<input class="req" type="text" name="title" value="{$input_data.title}" size="50">
			</td>
		</tr>
		<tr>
			<th>内容</th>
			<td>
						{if $result_messages.detail!=""}
							<font color="red">{$result_messages.detail}<br /></font>
						{/if}
						<textarea class="reqNoT" name="detail" rows="10" cols="60">{$input_data.detail}</textarea>
			</td>
		</tr>
		<tr>
			<th>投稿日設定</th>
			<td>
						{if $result_messages.news_date!=""}
							<font color="red">{$result_messages.news_date}<br /></font>
						{/if}
				<input type="text" id="start" name="news_date" size="25" value="{$input_data.news_date|date_format:'%Y/%m/%d'}" style="ime-mode:disabled;"/>
			</td>
		</tr>
		<tr>
			<th>
				※表示/非表示
			</th>
			<td>
				<input type="radio" name="display_flg" value="1"{if $input_data.display_flg=="1"} checked="checked"{/if}/>表示
				&nbsp;<input type="radio" name="display_flg" value="0"{if $input_data.display_flg=="0"} checked="checked"{/if}/>非表示

			</td>
		</tr>

		</table>
		<div>
			<input type="submit" name="regist"  value=" 登 録 ">&nbsp;
			<input type="reset" name="submit" value="リセット">&nbsp;
			<input type="hidden" name="news_no" value="{$input_data.news_no}">
		</div>
		</form>
		<br />


	</div>

</div>

{include file="footer.tpl"}
</body>
</html>

