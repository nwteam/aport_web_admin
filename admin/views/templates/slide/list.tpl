{include file="head.tpl"}
<script type="text/javascript">
{literal}


//編集
function edit( slide_no )
{
	document.regForm.slide_no.value = slide_no;
	document.regForm.submit();
}
function del( slide_no )
{
	if( !confirm( "指定のデータを削除します。\r\nよろしいですか？" ) )
	{
		return false;
	}
	document.regForm.action = "slide/list/";
	document.regForm.slide_no.value = slide_no;
	document.regForm.exec.value = "delete";
	document.regForm.submit();
}
function mainup( slide_no, val )
{

	document.regForm.action = "slide/list/";
	document.regForm.slide_no.value = slide_no;
	document.regForm.value.value = val;
	document.regForm.exec.value = "mainup";
	document.regForm.submit();

}
function maindown( slide_no, val )
{

	document.regForm.action = "slide/list/";
	document.regForm.slide_no.value = slide_no;
	document.regForm.value.value = val;
	document.regForm.exec.value = "maindown";
	document.regForm.submit();
}
function addmain()
{
	document.regForm.action = "slide/edit/";
	document.regForm.exec.value = "main";
	document.regForm.submit();
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>トップページ表示順管理

		</h3>

{if $msg }
<div><font color="red">{$msg}</font></div><br />
{/if}
<form action="slide/edit" name="regForm" method="post">
<input type="button" value="表示プロジェクト追加" onclick="addmain();" />
<input type="hidden" name="slide_no" />
<input type="hidden" name="value" />
<input type="hidden" name="exec" />

<!-- 一覧 -->
<table>
	<tr><th>順番</th><th>画像</th><th>プロジェクト</th><th>上</th><th>下</th><th>Newマーク</th><th></th><th></th></tr>
{foreach from=$cateArr item="item"}

<tr>
<td>
{$item.v_order}
</td>
<td>
	<img src="{$smarty.const.URL_IMG_PROJECT}{$item.cover_img}" widht=145 height=105>
</td>
<td>
	{$item.link}:{$item.project_title}
</td>
<td>
{if $item.mainup==1}
<a href="javascript:void( 0 );" onclick="mainup({$item.slide_no}, {$item.v_order} )">▲</a>
{/if}
</td>
<td>
{if $item.maindown==1}
<a href="javascript:void( 0 );" onclick="maindown({$item.slide_no}, {$item.v_order} )">▼</a>
{/if}
</td>
<td>
{if $item.view_flg==1}New{/if}
</td>

</td>
<td><input type="button" value="変更" onclick="edit({$item.slide_no}, {$item.v_order});"></td>
<td>
{if $item.subflag!=1}
<input type="button" value="削除" onclick="del({$item.slide_no}, {$item.v_order});">
{/if}
</td>

</tr>
{/foreach}
</table>

<!-- 一覧 -->
<input type="button" value="表示プロジェクト追加" onclick="addmain();" />
</form>

{include file="footer.tpl"}
</body>
</html>
