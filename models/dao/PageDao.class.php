<?php
class PageDAO extends DAOBase {

	/**
	 * FBページ登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$item=array()) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into fbpage($ins) values($valu)";
	//	print $sql."<br>";

		$this->db->beginTransaction();
		try {

			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'fbpage'." . $e);
			$this->db->rollBack();
			return false;
		}

		// return $lastInsertId;;
		return true;

	}


		/**
	 * 検索
	 * @param $search:検索条件
	 * 　　（$search[condition] = condition; こんな感じ)
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="") {

		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {
			 if(!$search["page"]) $search["page"]=1;
			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}

// ここ改修。とりあえず保留。
		// generate SQL
		// $sql="SELECT distinct * FROM fbpage where stat='1' " .  $limit_str ;
		// $sql="SELECT distinct f.* FROM fbpage as f , owner as o ".$tbl." where f.owner_id=o.owner_id ".$where.$ord.$limit_str;

		if(count($search[like])==1){
			if($search[like][0]==1){	//いいね済のページを抽出
				$sql="SELECT distinct f.* FROM fbpage as f
						inner join
							(select r.fbpage_id as fb_id from point_record as r
								where member_id ='" .$search[member_id]."' and stat >=0 ) as l
						on f.fbpage_id=l.fb_id "
					  . $where . $ord . $limit_str;
			}else{	//未いいねのページを抽出
				$sql="SELECT distinct f.* FROM fbpage as f
						left join
							(select r.fbpage_id as fb_id from point_record as r
								where member_id ='" .$search[member_id]."' and stat >=0 ) as l
						on f.fbpage_id=l.fb_id "
					  . $where." and l.fb_id is null " . $ord . $limit_str;
						}
		}else{
			$sql="SELECT distinct f.* FROM fbpage as f " . $tbl . $where . $ord . $limit_str;
		}
	//	echo $sql."<br>";

		$prodArr=$this->executeQuery($sql);

		return $prodArr;

	}


	/**
	 * 検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 * (参考：CompanyDao.class.php)
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		if(count($search[like])==1){
			if($search[like][0]==1){	//いいね済のページをカウント
				$sql="select count(f.fbpage_id) as cnt FROM fbpage as f
						inner join
							(select r.fbpage_id as fb_id from point_record as r
								where member_id ='" .$search[member_id]."' and stat >=0 ) as l
						on f.fbpage_id=l.fb_id "
					  . $where ;
			}else{	//未いいねのページをカウント
				$sql="select count(f.fbpage_id) as cnt FROM fbpage as f
						left join
							(select r.fbpage_id as fb_id from point_record as r
								where member_id ='" .$search[member_id]."' and stat >=0 ) as l
						on f.fbpage_id=l.fb_id "
					  . $where." and l.fb_id is null " ;
						}
		}else{
			$sql="select count(f.fbpage_id) as cnt FROM fbpage as f " . $tbl . $where ;
		}
	//	echo $sql."<br>";
		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}


	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		// オーナーID
		if($search["owner_id"]){
			$whTmp[]="owner_id = '".addslashes($search["owner_id"])."'";
		}

		// ジャンル
		if($search["genre"]){
//			$whTmp[]="category like '%".addslashes($search["category"])."%'";
			$whTmp[]="genre_id = '".addslashes($search["genre"])."'";
		}
		// カテゴリー
		if($search["category"]){
//			$whTmp[]="category like '%".addslashes($search["category"])."%'";
			$whTmp[]="category = '".addslashes($search["category"])."'";
		}
		// 都道府県
		if($search["place"]){
			$whTmp[]="pref = '".addslashes($search["place"])."'";
		}

		// フリーワード
		if($search["about"]){
			$whTmp[]="about like '%".addslashes($search["about"])."%'";
		}

		// ステータス
		if(is_array($search[stat])){
			$whTmp[]="stat ".$search[stat][ope]. "'".addslashes($search["stat"][data])."'";

		}
		else if($search["stat"]){
			$whTmp[]="stat = '".addslashes($search["stat"])."'";
		}

		// 凍結フラグ
		if(isset($search["freeze_flg"])){
			$whTmp[]="freeze_flg = '".addslashes($search["freeze_flg"])."'";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}

		// 多分tblはないよね。
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		// echo "here is makeSearch().";
		// var_dump($where);
		return array($where,$tbl);

	}

}


?>

