{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#user_id").val("");
	$("#user_name").val("");
}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>起案者一覧</h4>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="actress/list">
		<table class="search">
		<tr>
			<th>名前で探す：</th>
			<td>
				<input type="text" name="name" id="user_name"  value="{$search.name}" size="20" />
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>
		<a class="btn" href="/actress/edit/">新規作成</a>
		{* 検索結果 *}
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="right red">※プロジェクト登録済の起案者を削除することはできません。</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="actress/list/">
			<table class="admins clear">
			<tr>
				<th width="20">no</th>
				<th width="120">起案者名</th>
				<th width="200">メールアドレス</th>
				<th width="50">ステータス</th>
				<th width="60">登録日</th>
				<th width="60">プロジェクト</th>
				<th width="40">削除</th>
			</tr>
			{foreach from=$actress_list item="actress"}
				<tr>
					<td><a href="actress/edit/?actress_no={$actress.actress_no}&lang={$lang_code}">{$actress.actress_no}</a></td>
					<td>{$actress.public_name}</td>
					<td>{$actress.email}</td>
					<td>
							{assign var="status" value=$actress.status}
							{if $actress.status==0 && $actress.entry_type==2}
								仮登録
							{else}
								{$statusArr.$status}
							{/if}
					</td>
					<td>{$actress.reg_date|date_format:"%Y/%m/%d"}</td>
					<td>
					{if $actress.status==1}
					<a href="project/list/?actress_no={$actress.actress_no}">{$actress.project_cnt}件</a>&nbsp;
					<a href="project/editBasic/?lang={$lang_code}&actress_no={$actress.actress_no}">新規作成</a>
					{/if}
					</td>
					<td style="text-align: center;">
					<input type="checkbox" name="delete_dummy[]" value="{$actress.actress_no}" {if $actress.project_cnt>0}disabled="disabled" {/if}/>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="10">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			<div class="center">
				<input type="submit" name="sbm_update" value="一覧を更新する" onClick="return confirm('更新します。削除チェックの場合は、会員を削除します。');">
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

