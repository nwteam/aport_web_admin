<?php
class PointDAO extends DAOBase {

	/**
	 * ポイント集計
	 * @param unknown_type $target_month yyyymm
	 * @param unknown_type $fbpage_id
	 */
	public function getPointTotal($target_month="",$fbpage_id="") {

		if($target_month){
			$whTmp[]=" DATE_FORMAT(insert_date, '%Y%m') = '".addslashes($target_month)."'";
		}
		if($fbpage_id){
			$whTmp[]=" fbpage_id = '".addslashes($fbpage_id)."'";
		}


		$wh =implode(" and ",$whTmp);
		if($wh){
			$wh=" and ".$wh;
		}

		$sql="SELECT fbpage_id,DATE_FORMAT(insert_date, '%Y%m')as month,Count(no) as cnt
				 FROM point_record
				 WHERE stat=0 and point_flg =0".$wh.
				" GROUP BY fbpage_id, DATE_FORMAT(insert_date, '%Y%m') ";

	//	print $sql."<br>";
		$ret= $this->executeQuery($sql);

		return $ret;

	}

	/**
	 * ポイント集計
	 * @param unknown_type $fbpage_id
	 */
	public function getPointTotalALL($fbpage_id="") {

		if($fbpage_id){
			$whTmp[]=" fbpage_id = '".addslashes($fbpage_id)."'";
		}


		$wh =implode(" and ",$whTmp);
		if($wh){
			$wh=" and ".$wh;
		}

		$sql="SELECT Count(no) as cnt
				 FROM point_record
				 WHERE stat=0 and point_flg =0".$wh;

		//print $sql."<br>";
		$ret= $this->executeQuery($sql);

		return $ret[0][cnt];

	}

	/**
	 * ポイント検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		$sql="select count(*) as cnt from point_record ".$tbl.$where;

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//
		if($search["member_id"]){
			$whTmp[]="member_id =".addslashes($search["member_id"]);
		}

		//stat
		if(is_array($search["stat"])){
			$whTmp[]="stat ".$search["stat"]["ope"]."'".addslashes($search["stat"]["data"])."'";

		}elseif(isset($search["stat"])){
			$whTmp[]="stat ='".addslashes($search["stat"])."'";
		}

		//point_flg
		if(is_array($search["point_flg"])){
			$whTmp[]="point_flg ".$search["point_flg"]["ope"]."'".addslashes($search["point_flg"]["data"])."'";

		}elseif(isset($search["point_flg"])){
			$whTmp[]="point_flg ='".addslashes($search["point_flg"])."'";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		return array($where,$tbl);

	}

	/**
	 * ポイント検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {

			if(!$search["page"]) $search["page"]=1;

			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}

		$sql="select * from point_record as p ".$where.$ord.$limit_str;

	//	print $sql."<br>";
		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;

	}
}


?>

