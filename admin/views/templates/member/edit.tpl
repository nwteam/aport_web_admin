{include file="head.tpl"}
<script language="JavaScript">
<!--
{literal}


{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>会員情報編集</h4>

		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
{include file="messages.tpl"}

		←<a href="/member/list">ユーザー一覧へ</a>

<form action="" method="post"  >
      <table class="w01">
        <tr>
          <th>会員No</th>
          <td>{$input_data.user_no}</td>
          <input type="hidden" name="user_no" value="{$input_data.user_no}"  />
        </tr>

        <tr>
          <th>ニックネーム<span class="red">※</span></th>
          <td>
	 		{if $result_messages.nickname}
				<span class="red">{$result_messages.nickname}</span><br />
			{/if}
          <input type="text" name="nickname" value="{$input_data.nickname}"  />
		（全角可4～20文字）
          </td>
        </tr>
       <tr>
          <th>アドレス<span class="red">※</span></th>
          <td>
	 		{if $result_messages.email}
				<span class="red">{$result_messages.email}</span><br />
			{/if}
          <input type="text" name="email" value="{$input_data.email}"  /></td>
        </tr>
        <tr>
          <th>生年月日</th>
          <td>
          	{html_options name="birth_year" options=$yearArr selected=$input_data.birth_year}
            年
            {html_options name="birth_month" options=$monthArr selected=$input_data.birth_month}
            月
            {html_options name="birth_day" options=$dayArr selected=$input_data.birth_day}

            日 </td>
        </tr>
        <tr>
          <th>パスワード<span class="red">※</span></th>
          <td>
	 		{if $result_messages.new_password}
				<span class="red">{$result_messages.new_password}</span><br />
			{/if}
          <input type="text" name="new_password" value="{$input_data.new_password}"  />（半角英数字8～12文字）
          <br/>※変更する場合のみ入力してください
          </td>
        </tr>
        <tr>
          <th>LINE ID</th>
          <td>
	 		{if $result_messages.line_id}
				<span class="red">{$result_messages.line_id}</span><br />
			{/if}
          <input type="text" name="line_id" value="{$input_data.line_id}"  />
          </td>
        </tr>

       <tr>
          <th>氏名<span class="red">※</span></th>
          <td>
	 		{if $result_messages.member_name}
				<span class="red">{$result_messages.member_name}</span><br />
			{/if}
          <input type="text" name="member_name" value="{$input_data.name_1}{$input_data.name_2}"  />
          </td>
        </tr>
        <tr>
          <th>郵便番号<span class="red">※</span></th>
          <td>
	 		{if $result_messages.zipcode}
				<span class="red">{$result_messages.zipcode}</span><br />
			{/if}
          <input type="text" name="zipcode" value="{$input_data.zipcode}"  />
        </td>
        </tr>
        <tr>
          <th>都道府県<span class="red">※</span></th>
          <td>
	 		{if $result_messages.add_1}
				<span class="red">{$result_messages.add_1}</span><br />
			{/if}

			{html_options name=add_1 options=$prefArr selected=$input_data.add_1}
		</td>
        </tr>
        <tr>
          <th>住所<span class="red">※</span></th>
          <td>
	 		{if $result_messages.address}
				<span class="red">{$result_messages.address}</span><br />
			{/if}

          <input type="text" name="address" value="{$input_data.address}"  /></td>
        </tr>
        <tr>
          <th>電話<span class="red">※</span></th>
          <td>
	 		{if $result_messages.tel}
				<span class="red">{$result_messages.tel}</span><br />
			{/if}

          <input type="text" name="tel" value="{$input_data.tel}" />（半角入力、ハイフン不要。）</td>
        </tr>
<!--
        <tr>
          <th>カナ<span class="red">※</span></th>
          <td>
	 		{if $result_messages.furigana}
				<span class="red">{$result_messages.furigana}</span><br />
			{/if}
          <input type="text" name="furigana" value="{$input_data.furigana}"  /></td>
        </tr>
 -->
        </table>

 <!--
        口座情報
 			<table>
			<tr>
				<td colspan="2" align="left"><label for=""> <input type="radio" name="bank_category" value="0" {if $input_data.bank_category==0}checked{/if}/>銀行</label></td>
			</tr>
				<tr>
					<th>銀行名</th>
					<td>
						<input type="text" name="bank_company" value="{$input_data.bank_company}"  />
					</td>
				</tr>
				<tr>
					<th>支店名</th>
					<td>
					<input type="text" name="bank_branch" value="{$input_data.bank_branch}"  />支店
					</td>
				</tr>
				<tr>
					<th>支店番号</th>
					<td>
			          <input type="text" name="bank_branch_id" value="{$input_data.bank_branch_id}"  />
			        </td>
				</tr>
				<tr>
					<th>口座種別</th>
					<td>
			          <label>当座
			              <input type="radio" name="bank_account_type" value="1" {if $input_data.bank_account_type==0}checked{/if} />
			            </label>
			            <label>普通
			              <input type="radio" name="bank_account_type" value="2" {if $input_data.bank_account_type==1}checked{/if} />
			            </label>
					</td>
				</tr>
				<tr>
					<th>口座番号</th>
					<td>
			           <input type="text" name="bank_account" value="{$input_data.bank_account}"  />
			        </td>
				</tr>

			</table>
			<table>
			<tr>
				<td colspan="2" align="left"><label for=""> <input type="radio" name="bank_category"  value="1" {if $input_data.bank_category==1}checked{/if} />ゆうちょ銀行</label></td>
			</tr>
				<tr>
					<th>記号</th>
					<td>
			          <input type="text" name="yucho_account1" value="{$input_data.yucho_account1}"  />
					</td>
				</tr>
				<tr>
					<th>番号</th>
					<td>
			          <input type="text" name="yucho_account2" value="{$input_data.yucho_account2}"  />
			        </td>
				</tr>
			</table>
			<table>
			<tr>
				<td colspan="2" align="left">共通</td>
			</tr>
				<tr>
					<th>名義人名</th>
					<td><inout>
							<input type="text" name="bank_holder" value="{$input_data.bank_holder}" />
						</inout></td>
				</tr>
			</table>
 -->
      <div class="submit">
        <input type="submit"  name="modify" value="更新" />
        <input type="hidden" name="member_id" value="{$input_data.member_id}">
      </div>
    </div>
</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
