<?php
class AdminController extends AppRootController {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();

	}

	/**
	 * ログイン画面
	 */
    public function loginAction() {

    	// formがsubmitされた
    	if (isset($_POST["sbm_login"])) {

    		$commonDao = new CommonDao();
    		$search[status]="active";
    		$search[user_id]=$this->request->getParam("user_id");
    		$search[password]=to_hash($this->request->getParam("password"));
    		$admin_list=$commonDao->get_data_tbl2("site_administrators",$search);

/*
    		$search = new SiteAdminSearchCondition();
    		$search->setStatus("active");
    		$search->setUserId($this->request->getParam("user_id"));
    		$search->setPassword($this->request->getParam("password"));
    		$admin_dao = new SiteAdminDao();
			$admin_list = $admin_dao->doSearch($search);
*/
			if (count($admin_list) != 0) {
				$admin = $admin_list[0];

				$this->setAdminSession($admin);
				header("Location: /");
				exit;
			}
			else {
				$this->view->assign("user_id", $this->request->getParam("user_id"));
				$this->view->assign("msg", "ユーザーIDまたはパスワードが一致しません。");
			}
    		    	}

    	// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        $this->setTemplatePath("login.tpl");
        return;
	}

	/**
	 * ログアウト
	 */
	public function logoutAction() {
		$this->deleteAdminSession();
		header("Location:  /");
	}

	/**
	 * 管理者一覧表示
	 */
	public function listAction() {

		$dao = new SiteAdminDao();
		$commonDao=new CommonDao();
		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();


		$mode = "add";

		if(isset($_POST[sbm_save])){

			$mode = $_REQUEST["mode"];
			$op = "登録";

			$admin_obj=$_POST;

			//入力チェック
			if($mode=="add"){
				$checkData=CommonChkArray::$adminNewCheckData;
			}
			else{
				$checkData=CommonChkArray::$adminUpCheckData;
			}

			//入力チェック
			$ret=!$this->check($admin_obj,$checkData);

			//ユーザーID重複チェック
			if($mode=="add"){
				$ret=$commonDao->get_data_tbl("site_administrators","user_id",$admin_obj[user_id]);
				if($ret){
					$this->addMessage("user_name", "入力されたユーザーIDはすでに使用されています");
				}
			}
			if (count($this->getMessages()) >0) {
				$this->setSessionMessage();

				//$this->view->assign("result_messages", $result_messages);
			}
			else {

				foreach($checkData[dbstring] as $key=>$val){
					$ins[$key]=$admin_obj[$key];
				}

				if($mode=="add" || $mode!="add" && $admin_obj[password]!=""){
					$ins[password]=to_hash(trim($_REQUEST["password"]));
				}
				if($mode=="add"){
					//新規登録
					$ins[status]="active";
					$commonDao->InsertItemData2("site_administrators",$ins);

				}
				else{
					$op = "更新";
					$admin_no = $_REQUEST["admin_no"];
					//$admin_obj = $dao->getAdmin($admin_no);

					$uwhere[admin_no]=$admin_no;
					$commonDao->updateData2("site_administrators",$ins,$uwhere);
				}

				$this->addMessage(SYSTEM_MESSAGE_INFO, "サイト管理者を" . $op . "しました");
				$this->margeMessages($dao->getMessages());
				$this->setSessionMessage();
			}
		}
		else if(isset($_REQUEST["admin_no"])){

			$admin_obj = $dao->getAdmin($_REQUEST["admin_no"]);
			$mode = "update";
		}
		else{

			$admin_obj[admin_type]="admin";

		}

		$admins = $dao->doSearch($search);
		$this->view->assign("admins", $admins);

		$this->view->assign("admin_obj", $admin_obj);
		$this->view->assign("mode", $mode);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("admin/list.tpl");
		return;
	}

	/**
	 * 管理者情報登録・更新
	 */
	public function editAction() {

		$dao = new SiteAdminDao();
		$commonDao=new CommonDao();

		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();

		$mode = $_REQUEST["mode"];
		$op = "登録";

		if ($mode == "add") {

			$ins[status]="active";

		}
		else {
			$op = "更新";
			$admin_no = $_REQUEST["admin_no"];
			//$admin_obj = $dao->getAdmin($admin_no);

			$uwhere[admin_no]=$admin_no;
		}

		$input_data=$_POST;

		//入力チェック
		$checkData=CommonChkArray::$adminCheckData;
		//入力チェック
		$ret=!$this->check($input_data,$checkData);

		//ユーザーID重複チェック
		if($mode=="add"){
			$ret=$commonDao->get_data_tbl("site_administrators","user_id",$input_data[user_id]);
			if($ret){
				$this->addMessage("user_name", "入力されたユーザーIDはすでに使用されています");
			}
		}

		if (count($this->getMessages()) >0) {
			$this->setSessionMessage();
			$this->redirect("/admin/list");

			//$this->view->assign("result_messages", $result_messages);
		}
		else {

			foreach($checkData[dbstring] as $key=>$val){
				$ins[$key]=$input_data[$key];
			}

			if($mode=="add" || $mode!="add" && $input_data[password]!=""){
				$ins[password]=to_hash(trim($_REQUEST["password"]));
			}
			if($mode=="add"){
				//新規登録

				$commonDao->InsertItemData2("site_administrators",$ins);

			}
			else{
				$commonDao->updateData2("site_administrators",$ins,$uwhere);
			}

			$this->addMessage(SYSTEM_MESSAGE_INFO, "サイト管理者を" . $op . "しました");
			$this->margeMessages($dao->getMessages());
			$this->setSessionMessage();
			$this->redirect("/admin/list");
		}

		return ;
	}

	/**
	 * リスト更新
	 */
	public function bulkUpdateAction() {

		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();

		$commonDao=new CommonDao();

		//無効
		$commonDao->updateData("site_administrators", "status","active","","");
		if($_POST[disabled_dummy]){
			foreach($_POST[disabled_dummy] as $key=>$val){
				//無効処理
								//無効処理
				if($admin[admin_no]==$val){
					$this->addMessage(SYSTEM_MESSAGE_VALID, "自分自身を無効する事はできません");
					$this->setSessionMessage();
					$this->redirect("/admin/list");
					return;
				}
				else{

					$commonDao->updateData("site_administrators", "status","disabled","admin_no",$val);
				}
			}
		}


		//削除
		if($_POST[delete_dummy]){
			foreach($_POST[delete_dummy] as $key=>$val){
				//無効処理
				if($admin[admin_no]==$val){
					$this->addMessage(SYSTEM_MESSAGE_VALID, "自分自身を削除することはできません");
					$this->setSessionMessage();
					$this->redirect("/admin/list");
					return;
				}
				else{

					$commonDao->del_Data("site_administrators", "admin_no",$val);

				}

			}
		}


/*
		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();

		$dao = new SiteAdminDao();

		for ($i=0; $i<count($_REQUEST["admin_no"]); $i++) {
			$admin_obj = $dao->getAdmin($_REQUEST["admin_no"][$i]);

			// 削除
			if ($_REQUEST["delete"][$i]=="t") {

				if ($admin->getAdminNo() == $admin_obj->getAdminNo()) {
					$this->addMessage(SYSTEM_MESSAGE_VALID, "自分自身を削除することはできません");
					$this->setSessionMessage();
					$this->redirect("/admin/list");
					return;
				}
				else {
					$dao->delete($admin_obj);
					continue;
				}
			}

			// ステータス
			if ($_REQUEST["disabled"][$i]=="t") {
				if ($admin->getAdminNo() == $admin_obj->getAdminNo()) {
					$this->addMessage(SYSTEM_MESSAGE_VALID, "自分自身を無効にすることはできません");
					$this->setSessionMessage();
					$this->redirect("/admin/list");
					return;
				}
				$admin_obj->setStatus("disabled");
			}
			else {
				$admin_obj->setStatus("active");
			}
			$dao->update($admin_obj);
		}
*/
		$this->addMessage(SYSTEM_MESSAGE_INFO, "管理者一覧を更新しました");
		$this->setSessionMessage();
		$this->redirect("/admin/list");
		return;
	}

	/**
	 * POSTデータをSiteAdminオブジェクトに格納する
	 * @param SiteAdmin $admin
	 */
	private function setAdminPostData(SiteAdmin $admin) {
		$admin->setAdminNo($_REQUEST["admin_no"]);
		$admin->setUserId(trim($_REQUEST["user_id"]));
		$admin->setEmail(trim($_REQUEST["email"]));
		$admin->setUserName(trim($_REQUEST["user_name"]));
		$admin->setAdminType(trim($_REQUEST["admin_type"]));

		// パスワードがsubmitされた場合は変更する
		if (strlen($_REQUEST["password"]) > 0) {
			$admin->setPassword(to_hash(trim($_REQUEST["password"])));
		}
	}

	/**
	 * 入力チェック
	 * @param SiteAdmin $admin
	 * @return String エラーメッセージ（エラーがない場合は空文字列）
	 */
	private function validate(SiteAdmin $admin) {

		// ユーザーID重複チェック
		$dao = new SiteAdminDao();
		//$search = new SiteAdminSearchCondition();
		$search = new SiteAdmin();
		$search->setUserId($admin->getUserId());
		$search->setAdminNoNotIn($admin->getAdminNo());
		$admin_list = $dao->doSearch($search);
		if (count($admin_list) != 0) {
			$this->addMessage(SYSTEM_MESSAGE_VALID, "入力されたユーザーIDはすでに使用されています");
		}

		// 名前
		if (!strlen($admin->getUserName())) {
			$this->addMessage(SYSTEM_MESSAGE_VALID, "名前を入力してください");
		}

		// 管理者種別
		if (!strlen($admin->getAdminType())) {
			$this->addMessage(SYSTEM_MESSAGE_VALID, "管理者種別を選択してください");
		}

		if ($this->countMessage(SYSTEM_MESSAGE_VALID) > 0) {
			return false;
		}
		return true;
	}
}
?>
