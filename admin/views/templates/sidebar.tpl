<div id="sidebar clear">
	<ul id="jMenu">

		<li><a>TOP</a>
				<ul>
					<li><a href="/">TOP</a></li>
				</ul>
		</li>
		<li><a>プロジェクト管理</a>
				<ul>
					<li><a href="/actress/list/">起案者一覧</a></li>
					<li><a href="/project/list/">プロジェクト一覧</a></li>
			<!--		<li><a href="/company/list/">サポート一覧</a></li>-->
				</ul>
		</li>
		<li><a>会員情報管理</a>
				<ul>
					<li><a href="/member/list/">会員一覧</a></li>
					<li><a href="/comment/list/">コメント一覧</a></li>
				</ul>
		</li>
<!--
		<li><a>支払・請求管理</a>
				<ul>
					<li><a href="/pay/list/">ポイント換金 未払い一覧</a></li>
					<li><a href="/pay/history/">ポイント換金履歴</a></li>
					<li><a href="/billing/list/">オーナー請求 未入金一覧</a></li>
					<li><a href="/billing/history/">オーナー請求履歴</a></li>
				</ul>
		</li>
		<li><a>カテゴリー管理</a>
				<ul>
					<li><a href="/master/category2">業種マスタ</a></li>
					<li><a href="/master/category1">求人職種マスタ</a></li>
				</ul>
		</li>
-->
		<li><a>コラム管理</a>
				<ul>
					<li><a href="/column/list">コラム管理</a></li>
<!--					<li><a href="/column/category">コラムカテゴリ管理--></a></li>
				</ul>
		</li>
		<li><a>その他</a>
				<ul>
<!-- 					<li><a href="/news/list/">お知らせ管理</a></li> -->
					<li><a href="/slide/list/">トップページ表示順管理</a></li>
 					<li><a href="/inquiry/list/">問い合わせ管理</a></li>
					<li><a href="/admin/list">サイト管理者</a></li>
<!-- 					<li><a href="/siteconfig/">サイト設定</a></li> -->
				</ul>
		</li>

	</ul>
</div>