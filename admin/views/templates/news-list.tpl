{include file="head.tpl"}
<script type="text/javascript">
{literal}



{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>お知らせ管理</h3>
		<font color="red">
		{include file="messages.tpl"}
		</font>
		<div style="text-align:left;">
			<form name="fm_add" id="fm_add" method="POST" action="/news/edit/">
				<input type="hidden" name="mode" value="add" />
				<input type="submit" name="sbm_add" value="新規作成" />
			</form>
		</div>

			<div class="paging">
			</div>
			<form name="fm_list" id="fm_list" method="POST" action="">
			<table class="admins">
			<tr>
				<th class="id">登録日</th>
				<th class="id">タイトル</th>
				<th class="">詳細</th>
				<th class="">表示/非表示</th>
				<th class="">削除</th>
			</tr>
			{foreach from=$resuArr item="resu"}
				<tr>
					<td>{$resu.insert_date}</td>
					<td>{$resu.title}</td>
					<td><a href="/news/edit/?news_no={$resu.news_no}">詳細</a></td>
					<td>
					<input type="radio" name="display_flg[{$resu.news_no}]" value="1"{if $resu.display_flg=="1"} checked="checked"{/if}/>表示
					&nbsp;<input type="radio" name="display_flg[{$resu.news_no}]" value="0"{if $resu.display_flg=="0"} checked="checked"{/if}/>非表示
					<input type="hidden" name="display_flg_org[{$resu.news_no}]"  value="{$resu.display_flg}" />
					</td>
					<td><input type="checkbox" name="delete_dummy[]" value="{$resu.news_no}"  /></td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="5">お知らせははありません。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">

			</div>
			<div class="center">
				<input type="submit" name="regist" value="更新する" onclick="return confirm('お知らせの表示/非表示を更新します。削除にチェックしたお知らせを削除します。');">
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

