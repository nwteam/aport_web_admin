<?php
class ActressController extends AppRootController {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();

		require_once sprintf("%s/dao/ActressDao.class.php", MODEL_PATH);


	}

	/**
	 * 起案者一覧表示・検索
	 */
	public function listAction() {

		$search = null;
		$search_flag = false;
		$limit=ADMIN_V_CNT;

		$dao = new ActressDao();
		$commonDao = new CommonDao();

		//言語コード
		$lang_code=MAIN_LANGUAGE;
		$this->view->assign("lang_code", $lang_code);

		$page = $_REQUEST["page"];

		// 検索送信ボタンが押下されているか？
		if (isset($_POST["sbm_search"])) {

			$search[name]=$_POST[name];
			$search[page]=1;

			$_SESSION[search_condition]=$search;


		}
		// ページ番号が渡されたか？
		else if ($page) {
			// ページングによる遷移
			$search = $_SESSION["search_condition"];
			$search[page]=$this->request->getParam("page");

		}else if($_POST[sbm_update]){	//一括更新
			//ステータス変更
			if($_POST[status]){

				foreach($_POST[status] as $key=>$val){
					if($val!=$_POST[status_old][$key]){
						//ステータス更新
						$ret=$commonDao->updateData("sf_actress","status",$val,"actress_no",$key);
						if(!$ret){
							$delFlg=1;
						}
					}
				}
				if($delFlg==1){
					$this->addMessage("info","更新エラーがあります。");
				}
				else{
					$this->addMessage("info","ステータスを更新しました");
				}
			}

			//削除処理
			if($_POST[delete_dummy]){

				foreach($_POST[delete_dummy] as $key=>$val){
					//削除
					//削除
					$ret=$dao->delete($val);
					if(!$ret){
						$delFlg=1;
					}

				}
				if($delFlg==1){
					$this->addMessage("info","起案者削除エラーがあります。");
				}
				else{
					$this->addMessage("info","チェックした起案者を削除しました");
				}

			}

		}

		else {
			// sessionに検索条件が保存されている場合

			if($_SESSION["search_condition"]) unset($_SESSION["search_condition"]);
			$search[page]=1;

		}

		$total_cnt=$dao->searchCount($search,$lang_code);
		if($total_cnt>$limit){
			$page_navi = get_page_navi2($total_cnt, $limit, $search[page], "/actress/list/");
		}
		$actress_list=$dao->search($search,"a.create_date desc , a.actress_no desc",$limit,$lang_code);

		//登録プロジェクト数の取得
		for($c=0;$c<count($actress_list);$c++){
			$tmp=$commonDao->get_data_tbl("sf_project",array("project_owner","del_flg"),array($actress_list[$c][actress_no],0));
			$actress_list[$c][project_cnt]=count($tmp);
		}

		$entryArr=CommonArray::$entry_type_array;	//登録タイプ
		$this->view->assign("entryArr", $entryArr);
		$statusArr=CommonArray::$actress_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);


		$this->view->assign("actress_list", $actress_list);
		$this->view->assign("total_cnt", $total_cnt);
		$this->view->assign("navi", $page_navi);
		$this->view->assign("search", $search);


		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("actress/list.tpl");
		return;
	}

	/**
	 * 起案者情報更新
	 */
	public function editAction() {

		$dao = new ActressDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);
		$actress_no=$_REQUEST[actress_no];
		if($actress_no){
			$this->view->assign("mode", "edit");
			$this->view->assign("mode_str", "編集");
		}else{
			$this->view->assign("mode", "add");
			$this->view->assign("mode_str", "新規登録");
		}
		$lang_code="ja";
		$this->view->assign("lang_code", $lang_code);

		if($_POST[submit]){
			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//---------------- 入力チェック ---------------------------
			//基本事項
				$baseData=CommonChkArray::$actressRegistCheckData;
				$detailData=CommonChkArray::$actressRegistDetailCheckData;
				//print_r($detailData);
				//$detailNullCheck=CommonChkArray::$actressRegistDetailNullCheckData;

			$this->check($input_data,$baseData);
			$this->check($input_data,$detailData);
			if($input_data[status]==1){	//採用の時は必須項目チェックをする

				//$this->check($input_data,$detailNullCheck);
				if($input_data[email]==""){
					$this->addMessage("email","メールアドレスを入力してください。");
				}elseif($input_data[mem_email]==""){
					$this->addMessage("mem_email","会員のメールアドレスを入力してください。");
				}
				else{
					//会員情報取得
					$ret=$commonDao->get_data_tbl("sf_member","email",$input_data[mem_email]);
					if(count($ret)==1){
						//初期化
						$data1[user_id]="";
						$where1[user_id]=$input_data[actress_no];
						$ret_init=$commonDao->updateData2("sf_member", $data1, $where1);
						//紐付け処理
						$data2[user_id]=$input_data[actress_no];
						$where2[user_no]=$ret[0][user_no];
						$ret=$commonDao->updateData2("sf_member", $data2, $where2);
					}else{
						$this->addMessage("mem_email","メールアドレスに紐付く会員はまだ登録されてません。");
					}
				}
			}else{//プロジェクトが登録済みの場合は採用以外には変更できない
				$tmp=$commonDao->get_data_tbl("sf_project",array("project_owner","del_flg"),array($input_data[actress_no],0));
				if(count($tmp)>0){
					$this->addMessage("status","プロジェクト登録後に「採用」以外に変更することはできません。");
				}
			}
			//-------------- ここまで -----------------------------------
			if (count($this->getMessages()) >0) {
				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}
				$this->view->assign("result_messages", $result_messages);
			}
			else {
				//基本事項
				foreach($baseData[dbstring] as $key=>$val){
					$bkey[]=$key;
					$bval[]=$input_data[$key];
				}
				$bkey[]="public_name";
				$bval[]=$input_data[name];
				$input_data[public_name]=$input_data[name];

				//詳細事項
				foreach($detailData[dbstring] as $key=>$val){
					$dkey[]=$key;
					$dval[]=$input_data[$key];
				}
				//print_r_with_pre($input_data);
				if($input_data[actress_no]){//修正
					$ret=$dao->upItemData($bkey,$bval,$dkey,$dval,"actress_no",$actress_no,array("actress_no","lang"),array($actress_no,$lang_code),$input_data);

					if($ret){
						//====== 画像 正式アップ & 画像リサイズ作業 ===================
						$this->addMessage("info","起案者情報を保存しました");
					}
					else{
						$this->addMessage("error","起案者情報の保存エラーです");
					}
				}
				else{
					//----------- 新規登録 ------------
					//起案者Noの取得
					//$input_data[actress_no]=$dao->getNewActressNo();
					//$dkey[]="actress_no";
					//$dval[]=$input_data[actress_no];
					$bkey[]="create_date";
					$bval[]=date("Y-m-d H:i:s");

					$dkey[]="create_date";
					$dval[]=date("Y-m-d H:i:s");

					$ret=$dao->InsertItemData($bkey,$bval,$dkey, $dval,$input_data);

					if($ret){
						$input_data[actress_no]=$ret;
						//====== 画像 正式アップ & 画像リサイズ作業 =================
						$this->addMessage("info","起案者情報を登録しました");
					}
					else{
						$this->addMessage("error","起案者情報の登録エラーです");
					}
				}

				$this->view->assign("finish_flg", 1);
				foreach($this->getMessages() as $edit_msg){
					$edit_messages[$edit_msg->getMessageLevel()]=$edit_msg->getMessageBody();
				}
				$this->view->assign("edit_messages", $edit_messages);
			}
		}
		else if($actress_no){
			//DBに登録されている情報取得
			$tmp=$dao->getActressData($actress_no,$lang_code);
			//print_r_with_pre($tmp);
			if($tmp){
				$input_data=$tmp;
				$db_data=$tmp;
			}
			else{
				$this->addMessage("error","該当の起案者はいません。");
        		$this->setTemplatePath("error.tpl");
				return;
			}
		}
		else{
			//新規登録
			//デフォルト
			$input_data[lang]=MAIN_LANGUAGE;
			$input_data[entry_type]=2;
			$input_data[status]=0;
		}
		//config
		$catArr=CommonArray::$array_category;
		$this->view->assign("catArr", $catArr);	//カテゴリ
		$prefArr=CommonArray::$pref_text_array;
		$this->view->assign("prefArr", $prefArr);	//都道府県
		$entryArr=CommonArray::$entry_type_array;	//登録タイプ
		$this->view->assign("entryArr", $entryArr);
		$statusArr=CommonArray::$actress_status_array;	//ステータス
		if($input_data[entry_type]==2){
			$statusArr[0]="仮登録";
		}
		$this->view->assign("statusArr", $statusArr);
		$this->view->assign("input_data", $input_data);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();
        // テンプレート表示
        $this->setTemplatePath("actress/edit.tpl");
		return;
	}
}
?>

