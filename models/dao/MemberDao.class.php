<?php
class MemberDAO extends DAOBase {


	/**
	 * 会員登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$item=array()) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into sf_member($ins) values($valu)";

		$this->db->beginTransaction();
		try {

			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'member'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;;

	}
	/**
	 * 会員 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$item=array()) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_member set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Member'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 人材検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		$sql="select count(*) as cnt from sf_member ".$tbl.$where;

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//
		if($search["member_id"]){
			$whTmp[]="member_id =".addslashes($search["member_id"]);
		}
		//email
		if($search["email"]){
			$whTmp[]="email ='".addslashes($search["email"])."'";
		}
		//pass
		if($search["password"]){
			$whTmp[]="password ='".addslashes(to_hash($search["password"]))."'";
		}
		//名前
		if($search["name"]){
			$tmp[]="CONCAT(name_1,name_2) like '%".addslashes($search["name"])."%'";
			$tmp[]="nickname like '%".addslashes($search["name"])."%'";

			$whTmp[]="(".implode(" or ",$tmp).")";


		}
		//登録状況
		if(! $search["retire"]){
			$whTmp[]="status <=1";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		return array($where,$tbl);

	}

	/**
	 * 会員検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {

			if(!$search["page"]) $search["page"]=1;

			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}

		$sql="select * from sf_member as m ".$where.$ord.$limit_str;

		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;

	}



	/**
	 * Memberの削除
	 */
	public function delete($user_no) {


		$this->db->beginTransaction();
		try{

			$sql = "DELETE FROM sf_member WHERE member_id = {$user_no}";
			// 実行
			$this->executeUpdate($sql);

			//関連のあるものすべて削除

			// コメントの削除
			$sql = "DELETE FROM user_request WHERE member_id = {$member_id}";
			$this->executeUpdate($sql);


			//ID番号の付け替え
			$nMember_id="-".$member_id;
			$sql="update point_record set member_Id=".$nMember_id." where member_id=".$nMember_id;
			$this->executeUpdate($sql);

//			$sql="update point_bonus_record set member_Id=".$nMember_id." where member_id=".$nMember_id;
//			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MSG_LEVEL_ERROR, "Failed to delete 'Member'. " . $e);
			$this->db->rollBack();
			throw new Exception($e);
			return false;
		}
		return true;
	}


}
?>