{include file="head.tpl"}

<script type="text/javascript">
{literal}

function editcancel()
{
	document.regForm.action="slide/list/";
	document.regForm.submit();
}


{/literal}
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>トップページ表示順管理</h3>
{if $msg }
		編集しました。
{else}
<form name="regForm" method="post" action="slide/edit/" enctype="multipart/form-data" >
<input type="hidden" name="ctitle" value="{$ctitle}" />
<input type="hidden" name="slide_no" value="{$input_data.slide_no}" />
<input type="hidden" name="exec" value="{$exec}" />

<a href="/slide/list/">←トップページ表示順リストに戻る</a><br />

{if $upErrFlg==99}
<div class="header_msg_error">登録処理に失敗しました</div>
{elseif $upErrFlg==1}
<div class="header_msg_info">トップページ表示順登録が完了しました</div>
{/if}

<font color="red">
{if $result_messages.link}{$result_messages.link}{/if}
</font>
<br />
※は必須項目です
<table class="admin_list01">
<th>※プロジェクト</th>
{if $edit_flg==1}
<td>
{$project_title}
</td>
<input type="hidden" name="link" value="{$input_data.link}" />
{else}
<td>
	{html_options name=link options=$ProjectArr selected=$input_data.link}
</td>
{/if}
</tr>
<tr>
	<th>
		※New マーク
	</th>
	<td>
		<input type="radio" name="view_flg" value="0"{if $input_data.view_flg=="0"} checked="checked"{/if}/>Off
		&nbsp;<input type="radio" name="view_flg" value="1"{if $input_data.view_flg=="1"} checked="checked"{/if}/>On
	</td>
</tr>

</table>
{if $upErrFlg}
<p class="center">
	<input type="submit" name="regist" value="OK" />
	<input name="cancel" type="button" value="キャンセル" onclick="editcancel();" />
</p>
{else}
<p class="center">
	<input type="submit" name="regist" value="OK" />
	<input name="cancel" type="button" value="キャンセル" onclick="editcancel();" />
</p>
{/if}
</form>

{/if}
{include file="footer.tpl"}
</body>
</html>
