<?php
class InvestDAO extends DAOBase {


	/**
	 * 会員登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$item=array()) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into sf_comment($ins) values($valu)";

		$this->db->beginTransaction();
		try {

			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'member'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;;

	}
	/**
	 * 会員 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$item=array()) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_comment set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Comment'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 支援検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		$sql="select count(*) as cnt from sf_invest as c
			inner join sf_member as m on c.member_id=m.user_no
			inner join sf_project as p on c.project_no=p.no"
		.$tbl.$where;

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//会員No
		if($search["user_no"]){
			$whTmp[]="c.member_id =".addslashes($search["user_no"]);
		}
		//プロジェクトNo
		if($search["project_no"]){
			$whTmp[]="c.project_no =".addslashes($search["project_no"]);
		}
		//プロジェクト名
		if($search["project_name"]){
			$whTmp[]="p.public_title like '%".addslashes($search["project_name"])."%'";
		}
		//ニックネーム
		if($search["nickname"]){
			$whTmp[]="m.nickname like '%".addslashes($search["nickname"])."%'";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		return array($where,$tbl);

	}

	/**
	 * 支援検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {

			if(!$search["page"]) $search["page"]=1;

			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}

		$sql="select c.* ,m.nickname as nickname,m.member_name,p.public_title as project_name
			from sf_invest as c
			inner join sf_member as m on c.member_id=m.user_no
			inner join sf_project as p on c.project_no=p.no"
			.$where.$ord.$limit_str;

		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;

	}

	/**
	 * 会員検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function getCommentData($no) {

		$no=addslashes($no);
		$sql="select c.* ,m.nickname as nickname,m.member_name,p.public_title as project_name
			from sf_comment as c
			inner join sf_member as m on c.member_id=m.user_no
			inner join sf_project as p on c.project_no=p.no
			where c.no=".$no;

		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;

	}


	/**
	 * 支援削除
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function DeleteInvest($invest_no,$project_no) {
		$commonDao = new CommonDAO();

		$this->db->beginTransaction();
		try{

			$tmp=$commonDao->get_data_tbl("sf_invest","no",$invest_no);
			$invest_data=$tmp[0];

			$sql = "DELETE FROM sf_invest WHERE no = {$invest_no}";
			// 実行
			$this->executeUpdate($sql);

			if($invest_data[status]>0){
				//プロジェクトの支援サマリーの更新
				$tmp=$commonDao->get_data_tbl("sf_project","no",$project_no);
				$project_data=$tmp[0];

				$summary=$project_data[now_summary]-$invest_data[invest_amount];
				$supporter=$project_data[now_supporter]-1;

				$fdata[now_summary]=$summary;
				$fdata[now_supporter]=$supporter;

				$where[no]=$project_no;

				$ret=$commonDao->updateData2("sf_project",$fdata,$where);
			}
			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MSG_LEVEL_ERROR, "Failed to delete 'Project'. " . $e);
			$this->db->rollBack();
			throw new Exception($e);
			return false;
		}
		return true;
	}

	/**
	 * プロジェクト支援コース別 有効支援件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function getInvestCount($project_no,$present_no) {

		$sql="select count(no) as cnt from sf_invest as c
				where project_no={$project_no} and present_no={$present_no} and status>0 and status<=91 ";

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}


}
?>