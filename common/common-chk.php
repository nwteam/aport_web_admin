<?php
/**
 * common.config.php
 * @version 0.1
 * @author takeshima
 * @since 2012/05/14
 */

// 共通チェック配列クラス
class CommonChkArray {


	/*
	 *
	 * チェック配列の数字意味
	   ========================================================================================
	   string  --　項目名
	　 chk     --  チェックするタイプ (0:なし 1:半角英数 2:数値のみ 3:e-mail 5:郵便番号/電話番号等(数字と"-")
	　　　　　　　 6:全角カナ //7:全角かな  8:半角カナ 11:数値と/ 12:数値と小数点)
		       14:数値と-と:
	   nchk    --  必須項目フラグ(1:必須　0:任意）　　
	   ========================================================================================
	*/
//入力チェック用配列(登録時に使用するので、テーブルに入れたい項目を、dbstringのみフォームのinput全項目セットしておく。別テーブルに入れる場合は、ここには入れずにソース内で調整）
//string は エラー時に表示される文字列
//、



//
// Spreaders
// added by Kensuke 20140501
//

//********* 起案者登録 **********************************
	//起案者登録
	public  static $actressRegistCheckData = Array(
	   "dbstring" =>  array(
			"email" => "メールアドレス",
			"status" => "審査ステータス",
	),
	"string" =>  array(
			"name" => "起案者名",
			"email" => "メールアドレス",
			"status" => "ステータス",
	),
     "type" => array(
			"name" => "text",
			"email" => "text",
			"status" => "pull",
			 			),
	"chk" => array(
			"email" => "3",
			 						),
	"max" => array(
			),
	"min" => array(
			),
	"nchk" => array(
			"name" => "1",
			"email" => "1",
			"status" => "1",
			)
		);
	//起案者詳細登録
	public  static $actressRegistDetailCheckData = Array(
		"dbstring" =>  array(
			"lang" => "言語コード",
			"name" => "名前",
			"fb_url" => "Facebookアカウント",
			"category_no" => "カテゴリ",
			"wish_price" => "想定目標金額",
			"project_name" => "プロジェクト名",
			"project_text" => "プロジェクト概要",
			"comment_text" => "想定リターン",
		),
		"string" =>  array(
			"name" => "名前",
			"fb_url" => "Facebookアカウント",
			"category_no" => "カテゴリ",
			"wish_price" => "想定目標金額",
			"project_name" => "プロジェクト名",
			"project_text" => "プロジェクト概要",
			"comment_text" => "想定リターン",
		),
		"type" => array(
			"name" => "text",
			"fb_url" => "text",
			"category_no" => "pull",
			"wish_price" => "text",
			"project_name" => "text",
			"project_text" => "text",
			"comment_text" => "text",
		),
		"chk" => array(
		),
		"max" => array(
		),
		"min" => array(
		),
		"nchk" => array(
		)
	);

	//起案者詳細登録
	public  static $actressRegistDetailNullCheckData = Array(
	   "dbstring" =>  array(
				),
	"string" =>  array(
//			"name" => "名前",
			"area" => "居住地域",
			"age" => "年齢",
			),
     "type" => array(
//			"name" => "text",
			"area" => "pull",
			"age" => "text",
			 			),
	"chk" => array(
			 						),
	"max" => array(
			),
	"min" => array(
			),
	"nchk" => array(
//			"area" => "1",
//			"age" => "1",
						)
		);
//********* プロジェクト登録 **********************************
	//プロジェクト基本情報登録
	public  static $projectRegistBasicCheckData = Array(
	   "dbstring" =>  array(
//			"user_no" => "会員番号",
			"project_type" => "プロジェクトタイプ",
			"public_title" => "共通タイトル",
			"category_no" => "カテゴリーNO",
//			"type" => "タイプ",
//			"area" => "プロジェクト名",
			"project_owner" => "起案者No",
			"wish_price" => "達成条件　支援額",
//			"wish_supporter" => "達成条件　支援件数",
//			"wish_share" => "達成条件　facebook、Twitter合算",
			"invest_limit" => "支援募集期限 ",
//			"project_start_date" => "プロジェクト開始日",
			"project_record_date" => "プロジェクト公開日",
//			"project_success_date" => "プロジェクト成立日",
//			"now_summary" => "支援金額 ",
			"movie_type" => "メイン動画 ",
			"movie_url" => "動画のURL ",
			"hope_url" => "希望のURL ",
			"now_supporter" => "支援者数",
//			"now_like" => "いいね数",
//			"now_twitter" => "ツイート数 ",
			//"movie_type" => "動画タイプ",
			"status" => "ステータス",
			"chk_status" => "審査ステータス",
//			"recommend_flg" => "お勧めフラグ",
//			"recommend_text" => "お勧めテキスト",
//			"admin_summary" => "運営調整　支援額 ",
//			"admin_share" => "運営調整　facebook、Twitter合算",
		),
	"string" =>  array(
//			"no" => "プロジェクトNo",
			"project_type" => "プロジェクトタイプ",
			"public_title" => "共通タイトル",
//			"user_no" => "会員番号",
			"category_no" => "カテゴリー",
//			"type" => "タイプ",
//			"area" => "プロジェクト名",
			"project_owner" => "起案者",
			"wish_price" => "達成条件　支援額",
//			"wish_supporter" => "達成条件　支援件数",
//			"wish_share" => "達成条件　facebook、Twitter合算",
			"invest_limit" => "支援募集期限 ",
//			"project_start_date" => "プロジェクト開始日",
			"project_record_date" => "プロジェクト公開日",
			"project_success_date" => "プロジェクト成立日",
//			"now_summary" => "支援金額 ",
			"movie_type" => "メイン動画 ",
			"movie_url" => "動画のURL ",
			"hope_url" => "希望のURL ",
//			"now_supporter" => "支援者数",
//			"now_like" => "いいね数",
//			"now_twitter" => "ツイート数 ",
			//"movie_type" => "動画タイプ",
			"status" => "ステータス",
			"chk_status" => "審査ステータス",
//			"recommend_flg" => "お勧めフラグ",
//			"recommend_text" => "お勧めテキスト",
//			"admin_summary" => "運営調整　支援額 ",
//			"admin_share" => "運営調整　facebook、Twitter合算",
					),
     "type" => array(
			"project_type" => "text",
			"public_title" => "text",
			"project_owner" => "pull",
			"wish_price" => "text",
//			"wish_share" => "text",
			"invest_limit" => "text",
//			"movie_type" => "option",
			"movie_type" => "radio ",
			"movie_url" => "text ",
			"hope_url" => "text ",
//			"admin_summary" => "text",
//			"admin_share" => "text",

					),
	"chk" => array(
			"wish_price" => "2",
//			"wish_share" => "2",
//			"admin_summary" => "2",
//			"admin_share" => "2",
			),
	"max" => array(
			),
	"min" => array(
			),
	"nchk" => array(
			"public_title" => "1",
			"category_no" => "1",
			"project_owner" => "1",
			"status" => "1",
			"chk_status" => "1",
//			"wish_price" => "1",
//			"wish_share" => "1",
//			"invest_limit" => "1",
//			"movie_type" => "1",
			)
		);
//********* コラム登録 **********************************
	//コラム基本情報登録
	public  static $columnRegistBasicCheckData = Array(
	   "dbstring" =>  array(
			"title" => "タイトル",
			"seo_keywords" => "meta keywords",
			"seo_description" => "meta description",
			"tag_list" => "タグ",
			"column_category_no" => "カテゴリー",
		),
	"string" =>  array(
			"title" => "タイトル",
			"seo_keywords" => "meta keywords",
			"seo_description" => "meta description",
			"tag_list" => "タグ",
			"column_category_no" => "カテゴリー",
					),
     "type" => array(
			"title" => "text",
			"column_category_no" => "pull",
			"seo_keywords" => "text",
			"seo_description" => "text",
			"tag_list" => "text",
					),
	"chk" => array(
			),
	"max" => array(
			),
	"min" => array(
			),
	"nchk" => array(
			"title" => "1",
			"column_category_no" => "1",
			"seo_keywords" => "1",
			"seo_description" => "1",
			)
		);
//********* 支援コース **********************************
	// 支援コース登録
	public  static $presetRegistCheckData = Array(
	"dbstring" =>  array(
			"project_no" => "プロジェクトNO",
			"lang" => "言語コード",
			"min" => "最低支援金額",
			"invest_limit" => "支援限度数",
			"sort" => "表示順",
			"title" => "タイトル",
			"text" => "支援コース内容",

			),
	"string" =>  array(
			"project_no" => "プロジェクトNO",
			"lang" => "言語コード",
			"min" => "最低支援金額",
			"invest_limit" => "支援限度数",
			"sort" => "表示順",
			"title" => "タイトル",
			"text" => "支援コース内容",
						),
     "type" => array(
			"min" => "text",
			"invest_limit" => "text",
			"sort" => "text",
			"title" => "text",
			"text" => "text",
									),
	"nchk" => array(
			"min" => "1",
			"invest_limit" => "1",
			"sort" => "1",
			"title" => "1",
			"text" => "1",
											),
		"max" => array(
//			"zip1" 			=> "3",
//			"zip2" 			=> "4",
			),
	"min" => array(
//			"zip1" 			=> "3",
//			"zip2" 			=> "4",
			),

	"chk" => array(
			"min" => "2",
			"limit" => "2",
			"sort" => "2",
			)
		);


//********* 新規会員登録 **********************************
	// ユーザー新規登録
	public  static $memberRegistCheckData = Array(
	"dbstring" =>  array(
			"nickname" => "ニックネーム",
			"email" => "メールアドレス",
			"birth_year" => "生年月日Y",
			"birth_month" => "生年月日M",
			"birth_day" => "生年月日D",
			"line_id" => "ラインID",
			"member_name" => "氏名",
			"zipcode" => "郵便番号",
			"add_1" => "都道府県",
			"address" => "住所",
			"tel" => "電話番号",

			),
	"string" =>  array(
			"nickname" => "ニックネーム",
			"email" => "メールアドレス",
			"birth_year" => "生年月日Y",
			"birth_month" => "生年月日M",
			"birth_day" => "生年月日D",
			"new_password" => "新パスワード",
			"line_id" => "ラインID",
			"member_name" => "氏名",
			"zipcode" => "郵便番号",
			"add_1" => "都道府県",
			"address" => "住所",
			"tel" => "電話番号",
		),
     "type" => array(
			"nickname" => "text",
			"email" => "text",
			"birth_year" => "pull",
			"birth_month" => "pull",
			"birth_day" => "pull",
			"new_password" => "text",
			"line_id" => "text",
			"member_name" => "text",
			"zipcode" => "text",
			"add_1" => "pull",
			"address" => "text",
			"tel" => "text",
			),
	"nchk" => array(
			"nickname" => "1",
			"email" => "1",
		),
		"max" => array(
			"new_password" => "12",
			"nickname" => "20",
			),
	"min" => array(
			"new_password" => "8",
			"nickname" => "4",
			),

	"chk" => array(
			"zipcode" 	=> "5",
			"email" 	=> "3",
			"tel" => "2",
			"new_password" => "1",
			)
		);



	public  static $AdCheckData = Array(
	   "dbstring" =>  array(
				"name" => "広告名",
				"fee" 	=> "掲載料金",
				"link" => "リンク先",
				"view_flg" => "表示/非表示",
				"view_start" => "掲載期間開始",
				"view_end" => "掲載期間終了",

		),
		"string" =>  array(
				"name" => "広告名",
				"fee" 	=> "掲載料金",
				"link" => "リンク先",
				"view_start" => "掲載期間開始",
				"view_end" => "掲載期間終了",
		),
	     "type" => array(
				"name" => "text",
				"link" => "text",
				"fee" 	=> "text",
	 		 ),
	    "max" => array(
	 		 ),
	    "min" => array(
	 		 ),
	 	"chk" => array(
				"name" => "0",
				"fee" 	=> "2",
				),
		"nchk" => array(
				"name" => "1",
				"link" => "1",
				"fee" => "0",
				"view_start" => "1",
				"view_end" => "1",

			)
	);

	public  static $SlideCheckData = Array(
	   "dbstring" =>  array(
				"link" => "リンク先",
				"view_flg" => "On/Off",

		),
		"string" =>  array(
				"link" => "リンク先",
		),
	     "type" => array(
				"link" => "text",
	 		 ),
	    "max" => array(
	 		 ),
	    "min" => array(
	 		 ),
	 	"chk" => array(
				),
		"nchk" => array(
				"link" => "1",
			)
	);
	//管理サイト　サイト管理者登録
	public  static $adminUpCheckData = Array(
	   "dbstring" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
		),
	"string" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
				),
     "type" => array(
			"user_id" => "text",
			"user_name" => "text",
			"email" 	=> "text",
		 ),
    "max" => array(
 		 ),
    "min" => array(
  		 ),
 	"chk" => array(
			"email" 	=> "3",

			),
	"nchk" => array(
			"user_id" => "1",
			"user_name" => "1",
			"email" 	=> "1",

			)
		);



	public  static $inquiryCheckData = Array(
	   "dbstring" =>  array(

		),
	"string" =>  array(
			"name" 	=> "氏名",
			"email" => "メールアドレス",
			"title" => "タイトル",
			"pur" 	=> "お問い合わせの目的",
			"inq" 	=> "内容",
		),
     "type" => array(
			"name" 	=> "text",
			"email" => "text",
			"title" 	=> "text",
			"pur" 	=> "radio",
			"inq" 	=> "textarea",
		 ),
    "max" => array(
 		 ),
    "min" => array(
  		 ),
 	"chk" => array(
			"email" 	=> "3",

			),
	"nchk" => array(
			"name" 		=> "1",
			"email"	 	=> "1",
			"title" 	=> "1",
			"pur" 		=> "1",
			"inq" 		=> "1",

			)
		);

//********* 管理サイト　サイト管理者更新 **************************
	public  static $adminNewCheckData = Array(
	   "dbstring" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
		),
	"string" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
				),
     "type" => array(
			"user_id" => "text",
			"user_name" => "text",
			"email" 	=> "text",
		 ),
    "max" => array(
 		 ),
    "min" => array(
  		 ),
 	"chk" => array(
			"email" 	=> "3",

			),
	"nchk" => array(
			"user_id" => "1",
			"user_name" => "1",
			"email" 	=> "1",

			)
		);




//********* お知らせ **********************************
	public  static $newsCheckData = Array(
	   "dbstring" =>  array(
			"title" 	=> "お知らせタイトル",
			"detail" 	=> "内容",
			"news_date" => "投稿日設定",
			"display_flg" => "表示/非表示",
	),
	"string" =>  array(
			"title" 	=> "お知らせタイトル",
			"detail" 	=> "内容",
			"news_date" => "投稿日設定",
	),
     "type" => array(
			"title" 	=> "text",
			"detail" 	=> "text",
 		 ),
    "max" => array(
	 ),
    "min" => array(
	 ),
 	"chk" => array(
	 ),
	"nchk" => array(
			"title" 	=> "1",
			"detail" 	=> "1",
			"news_date" => "1",
	 )
	);




}

	/*
	 *
	 * チェック配列の数字意味
	   ========================================================================================
	   string  --　項目名
	　 chk     --  チェックするタイプ (0:なし 1:半角英数 2:数値のみ 3:e-mail 5:郵便番号/電話番号等(数字と"-")
	　　　　　　　 6:全角カナ //7:全角かな  8:半角カナ 11:数値と/ 12:数値と小数点)
		       14:数値と-と:
	   nchk    --  必須項目フラグ(1:必須　0:任意）　　
	   ========================================================================================
	*/

?>
