<?php
class CharacterDao extends DaoBase {

	/**
	 * Category一覧を取得する
	 * @param unknown_type $where
	 * @param unknown_type $limit
	 * @param unknown_type $order
	 */
	public function getCategoryList($where="", $limit="", $order="") {
		if ($order == "") $order = " ORDER BY a.v_order ASC ";
		$sql = "SELECT
		            a.*
				FROM
					mst_characteristic  a
				{$where}
				{$order}
				{$limit}
			 ";

		$this->setFetchClass("employ");
		$categories_src = $this->executeQuery($sql);
		return $categories_src;
/*
		$categories = array();
		foreach ($categories_src as $category_src) {
			$categories[] = $this->getRentalCategory($category_src->getRentalCategoryNo());
		}
		return $categories;
*/
	}

	/**
	 * RentalCategoryを取得する
	 * @param int $category_no
	 * @return RentalCategory
	 */
	public function getRentalCategory($id) {
		$id = $this->db->quote(addslashes($id));
        $sql = "SELECT
                    a.*
                FROM
                    mst_characteristic  a
                WHERE
                    id = {$id}
                ";
        $this->setFetchClass("employ");
        $rs = $this->executeQuery($sql);

		if (!$rs) {
			return false;
		}

		$category = $rs[0];

		return $category;
	}
	public function getRentalCategoryArr($id) {
		$id = $this->db->quote(addslashes($id));
        $sql = "SELECT
                    a.*
                FROM
                    mst_characteristic  a
                WHERE
                    id = {$id}
                ";
        $rs = $this->executeQuery($sql);

		if (!$rs) {
			return false;
		}

		$category = $rs[0];
		return $category;
	}

	/**
	 *
	 * @param int $category_no
	 * @return RentalCategory
	 */
	public function getRentalCategoryByName($name) {
		$value = $this->db->quote(addslashes($name));
        $sql = "SELECT
                    a.*
                FROM
                    mst_characteristic  a
                WHERE
                    name = {$value}
                ";
        $rs = $this->executeQuery($sql);

		if (!$rs) {
			return false;
		}

		return $rs[0];
	}

	/**
	 * 検索結果一覧
	 * @param RentalCategorySearchCondition $search
	 * @param Int $limit 1ページ内表示件数
	 * @return Array 検索結果
	 */
	public function doSearch(RentalCategorySearchCondition $search, $limit=null) {
		$where = $this->makeSearchCondition($search);
		// 検索制限句生成
		if (!$limit) {
			$limit_str = "";
		}
		else {
			 $limit = (int)$limit;
			 $offset = ((int)$search->getCurPage()  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}
		// ORDER BY句生成
		$order = "";
		if ($search->getOrderByInsertDate()) {
			$order = "ORDER BY insert_date " . $search->getOrderByInsertDate();
		}

		return $this->getRentalCategoryList($where, $limit_str, $order);
	}


	/**
	 * 検索件数を取得
	 * @param RentalCategorySearchCondition $search
	 * @return Int 件数
	 */
	public function doSearchCount(RentalCategorySearchCondition $search) {
		$where = $this->makeSearchCondition($search);
		$sql = "SELECT
					COUNT(*)
				FROM
					mst_characteristic  a
				{$where}
			 ";
		$this->setFetchMode(PDO::FETCH_NUM);
		return $this->executeQuery($sql);
	}

	/**
	 * 検索条件生成
	 * @param RentalCategorySearchCondition $search
	 * @return String WHERE句
	 */
	private function makeSearchCondition(RentalCategorySearchCondition $search) {
		//$where = " WHERE  ";

		// カテゴリ名
		if ($search->getCategoryName()) {
			$where .= " AND a.category_name LIKE '%".addslashes($search->getCategoryName())."%' ";
		}

		//カテゴリID
		if ($search->getCategoryId()) {
			$where .= " AND c.category_id='".addslashes($search->getCategoryId())."' ";
		}
		return $where;
	}

	/**
	 * RentalCategory登録
	 * @param RentalCategory $category
	 * @return int 直近の挿入行ID
	 */
	public function insert($category) {

		//$parent_id = $this->db->quote($category->getParentid());
//		$parent_id = $category->getParentid();
//		if($parent_id=="") $parent_id=NULL;

		$name = $this->db->quote(htmlspecialchars($category->getName(), ENT_QUOTES));
		$order = $this->db->quote(htmlspecialchars($category->getOrder(), ENT_QUOTES));
		$regdate = $this->db->quote(htmlspecialchars($category->getRegdate(), ENT_QUOTES));

		$sql = "INSERT
				INTO mst_characteristic (
					 name
					, `v_order`
					, regdate
				)
				VALUES (
					 {$name}
					, {$order}
					, {$regdate}
				);
			";

		$this->db->beginTransaction();
		try{
			// 実行
			$this->executeUpdate($sql);
/*
			$id = $this->db->lastInsertId();
			$category->setRentalCategoryNo($id);
*/
			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'employ'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

/*
		// RentalCategoryExtAttrの登録
		$ext_attr_dao = new RentalCategoryExtAttrDAO();
		foreach ($category->getExtAttrList() as $attr) {
			$attr->setRentalCategoryNo($category->getRentalCategoryNo());
			$ext_attr_dao->insert($attr);
		}

		return $lastInsertId;
*/
	}

	/**
	 * RentalCategory更新
	 * @param RentalCategory $category
	 * @return Boolean 処理結果
	 */
	public function update($category,$id) {

		/*
		$category_no = $this->db->quote($category->getCategoryNo());
		$parent_no = $this->db->quote($category->getParentNo());
		$category_name = $this->db->quote($category->getCategoryName());
		$sort = $this->db->quote($category->getSort());
*/

		if($category->getOrder()){
			$setTmp[]=" `v_order` =".htmlspecialchars($category->getOrder(), ENT_QUOTES);
		}
		if($category->getName()){
			$setTmp[]=" name ='".htmlspecialchars($category->getName(), ENT_QUOTES)."'";
		}

		$setValue=implode(",",$setTmp);



		$sql = "UPDATE mst_characteristic  SET
				{$setValue}
			WHERE
					id={$id}
		";
//echo $sql;
		$this->db->beginTransaction();
		try{
			// 実行
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to update 'employ'." . $e);
			$this->db->rollBack();
			return false;
		}
/*
		// CategoryExtAttrの登録
		$ext_attr_dao = new CategoryExtAttrDAO();
		foreach ($category->getExtAttrList() as $attr) {
			$attr->setCategoryNo($category->getCategoryNo());
			$ext_attr_dao->update($attr);
		}
*/
		return true;
	}

	/**
	 * Category削除
	 * @param Category $category
	 * @return Boolean 処理結果
	 */
	public function delete(RentalCategory $category) {

/*
		// CategoryExtAttrの削除
		$ext_attr_dao = new CategoryExtAttrDAO();
		foreach ($category->getExtAttrList() as $attr) {
			$attr->setCategoryNo($category->getCategoryNo());
			$ext_attr_dao->delete($attr);
		}
*/
		$id = $this->db->quote($category->getId());
		$sql = "DELETE FROM mst_characteristic
				WHERE id = {$id}";
//echo $sql;
	    $this->db->beginTransaction();
	    try {
	    	// 実行
	    	$this->executeUpdate($sql);

	    	// コミット
			$this->db->commit();

	    }catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to delete 'Category'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}

	//

}
?>