<?php
class ProjectDAO extends DAOBase {

	/**
	 * プロジェクト情報取得
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function getProjectList($where="", $limit="", $order="") {
		if ($order == "") $order = " ORDER BY a.create_date ASC ";
		$sql = "SELECT
		            a.*
				FROM
					sf_project  a
				{$where}
				{$order}
				{$limit}
			 ";

		$project_src = $this->executeQuery($sql);
		return $project_src;
	}

	/**
	 * プロジェクト登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into sf_project($ins) values($valu)";

		$this->db->beginTransaction();
		try {
			//print $sql."<br>";
			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();
			//print "ID=".$lastInsertId."<br>";

			//メイン言語の詳細データの作成
			$ikey[]="project_no";
			$ival[]=$lastInsertId;
			$ikey[]="lang";
			$ival[]=MAIN_LANGUAGE;
			$ikey[]="project_name";
			$ival[]=$item[public_title];
			$ikey[]="create_date";
			$ival[]=date("Y-m-d H:i:s");

			$ret=$commonDao->InsertItemData("sf_project_detail",$ikey,$ival);

			//動画の登録
			if($item[new_movie]){
				switch($item[movie_type]){
					case 1;	//動画
						$image_type_str="PROJECT_MP4";
						$item[new_movie]=$lastInsertId."_mv_".$item[new_movie];
						break;

					case 2;	//YouTube
						$image_type_str="PROJECT_YOUTUBE";
						break;

					case 3;	//アニメーションＧＩＦ
						$image_type_str="PROJECT_GIF";
						$item[new_movie]=$lastInsertId."_mv_".$item[new_movie];
						break;
				}

				$ikey=array();
				$ival=array();
				$ikey[]="image_type";
				$ival[]=$image_type_str;
				$ikey[]="parent_no";
				$ival[]=$lastInsertId;
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$item[new_movie];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);
			}

				//動画の場合の静止画の登録
			if($item[new_movie2]){
				$ikey=array();
				$ival=array();

				$ikey[]="image_type";
				$ival[]="PROJECT_POSTER";
				$ikey[]="parent_no";
				$ival[]=$lastInsertId;
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$lastInsertId."_mvp_".$item[new_movie2];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);
			}

			//動画（YouTube）の登録
			if ($item[movie_type]==2){

				$ikey=array();
				$ival=array();

				$ikey[]="image_type";
				$ival[]="PROJECT_YOUTUBE";
				$ikey[]="parent_no";
				$ival[]=$lastInsertId;
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$item[movie2][file_name];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");
				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);


			}
			//サムネイル
			if($item[new_cover_img]){
				$ikey=array();
				$ival=array();

				$ikey[]="image_type";
				$ival[]="PROJECT_COVER";
				$ikey[]="parent_no";
				$ival[]=$lastInsertId;
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$lastInsertId."_sn_".$item[new_cover_img];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

			}

			// コミット
			$this->db->commit();

			$this->db->beginTransaction();
			if($item[new_cover_img]){
				$cover_img_name=$lastInsertId."_sn_".$item[new_cover_img];
				$sql="update sf_project set cover_img='{$cover_img_name}' where no={$lastInsertId}";
				$ret=$commonDao->run_sql($sql);
			}
			// コミット
			$this->db->commit();

		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'sf_project'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;

	}
	/**
	 * プロジェクト 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_project set $ins $where";

		$this->db->beginTransaction();
		try {
			//print $sql."<br>";
			// 実行
			$this->executeUpdate($sql);

			//共通氏名の反映

			$name_dt=trim($item[public_title]);
			$name_dt=htmlspecialchars(addslashes($name_dt));
			$sql="update sf_project_detail set project_name='".$name_dt."' where project_no=".$item[no] ." and lang='".MAIN_LANGUAGE."'";
			$this->executeUpdate($sql);

			//動画(動画・アニメーションGIF)の登録
			if($item[new_movie]){
				switch($item[movie_type]){
					case 1;	//動画
						$image_type_str="PROJECT_MP4";
						break;

					case 3;	//アニメーションＧＩＦ
						$image_type_str="PROJECT_GIF";
						break;
				}
			//既存の画像の削除
				$ret=$commonDao->del_Data("general_images",array("image_type","parent_no"),array($image_type_str,$item[no]));

				$ikey[]="image_type";
				$ival[]=$image_type_str;
				$ikey[]="parent_no";
				$ival[]=$item[no];
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$item[no]."_mv_".$item[new_movie];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);
			}

				//動画の場合の静止画の登録
			if($item[new_movie2]){
				$ikey=array();
				$ival=array();

				//既存の画像の削除
				$ret=$commonDao->del_Data("general_images",array("image_type","parent_no"),array("PROJECT_POSTER",$item[no]));

				$ikey[]="image_type";
				$ival[]="PROJECT_POSTER";
				$ikey[]="parent_no";
				$ival[]=$item[no];
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$item[no]."_mvp_".$item[new_movie2];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);
			}

			//動画（YouTube）の登録
			if ($item[movie_type]==2){

				$ikey[]="image_type";
				$ival[]="PROJECT_YOUTUBE";
				$ikey[]="parent_no";
				$ival[]=$item[no];
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$item[movie2][file_name];

				if($item[movie2][image_no]){
					//更新
					$ret=$commonDao->updateData("general_images",$ikey,$ival,"image_no",$item[movie2][image_no]);
				}else{
					$ikey[]="insert_date";
					$ival[]=date("Y-m-d H:i:s");
					$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

				}
			}
			//サムネイル
			if($item[new_cover_img]){
				$ikey=array();
				$ival=array();

				//既存の画像の削除
				$ret=$commonDao->del_Data("general_images",array("image_type","parent_no"),array("PROJECT_COVER",$item[no]));

				$ikey[]="image_type";
				$ival[]="PROJECT_COVER";
				$ikey[]="parent_no";
				$ival[]=$item[no];
				$ikey[]="parent_type";
				$ival[]="project";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_PROJECT";
				$ikey[]="file_name";
				$ival[]=$item[no]."_sn_".$item[new_cover_img];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

			}

			//支援のステータスの更新
			if($item[invest_status]){
				$sql="update sf_invest set status='{$item[invest_status]}'
					 where project_no='{$item[no]}' and status>=1 and status<=3";
				$ret=$commonDao->run_sql($sql);
			}

			// コミット
			$this->db->commit();

			$this->db->beginTransaction();
			if($item[new_cover_img]){
				$cover_img_name=$item[no]."_sn_".$item[new_cover_img];
				$sql="update sf_project set cover_img='{$cover_img_name}' where no={$item[no]}";
				$ret=$commonDao->run_sql($sql);
			}
			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 人材検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		$sql="select count(*) as cnt from sf_project  as p
			inner join sf_actress as a on p.project_owner=a.actress_no"
			.$tbl.$where;

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		if($search["no"]){
			$whTmp[]="no =".addslashes($search["no"]);
		}
		//
		if($search["member_id"]){
			$whTmp[]="member_id =".addslashes($search["member_id"]);
		}
		//email
		if($search["email"]){
			$whTmp[]="email ='".addslashes($search["email"])."'";
		}
		//pass
		if($search["password"]){
			$whTmp[]="password ='".addslashes(to_hash($search["password"]))."'";
		}
		//名前
		if($search["name"]){
			$tmp[]="name like '%".addslashes($search["name"])."%'";
			$tmp[]="public_name like '%".addslashes($search["name"])."%'";

			$whTmp[]="(".implode(" or ",$tmp).")";
		}
		//プロジェクトタイトル
		if($search["project_name"]){
			$whTmp[]="p.public_title like '%".addslashes($search["project_name"])."%'";
		}

		if($search["actress_no"]){
			$whTmp[]="project_owner =".addslashes($search["actress_no"]);
		}
		if($search["actress_name"]){
			$whTmp[]="a.public_name like '%".addslashes($search["actress_name"])."%'";
		}
		if($search["status"]){
			$whTmp[]="p.status ='".addslashes($search["status"])."'";
		}
		if($search["chk_status"]){
			$whTmp[]="p.chk_status ='".addslashes($search["chk_status"])."'";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}


		return array($where,$tbl);

	}

	/**
	 * プロジェクト検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="",$langcode="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {

			if(!$search["page"]) $search["page"]=1;

			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}
		$sql="select p.* ,p.admin_summary+p.now_summary as now_admin_summary, a.public_name as actress_name from sf_project as p
			inner join sf_actress as a on p.project_owner=a.actress_no "
			.$where.$ord.$limit_str;

		//print $sql."<br>";

		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;

	}



	/**
	 * Projectの削除
	 */
	public function delete($project_no) {
		$commonDao = new CommonDAO();

		$this->db->beginTransaction();
		try{

			$sql = "DELETE FROM sf_project WHERE no = {$project_no}";
			// 実行
			$this->executeUpdate($sql);

			//関連のあるものすべて削除

			//多言語情報の削除
			$sql = "DELETE FROM sf_project_detail WHERE project_no = {$project_no}";
			// 実行
			$this->executeUpdate($sql);

			//支援コースの削除
			$sql = "DELETE FROM sf_prj_present WHERE project_no = {$project_no}";
			// 実行
			$this->executeUpdate($sql);

			// 画像の削除
			$tmp=$commonDao->get_data_tbl("general_images",array("parent_type","parent_no"),array("project",$project_no));

			for($i=0;$i<count($tmp);$i++){
				if($tmp[$i][image_type]!="PROJECT_YOUTUBE"){//YouTubeの時はファイルがない
					unlink(constant($tmp[$i][dir]).$tmp[$i][file_name]);
				}
			}
			$sql = "DELETE FROM general_images WHERE parent_type ='project' and parent_no = {$project_no}";
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MSG_LEVEL_ERROR, "Failed to delete 'Project'. " . $e);
			$this->db->rollBack();
			throw new Exception($e);
			return false;
		}
		return true;
	}

	/**
	 * プロジェクト詳細説明の更新

	 */

	public function updateProjectDetail($data,$project_no,$lang) {
		$sql="update sf_project_detail set project_text='".$data."' where project_no=".$project_no." and lang='".$lang."'";

		//print $sql."<br>";
		try {
			$this->db->beginTransaction();

			// 実行
			//$this->executeUpdate($sql,$this->get_db());
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to Update." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

	/**
	 * 支援一覧情報取得
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function getInvestList($where="", $limit="", $order="") {
		if ($order == "") $order = " ORDER BY i.create_date ASC ";
		if ($where != "") $where = " WHERE ".$where;
		$sql = "SELECT
		            i.*,p.*,m.*,i.no as invest_no ,i.status as invest_status ,i.create_date as invest_date
				FROM
					sf_invest  i
				inner join sf_project p on i.project_no=p.no
				inner join sf_member m on i.member_id=m.user_no
				{$where}
				{$order}
				{$limit}
			 ";
		//print $sql."<br>";
		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}


		return $Arr;
	}

	/**
	 * 支援削除
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function DeleteInvest($invest_no,$project_no) {
		$commonDao = new CommonDAO();

		$this->db->beginTransaction();
		try{

			$tmp=$commonDao->get_data_tbl("sf_invest","no",$invest_no);
			$invest_data=$tmp[0];

			$sql = "DELETE FROM sf_invest WHERE no = {$invest_no}";
			// 実行
			$this->executeUpdate($sql);

			if($invest_data[status]>0){
				//プロジェクトの支援サマリーの更新
				$tmp=$commonDao->get_data_tbl("sf_project","no",$project_no);
				$project_data=$tmp[0];

				$summary=$project_data[now_summary]-$invest_data[invest_amount];
				$supporter=$project_data[now_supporter]-1;

				$fdata[now_summary]=$summary;
				$fdata[now_supporter]=$supporter;

				$where[no]=$project_no;

				$ret=$commonDao->updateData2("sf_project",$fdata,$where);
			}
			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MSG_LEVEL_ERROR, "Failed to delete 'Project'. " . $e);
			$this->db->rollBack();
			throw new Exception($e);
			return false;
		}
		return true;
	}

	/**
	 * プロジェクト プルダウン用の配列取得

	 */

	public function getProjectOptionArr() {
		$sql = "SELECT
		            no,public_title
				FROM
					sf_project
				WHERE status>0 and del_flg=0
				ORDER BY no desc
			 ";
		//print $sql."<br>";

		$ret = $this->executeQuery($sql);

		$Arr[""]="選択してください";
		if($ret){
			foreach($ret as $data){
				$Arr[$data[no]]=$data[no].":".$data[public_title];
			}
		}
		return $Arr;
	}
	/**
	 * プロジェクト プルダウン用の配列取得

	 */

	public function getProjectOrderOptionArr() {
		$sql = "SELECT no,public_title
				FROM
					sf_project
				WHERE status>0 and del_flg=0 and no not in
				(select link from mst_slide )
				ORDER BY no desc
			 ";
		//print $sql."<br>";

		$ret = $this->executeQuery($sql);

		$Arr[""]="選択してください";
		if($ret){
			foreach($ret as $data){
				$Arr[$data[no]]=$data[no].":".$data[public_title];
			}
		}
		return $Arr;
	}

	/**
	 * カテゴリ プルダウン用の配列取得

	 */

	public function getCategoryOptionArr() {
		$sql = "SELECT
		            no,category_name
				FROM
					sf_category
				WHERE del_flg=0
			 ";

		$ret = $this->executeQuery($sql);

		$Arr[""]="選択してください";
		if($ret){
			foreach($ret as $data){
				$Arr[$data[no]]=$data[category_name];
			}
		}
		return $Arr;
	}

}
?>