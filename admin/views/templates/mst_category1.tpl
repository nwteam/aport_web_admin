{include file="head.tpl"}
<script type="text/javascript">
{literal}

//編集
function edit( id )
{
	document.regForm.id.value = id;
	document.regForm.submit();
}
function del( id )
{
	if( !confirm( "指定のデータを削除します。\r\nよろしいですか？" ) )
	{
		return false;
	}
	document.regForm.action = "./master/category1/";
	document.regForm.id.value = id;
	document.regForm.exec.value = "delete";
	document.regForm.submit();
}
function mainup( id, val )
{
	document.regForm.action = "master/category1/";
	document.regForm.id.value = id;
	document.regForm.value.value = val;
	document.regForm.exec.value = "mainup";
	document.regForm.submit();
}
function maindown( id, val )
{
	document.regForm.action = "master/category1/";
	document.regForm.id.value = id;
	document.regForm.value.value = val;
	document.regForm.exec.value = "maindown";
	document.regForm.submit();
}
function middleup( id, val )
{
	document.regForm.action = "master/category1/";
	document.regForm.id.value = id;
	document.regForm.value.value = val;
	document.regForm.exec.value = "middleup";
	document.regForm.submit();
}
function middledown( id, val )
{
	document.regForm.action = "master/category1/";
	document.regForm.id.value = id;
	document.regForm.value.value = val;
	document.regForm.exec.value = "middledown";
	document.regForm.submit();
}
function subup( id, val )
{
	document.regForm.action = "master/category1/";
	document.regForm.id.value = id;
	document.regForm.value.value = val;
	document.regForm.exec.value = "subup";
	document.regForm.submit();
}
function subdown( id, val )
{
	document.regForm.action = "master/category1/";
	document.regForm.id.value = id;
	document.regForm.value.value = val;
	document.regForm.exec.value = "subdown";
	document.regForm.submit();
}
function addsub( id )
{
	document.regForm.action = "master/category1edit/";
	document.regForm.id.value = id;
	document.regForm.exec.value = "sub";
	document.regForm.submit();
}
function addmain()
{
	document.regForm.action = "master/category1edit/";
	document.regForm.exec.value = "main";
	document.regForm.submit();
}
function addmiddle(id)
{
	document.regForm.action = "master/category1edit/";
	document.regForm.id.value = id;
	document.regForm.exec.value = "middle";
	document.regForm.submit();
}


{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>職種管理</h3>


{if $msg }
<div><font color="red">{$msg}</font></div><br />
{/if}
<form action="master/category1edit" name="regForm" method="post">
<input type="button" value="メインカテゴリー追加" onclick="addmain();" />
<input type="hidden" name="id" />
<input type="hidden" name="value" />
<input type="hidden" name="exec" />

<!-- 一覧 -->
<table>
	<tr><th>カテゴリー名</th><th>上</th><th>下</th><!-- <th>上</th><th>下</th> --><th></th><th></th></tr>
{foreach from=$cateArr item="item"}

<tr>
<td>

{if $item.cflag==2}
&nbsp;&nbsp;└
{/if}

{if $item.cflag==3 && $item.subdown==1}
&nbsp;&nbsp;&nbsp;&nbsp;├
{/if}
{if $item.cflag==3 && $item.subdown==0}
&nbsp;&nbsp;&nbsp;&nbsp;└
{/if}
{$item.name}
</td>
<td>
{if $item.mainup==1}
<a href="javascript:void( 0 );" onclick="mainup({$item.id}, {$item.v_order} )">▲</a>
{/if}
</td>
<td>
{if $item.maindown==1}
<a href="javascript:void( 0 );" onclick="maindown({$item.id}, {$item.v_order} )">▼</a>
{/if}
</td>
<!--
<td>
{if $item.middleup==1}
<a href="javascript:void( 0 );" onclick="middleup({$item.id}, {$item.v_order} )">▲</a>
{/if}
</td>
<td>
{if $item.middledown==1}
<a href="javascript:void( 0 );" onclick="middledown({$item.id}, {$item.v_order} )">▼</a>
{/if}

<td>
{if $item.subup==1}
<a href="javascript:void( 0 );" onclick="subup({$item.id}, {$item.v_order} )">▲</a>
{/if}
</td>
<td>
{if $item.subdown==1}
<a href="javascript:void( 0 );" onclick="subdown({$item.id}, {$item.v_order} )">▼</a>
{/if}
</td>
  -->
<td><input type="button" value="変更" onclick="edit({$item.id})"></td>
<td>
{if $item.subflag!=1}
<input type="button" value="削除" onclick="del({$item.id});">
{/if}
</td>
<!--
<td>
{if $item.cflag==1}
<input type="button" value="中カテゴリー追加" onclick="addmiddle({$item.id});">
<input type="button" value="小カテゴリー追加" onclick="addsub({$item.id});">
{elseif $item.cflag==2}

<input type="button" value="小カテゴリー追加" onclick="addsub({$item.id});">
{/if}

</td>
 -->
</tr>
{/foreach}
</table>

<!-- 一覧 -->
<input type="button" value="メインカテゴリー追加" onclick="addmain();" />
</form>

{include file="footer.tpl"}
</body>
</html>
