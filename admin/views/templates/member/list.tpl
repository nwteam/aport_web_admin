{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#user_id").val("");
	$("#user_name").val("");
	$("#retire").attr("checked", false);

}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>会員一覧</h4>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="member/list">
		<table class="search">
		<tr>
			<th>名前で探す：</th>
			<td>
				<input type="text" name="name" id="user_name"  value="{$search.name}" size="20" />
			</td>
			<th>登録状況：</th>
			<td>
				<label><input type="checkbox" name="retire" id="retire"  value="1" {if $search.retire}checked="checked"{/if} />
				退会済みの会員も表示する
				</label>
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>

		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="member/list/">

			<table class="admins clear">
			<tr>
				<th width="100">会員No</th>
				<th width="120">ニックネーム</th>
				<th width="120">氏名</th>
				<th width="200">メールアドレス</th>
				<th width="60">登録日</th>
				<th width="60">登録状況</th>
				<th width="60">コメント</th>
				<th width="60">支援</th>
			 	<th width="40">退会</th>
			 <!-- 	<th width="40">削除</th> -->
			</tr>
			{foreach from=$members item="member"}
				<tr>
					<td><a href="member/edit/?user_no={$member.user_no}">{$member.user_no}</a></td>
					<td>{$member.nickname}</td>
					<td>{$member.name_1}{$member.name_2}</td>
					<td>{$member.email}</td>
					<td>{$member.create_date|date_format:"%Y/%m/%d"}</td>
					<td>
						{assign var="status" value=$member.status}
						{$statusArr.$status}
					</td>
					<td><a href="comment/list/?user_no={$member.user_no}">一覧</a></td>
					<td><a href="invest/list/?user_no={$member.user_no}">一覧</a></td>
					<td>
					{if $member.status<=1}
					<input type="submit" name="sbm_retire[{$member.user_no}]" value="退会処理" onClick="return confirm('退会処理を実行します。よろしいですか');">
					{/if}
					</td>
					<!--
					<td>
					{if $member.status!=1}
						<input type="submit" name="delete_dummy[{$member.user_no}]" value="削除" onClick="return confirm('会員情報を削除します。よろしいですか');">
					{/if}
					</td>
					 -->
				</tr>
			{foreachelse}
				<tr>
					<td colspan="12">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			<div class="center">
		<!--
				<input type="submit" name="sbm_update" value="一覧を更新する" onClick="return confirm('更新します。削除チェックの場合は、会員を削除します。');">
		 -->
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

