{include file="head.tpl"}

{literal}
<script type="text/javascript">
$(function(){
	$("#start").datepicker({ dateFormat: "yy/mm/dd" });
	$("#end").datepicker({ dateFormat: "yy/mm/dd" });
});

function CheckForm() {
	var name = document.getElementById('AdName').value;
	if( !name ) {
		alert('広告の名を入力してください'); document.getElementById('AdName').focus(); return false;	
	}
	
	var AdFile = document.getElementById('AdFile').value;
	if( !AdFile ) {
		alert('広告写真をアップロードしてください'); document.getElementById('AdFile').focus(); return false;		
	}
	
	var AdLink = document.getElementById('AdLink').value;
	if( !AdLink ) {
		alert('広告の接続を入力してください'); document.getElementById('AdLink').focus(); return false;		
	}
	
	var AdLink = document.getElementById('AdLink').value;
	if( !AdLink ) {
		alert('広告の接続を入力してください'); document.getElementById('AdLink').focus(); return false;		
	}
	
	var start = document.getElementById('start').value;
	if( !start ) {
		alert('広告時間を選択してください'); document.getElementById('start').focus(); return false;		
	}
	
	var end = document.getElementById('end').value;
	if( !end ) {
		alert('広告終瞭時間を選択してください'); document.getElementById('end').focus(); return false;		
	}
	return true;
}
</script>
{/literal}

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>広告管理</h3>
        <a href="/ad">←広告リストに戻る</a><br />
        <form name="regForm" method="post" onSubmit="return CheckForm(); false;" action="ad/add/" enctype="multipart/form-data" >
            <table class="admin_list01">
                <tr>
                    <th>※広告名</th>
                    <td><input type="text" id="AdName" name="name" value="" style="ime-mode:active;width:350px" /></td>
                </tr>
                <tr>
                    <th>※広告バナー</th>
                    <td><input type="file" id="AdFile" name="up_file"  size="30"/></td>
                </tr>
                <tr>
                    <th>※リンク先</th>
                    <td>
                        <input type="text" id="AdLink" name="link" value="" style="ime-mode:active;width:350px" />
                    </td>
                </tr>
                <tr>
                    <th>※表示/非表示</th>
                    <td>
                        <input type="radio" name="view_flg" value="1" checked />表示
                        &nbsp;<input type="radio" name="view_flg" value="0" />非表示
                    </td>
                </tr>
                <tr>
                    <th>位置（ について ）</th>
                    <td>
                        <input type="radio" name="p_point" value="0" checked />TOP
                        &nbsp;<input type="radio" name="p_point" value="1" />下層
                    </td>
                </tr>
                <tr>
                    <th>位置（ ぐらい ）</th>
                    <td>
                        <input type="radio" name="v_point" value="0" checked />右
                        &nbsp;<input type="radio" name="v_point" value="1" />左
                    </td>
                </tr>
                <tr>
                    <th>※掲載期間設定</th>
                    <td>
                        <input type="text" id="start" name="view_start" size="25" value="" style="ime-mode:disabled;"/>
                        ～
                        <input type="text" id="end" name="view_end" size="25" value="" style="ime-mode:disabled;"/>
                    </td>
                </tr>
                <tr>
                    <th>掲載料金</th>
                    <td><input type="text" name="fee" value=""  /></td>
                </tr>
            </table>
            <p class="center">
                <input type="submit" name="sublimit" value="OK" />
                <input name="cancel" type="button" value="キャンセル" onClick="window.location.href='/ad/'" />
            </p>
        </form>
    </div>
</div>

{include file="footer.tpl"}
</body>
</html>
