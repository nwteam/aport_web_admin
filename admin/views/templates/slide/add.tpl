{include file="head.tpl"}

{literal}
<script type="text/javascript">

</script>
{/literal}

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>トップページ表示順管理</h3>
	</div>
    <form name="regForm" method="post" onSubmit="return CheckForm(); false;" action="slide/add/" enctype="multipart/form-data" >
        <a href="/slide/list/">←トップページ表示順リストに戻る</a><br />
		<table class="admin_list01">
            <tr>
                <th>プロジェクト</th>
                <td>{html_options name=link options=$ProjectArr selected=$input_data.link}</td>
            </tr>
		</table>
        <p class="center">
            <input type="submit" name="sublimit" value="OK" />
            <input name="cancel" type="button" value="キャンセル" onClick="window.history.go(-1)" />
        </p>
	</form>
</div>

{include file="footer.tpl"}
</body>
</html>
