<?php
class MemberController extends AppRootController {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();

		require_once sprintf("%s/dao/MemberDao.class.php", MODEL_PATH);
/*
		require_once sprintf("%s/structure/member/Member.class.php", MODEL_PATH);
		require_once sprintf("%s/structure/member/MemberPassword.class.php", MODEL_PATH);
		require_once sprintf("%s/structure/member/MemberExtAttr.class.php", MODEL_PATH);
		require_once sprintf("%s/condition/member/MemberSearchCondition.class.php", MODEL_PATH);
		require_once sprintf("%s/dao/member/MemberDao.class.php", MODEL_PATH);
		require_once sprintf("%s/dao/member/MemberPasswordDao.class.php", MODEL_PATH);
		require_once sprintf("%s/dao/member/MemberExtAttrDao.class.php", MODEL_PATH);
*/


	}

	/**
	 * 会員一覧表示・検索
	 */
	public function listAction() {

		$search = null;
		$search_flag = false;
		$limit=ADMIN_V_CNT;

		$dao = new MemberDao();
		$commonDao = new CommonDao();

		$page = $_REQUEST["page"];

		// 検索送信ボタンが押下されているか？
		if (isset($_POST["sbm_search"])) {

			$search[scholar_id]=$_POST[scholar_id];
			$search[name]=$_POST[name];
			$search[retire]=$_POST[retire];
			$search[page]=1;

			$_SESSION[search_condition]=$search;


		}
		// ページ番号が渡されたか？
		else if ($page) {
			// ページングによる遷移
			$search = $_SESSION["search_condition"];
			$search[page]=$this->request->getParam("page");

		}else if($_POST[sbm_update]){	//一括更新
			//凍結/開始処理
			if($_POST[stop_dummy]){

				foreach($_POST[stop_dummy] as $key=>$val){
					//削除
					$ret=$commonDao->updateData("sf_member","stat",$val,"member_id",$key);
					if(!$ret){
						$delFlg=1;
					}
				}
				if($delFlg==1){
					$this->addMessage("info","更新エラーがあります。");
				}
				else{
					$this->addMessage("info","更新しました");
				}
			}

			//削除処理
			if($_POST[delete_dummy]){

				foreach($_POST[delete_dummy] as $key=>$val){
					//削除
					//削除
					$ret=$dao->delete($val);
					if(!$ret){
						$delFlg=1;
					}

				}
				if($delFlg==1){
					$this->addMessage("info","会員削除エラーがあります。");
				}
				else{
					$this->addMessage("info","チェックした会員を削除しました");
				}

			}

		}else if($_POST[sbm_retire]){	//退会処理
				foreach($_POST[sbm_retire] as $key=>$val){
					//会員情報取得
					$ret=$commonDao->get_data_tbl("sf_member","user_no",$key);
				//	print_r_with_pre($ret[0]);
					//ステータス・会員情報更新
					$data[member_name]="退会済み_".$ret[0][member_name];
					$data[nickname]="退会済み_".$ret[0][nickname];
					$data[email]="退会済み_".$ret[0][email];
					if($ret[0][facebook_id]!=""){
						$data[facebook_id]="退会済み_".$ret[0][facebook_id];
					}
					if($ret[0][twitter_id]!=""){
						$data[twitter_id]="退会済み_".$ret[0][twitter_id];
					}
					$data[status]=9;

					$where[user_no]=$key;
					$ret=$commonDao->updateData2("sf_member", $data, $where);
					if(!$ret){
						$retire_Flg=1;
					}

					if($retire_Flg==1){
						$this->addMessage("info","退会処理中にエラーが発生しました");
					}
					else{
						$this->addMessage("info","退会処理を実行しました");
					}
				}

		}
		else {
			// sessionに検索条件が保存されている場合

			if($_SESSION["search_condition"]) unset($_SESSION["search_condition"]);
			$search[page]=1;

		}

		$total_cnt=$dao->searchCount($search);
		if($total_cnt>$limit){
			$page_navi = get_page_navi2($total_cnt, $limit, $search[page], "/member/list/");
		}
		$members=$dao->search($search,"create_date desc",$limit);

		$statusArr=CommonArray::$member_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);

		$this->view->assign("members", $members);
		$this->view->assign("total_cnt", $total_cnt);
		$this->view->assign("navi", $page_navi);
		$this->view->assign("search", $search);


		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("member/list.tpl");
		return;
	}

	/**
	 * 会員情報更新
	 */
	public function editAction() {

		$dao = new MemberDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$admin = $this->getAdminSession();
		$user_no=$_REQUEST[user_no];

		//config
		$prefArr=CommonArray::$pref_text_array;

		if($_POST[modify]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];

			//---------------- 入力チェック ---------------------------
			//基本事項
			$baseData=CommonChkArray::$memberRegistCheckData;
			$this->check($input_data,$baseData);
			//-------------- ここまで -----------------------------------

			if (count($this->getMessages()) >0) {

				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}
					//	print_r_with_pre($result_messages);

				$this->view->assign("result_messages", $result_messages);
			}
			else {

				//基本事項
				foreach($baseData[dbstring] as $key=>$val){
					$dkey[]=$key;
					$dval[]=$input_data[$key];
				}
				//パスワード
				if($input_data[new_password]!=""){
					$dkey[]="password";
					$dval[]=to_hash($input_data[new_password]);
				}
				$ret=$dao->upItemData($dkey,$dval,"user_no",$user_no);
				if($ret){
					$this->addMessage("info","ユーザー情報を更新しました");
				}
				else{
					$this->addMessage("info","ユーザー情報の更新エラーです");
				}
			}
		}
		else{

			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("sf_member","user_no",$user_no);
			$input_data=$tmp[0];
		}


		//年月日プルダウン
		$yearArr=makeYearList("1940",0,1);
		$monthArr=makeMonthList(1);
		$dayArr=makeDayList(1);

		$this->view->assign("yearArr", $yearArr);
		$this->view->assign("monthArr", $monthArr);
		$this->view->assign("dayArr", $dayArr);

		$this->view->assign("msg", $msg);
		$this->view->assign("input_data", $input_data);
		$this->view->assign("prefArr", $prefArr);


		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("member/edit.tpl");
		return;
	}



}
?>

