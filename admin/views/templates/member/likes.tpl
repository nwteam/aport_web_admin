{include file="head.tpl"}
<script type="text/javascript">
{literal}


{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>ユーザー</h3>

<h4>{$memArr.name}さんのいいね履歴</h4>
{include file="messages.tpl"}

		←<a href="/member/list">ユーザー一覧へ</a>

		{* 検索結果 *}

<!--<p>{$memArr.name}さんの&nbsp;いいね記録</p> -->

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>

			<form name="fm_list" id="fm_list" method="POST" action="pay/history/">

			<table class="admins_m clear">
			<tr>
				<th width="80">FacebookID</th>
				<th width="200">Facebookページ名</th>
				<th width="100">オーナー名</th>
				<th width="100"></th>
				<th width="80">日付</th>
			</tr>
			{foreach from=$arr item="item"}
				<tr>
					<td>{$item.fbpage_id}</td>
					<td>{$item.fbpage_name}</td>
					<td><a href="/company/edit/?owner_id={$item.owner_id}">{$item.owner_name}</a></td>
					<td>{if $item.stat>=0}いいね{else}いいね→いいね取消{/if}</td>
					<td>{$item.insert_date}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="5">いいね履歴はありません。</td>
				</tr>
			{/foreach}
			</table>

			</form>

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

