{include file="head.tpl"}
<script type="text/javascript">
{literal}

/**
 * 入力チェック
 */
function validate() {
	var msg = "";

	if (msg != "") {
		alert(msg);
		return false;
	}
	return true;
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}

	<div id="main_content">
		<h3>システム設定一覧</h3>
		{include file="messages.tpl"}
		<form method="post" name="fm" action="siteConfig">
			<input type="hidden" name="submit_data" value="t"/>
			<table class="edit">
			{foreach from=$config_list item="config"}
				<tr>
					<th>{$config.config_name}</th>
					<td>
						<input type="text" name="config_value[]" value="{$config.config_value}" size="{$config.config_length}"/>
						<br />
						{$config.config_explain}
					</td>
					<input type="hidden" name="config_key[]" value="{$config.config_key}"/>
				</tr>
			{foreachelse}
			<tr>
				<td colspan="2">システム設定データがありません。</td>
			</tr>
			 {/foreach}
			</table>
			<div class="center">
				<button type="submit" onclick="return validate()">更新</button>
			</div>
		</form>
	</div>
</div>
{include file="footer.tpl"}

</body>
</html>

