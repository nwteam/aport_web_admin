<?php
class ColumnController extends AppRootController {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();

	}

	/**
	 * コラム一覧表示・検索
	 */
	public function listAction() {

		$search = null;
		$search_flag = false;
		$limit=ADMIN_V_CNT;

		$dao = new ColumnDao();
		$commonDao = new CommonDao();

		$page = $_REQUEST["page"];

		//言語コード
		//$lang_code=MAIN_LANGUAGE;
		//$this->view->assign("lang_code", $lang_code);

		// 検索送信ボタンが押下されているか？
		if (isset($_POST["sbm_search"])) {

			$search[id]=$_POST[id];
			$search[column_title]=$_POST[column_title];

			$_SESSION[search_condition]=$search;
		}
		// ページ番号が渡されたか？
		else if ($page) {
			// ページングによる遷移
			$search = $_SESSION["search_condition"];
			$search[page]=$this->request->getParam("page");

		}else if($_POST[sbm_update]){	//一括更新
			//削除処理
			if($_POST[delete_dummy]){
				foreach($_POST[delete_dummy] as $key=>$val){
					//削除
					$ret=$dao->delete($val);
					if(!$ret){
						$delFlg=1;
					}
				}
				if($delFlg==1){
					$this->addMessage("info","コラム削除エラーがあります。");
				}
				else{
					$this->addMessage("info","チェックしたコラムを削除しました");
				}

			}
		}
		else {
			// sessionに検索条件が保存されている場合
			if($_SESSION["search_condition"]) unset($_SESSION["search_condition"]);
			$search[page]=1;
		}
		$total_cnt=$dao->searchCount($search);
		if($total_cnt>$limit){
			$page_navi = get_page_navi2($total_cnt, $limit, $search[page], "/column/list/");
		}
		//print_r_with_pre($search);

		$column_list=$dao->search($search,"insert_time desc",$limit);

//		$entryArr=CommonArray::$entry_type_array;	//登録タイプ
//		$this->view->assign("entryArr", $entryArr);
//		$statusArr=CommonArray::$column_status_array;	//ステータス
//		$this->view->assign("statusArr", $statusArr);

		$this->view->assign("column_list", $column_list);
		$this->view->assign("total_cnt", $total_cnt);
		$this->view->assign("navi", $page_navi);
		$this->view->assign("search", $search);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("column/list.tpl");
		return;
	}

	/**
	 * コラム基本情報更新
	 */
	public function editBasicAction() {

		$dao = new ColumnDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);

		$column_id=$_REQUEST[id];
		//$lang_code=$_REQUEST[lang];
		//$this->view->assign("lang_code", $lang_code);

		if($_POST[submit]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//print_r_with_pre($input_data);
			//print_r_with_pre($_FILES);

			//トップの画像ファイルアップ
			if(is_uploaded_file($_FILES["up_file4"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."_".$_FILES["up_file4"]["name"];//
				$_SESSION[TMP_FUP4]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				copy($_FILES["up_file4"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);

				$input_data[new_cover_img]=$temp_up_fname;
			}elseif($_SESSION[TMP_FUP4]!=""){
				$input_data[new_cover_img]=$_SESSION[TMP_FUP4];
			}
			//---------------- 入力チェック ---------------------------
			//基本事項
			$baseData=CommonChkArray::$columnRegistBasicCheckData;
			$this->check($input_data,$baseData);

			//トップの画像
			if($input_data[new_cover_img]=="" && $input_data[cover_img][file_name]==""){
				$this->addMessage("cover_img","トップの画像を指定してください。");
			}

			//-------------- 入力チェックここまで -----------------------------------
			if (count($this->getMessages()) >0) {
				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}

				$this->view->assign("result_messages", $result_messages);
			}
			else {

				//基本事項
				foreach($baseData[dbstring] as $key=>$val){
					$dkey[]=$key;
					$dval[]=$input_data[$key];
				}
				//print_r_with_pre($input_data);die;
				if($input_data[id]){//修正
					$dkey[]="update_time";
					$dval[]=date("Y-m-d H:i:s");

					$ret=$dao->upItemData($dkey,$dval,"id",$input_data[id],$input_data);
					if($ret){
						//====== 画像 正式アップ & 画像リサイズ作業 ===================
						//トップの画像
						if($_SESSION[TMP_FUP4]){
							$file_name=$input_data[id]."_cm_".$_SESSION[TMP_FUP4];
							copy(DIR_IMG_TMP.$_SESSION[TMP_FUP4],DIR_IMG_COLUMN.$file_name);
							//古い画像を削除
							if($input_data[top_img][file_name]){
								unlink(DIR_IMG_COLUMN.$input_data[top_img][file_name]);
							}
						}

						//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
						if($_SESSION[TMP_FUP4]){
							unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP4]);
							$input_data[top_img][file_name]=$input_data[id]."_cm_".$_SESSION[TMP_FUP4];
						}
						//セッションクリア
						$_SESSION[TMP_FUP4]="";
						$this->addMessage("info","コラム基本情報を保存しました");
					}
					else{
						$this->addMessage("error","コラム基本情報の保存エラーです");
					}
				}
				else{
					//----------- 新規登録 ------------
					$dkey[]="insert_time";
					$dval[]=date("Y-m-d H:i:s");

					$ret=$dao->InsertItemData($dkey, $dval,$input_data);

					if($ret){
						$input_data[id]=$ret;
						//====== 画像 正式アップ & 画像リサイズ作業 ===================
						//トップの画像
						if($_SESSION[TMP_FUP4]){
							//サイズ取得
							$photoArr=CommonArray::$topimgsize_array;
							$moto=DIR_IMG_TMP.$_SESSION[TMP_FUP4];
							$newDir=DIR_IMG_COLUMN.$input_data[id]."_cm_".$_SESSION[TMP_FUP4];
							//画像をリサイズして正式アップ
							resize_image2($moto,$newDir,$photoArr[top_img][w],$photoArr[top_img][h]);
							//仮画像削除
							unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP4]);
							$input_data[top_img][file_name]=$input_data[id]."_cm_".$_SESSION[TMP_FUP4];
						}
						//セッションクリア
						$_SESSION[TMP_FUP4]="";
						$this->addMessage("info","コラム基本情報を登録しました");
					}
					else{
						$this->addMessage("error","コラム基本情報の登録エラーです");
					}
				}

				$this->view->assign("finish_flg", 1);
				foreach($this->getMessages() as $edit_msg){
					$edit_messages[$edit_msg->getMessageLevel()]=$edit_msg->getMessageBody();
				}

				$this->view->assign("edit_messages", $edit_messages);
			}
		}
		else if($column_id){
			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("sf_column","id",$column_id);

			if($tmp){
				$input_data=$tmp[0];
				$db_data=$tmp[0];

				//トップの画像
				$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("COLUMN_COVER",$input_data[id]));
				$input_data[cover_img]=$tmp[0];
			}
			else{
				$this->addMessage("error","該当するコラムはありません");
        		$this->setTemplatePath("error.tpl");
				return;
			}
			$_SESSION[TMP_FUP4]="";
		}
		else{
			//新規登録
			//デフォルト
			$input_data[lang]=MAIN_LANGUAGE;
			$_SESSION[TMP_FUP4]="";
		}

		//カテゴリリスト
		$CategoryArr=$dao->getCategoryOptionArr();
		$this->view->assign("CategoryArr", $CategoryArr);

//		print_r_with_pre($input_data);
//		print_r_with_pre($_SESSION[TMP_FUP]);

		if($input_data[id]){
			$this->view->assign("mode", "edit");
			$this->view->assign("mode_str", "編集");
		}else{
			$this->view->assign("mode", "add");
			$this->view->assign("mode_str", "新規登録");
		}

		//$input_data[lang]=$lang_code;

		$this->view->assign("input_data", $input_data);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("column/edit_basic.tpl");

		return;
	}

	/**
	 * コラム詳細説明更新
	 */
	public function editDetailAction() {

		$dao = new ColumnDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);


		$column_id=$_REQUEST[id];
		//$lang_code=$_REQUEST[lang];
		//$this->view->assign("lang_code", $lang_code);

		if($_POST[exec]=="save"){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//print htmlspecialchars($input_data[column_text]) ;
			//---------------- 入力チェック ---------------------------
			//基本事項

			//-------------- ここまで -----------------------------------
			if (count($this->getMessages()) >0) {
				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}
				$this->view->assign("result_messages", $result_messages);
			}
			else {
				$ret=$commonDao->updateData("sf_column","contents",$input_data[contents],array("id"),array($input_data[id]));
				if($ret){
					$this->addMessage("info","コラム詳細説明を保存しました");
				}
				else{
					$this->addMessage("error","コラム詳細説明の保存エラーです");
				}
				$this->view->assign("finish_flg", 1);
				foreach($this->getMessages() as $edit_msg){
					$edit_messages[$edit_msg->getMessageLevel()]=$edit_msg->getMessageBody();
				}

				$this->view->assign("edit_messages", $edit_messages);

				//画像情報取得
				//画像
				$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("COLUMN_DETAIL",$input_data[id]));
				$input_data[image_list]=$tmp;

			}
		}
		else if($_POST[exec]=="delete"){	//削除
			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];

			$image_data=$commonDao->get_data_tbl("general_images","image_no",$_POST[image_no]);
			$ret=$commonDao->del_Data("general_images","image_no",$_POST[image_no]);
			//画像を削除
			unlink(DIR_IMG_COLUMN.$image_data[0][file_name]);
			//$this->addMessage("info","画像を削除しました");

			//画像情報取得
			//画像
			$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("COLUMN_DETAIL",$input_data[id]));
			$input_data[image_list]=$tmp;
		}
		else if($column_id){
			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("sf_column",array("id"),array($column_id));

			if($tmp){
				$input_data=$tmp[0];
				$input_data[contents]=htmlspecialchars_decode($input_data[contents]);
				$db_data=$tmp[0];

				//画像情報取得
				//画像
				$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("COLUMN_DETAIL",$input_data[id]));
				$input_data[image_list]=$tmp;
			}
			else{
				//新規言語？
				$this->addMessage("error","該当するコラムはありません");
        		$this->setTemplatePath("error.tpl");
				return;

			}
			if($_GET[ibk] && $_SESSION[contents]!=""){
				$input_data[contents]=$_SESSION[contents];
			}
			$_SESSION[contents]="";
			$_SESSION["input_data"]="";
		}
		else{
			//新規登録

			//デフォルト
			$input_data[lang]=MAIN_LANGUAGE;
			$_SESSION[TMP_FUP]="";
			$_SESSION[TMP_FUP2]="";
			$_SESSION[contents]="";
			$_SESSION["input_data"]="";
		}

//		print_r_with_pre($input_data);
//		print_r_with_pre($_SESSION[TMP_FUP]);
		//print_r_with_pre($input_data);
		$this->view->assign("input_data", $input_data);

		//コラム情報
		$column_info=$commonDao->get_data_tbl("sf_column","id",$column_id);
		$this->view->assign("column_info", $column_info[0]);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		// テンプレート表示
		$this->setTemplatePath("column/edit_detail.tpl");

		return;
	}

	/**
	 * コラム詳細用画像編集
	 */
	public function editImageAction() {

		$dao = new ColumnDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);

		$column_id=$_POST[id];
		//$lang_code=$_POST[lang];
		$image_no=$_POST[image_no];
		$this->view->assign("id", $column_id);
		//$this->view->assign("lang_code", $lang_code);

		if($_POST[sbm_save]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//---------------- 入力チェック ---------------------------
			//基本事項
			//-------------- ここまで -----------------------------------
			//ファイルアップ
			if(is_uploaded_file($_FILES["up_file"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."_".$_FILES["up_file"]["name"];//
				$_SESSION[TMP_FUP]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				copy($_FILES["up_file"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
			}

			//入力チェック
			if(!$_SESSION[TMP_FUP] && $input_data[file_name]==""){
				$this->addMessage("file","画像をアップしてください");
			}else{
				//編集の場合で画像を選択していないので一覧に戻る
				header("Location:  /column/editDetail/?id=$column_id&ibk=1");

			}
			if (count($this->getMessages()) >0) {

				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}

				$this->view->assign("result_messages", $result_messages);
			}
			else {
				//ファイル名の指定
				$file_name=$input_data[id]."_cdtl_".$_SESSION[TMP_FUP];

				//スライド登録
				if(!$image_no){
					//新規登録
					$ikey[]="image_type";
					$ival[]="COLUMN_DETAIL";
					$ikey[]="parent_no";
					$ival[]=$input_data[id];
					$ikey[]="parent_type";
					$ival[]="column";
					$ikey[]="sort";
					$ival[]=1;
					$ikey[]="dir";
					$ival[]="DIR_IMG_COLUMN";
					$ikey[]="file_name";
					$ival[]=$file_name;

					$ikey[]="insert_date";
					$ival[]=date("Y-m-d H:i:s");

					$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

				}
				else{
					//変更処理
					$ret=$commonDao->updateData("general_images", "file_name", $file_name, "image_no", $input_data[image_no]);
				}

				$upErrFlg="1";
				if($ret && $_SESSION[TMP_FUP]){
					//画像正式アップ(幅740より大きい場合はリサイズする）
					$max_width="600";
					// 画像のサイズとタイプを取得
					list($s_width,$s_height, $type) = getimagesize(DIR_IMG_TMP.$_SESSION[TMP_FUP]);
					if($s_width>$max_width){
						$moto=DIR_IMG_TMP.$_SESSION[TMP_FUP];
						$newDir=DIR_IMG_COLUMN.$file_name;
						//画像をリサイズして正式アップ
						resize_image2($moto,$newDir,$max_width);

					}else{
						copy(DIR_IMG_TMP.$_SESSION[TMP_FUP],DIR_IMG_COLUMN.$file_name);
					}
					unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP]);
					//古い画像を削除
					if($input_data[file_name]!=""){
						unlink(DIR_IMG_COLUMN.$input_data[file_name]);

					}
					$this->addMessage("info","画像を保存しました");
					header("Location:  /column/editDetail/?id=$column_id&ibk=1");
				}else if(!$ret){

					$upErrFlg=99;

				}

				if($_SESSION[TMP_FUP]) unset($_SESSION[TMP_FUP]);


			}
		}
		else if($image_no){
			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("general_images","image_no",$image_no);

			if($tmp){
				$input_data=$tmp[0];

			}
			else{
				$this->addMessage("error","該当する画像はありません");
        		$this->setTemplatePath("error.tpl");
				return;

			}

			$input_data[id]=$column_id;
			//$input_data[lang]=$lang_code;
			$_SESSION[contents]=$_POST[contents];
			$_SESSION[TMP_FUP]="";

		}
		else{
			//新規登録
			$input_data[id]=$column_id;
			//$input_data[lang]=$lang_code;
			$_SESSION[contents]=$_POST[contents];
			$_SESSION[TMP_FUP]="";
		}


		if($input_data[image_no]){
			$this->view->assign("mode", "edit");
			$this->view->assign("mode_str", "編集");
		}else{
			$this->view->assign("mode", "add");
			$this->view->assign("mode_str", "新規登録");
		}

		$this->view->assign("input_data", $input_data);

		//コラム情報
		$column_info=$commonDao->get_data_tbl("sf_column","id",$column_id);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("column/edit_image.tpl");

		return;
	}
}
?>

