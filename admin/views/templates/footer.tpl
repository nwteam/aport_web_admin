<div style="clear:both;"><br /></div>
<div id="footer">{$smarty.const.FOOTER_COPYRIGHT}</div>
{literal}
<script type="text/javascript">
	$(document).ready(function() {
		$("#jMenu").jMenu({
			openClick : false,
			 TimeBeforeOpening : 100,
			TimeBeforeClosing : 11,
			animatedText : false,
			paddingLeft: 1,
			effects : {
				effectSpeedOpen : 150,
				effectSpeedClose : 150,
				effectTypeOpen : 'slide',
				effectTypeClose : 'slide',
				effectOpen : 'swing',
				effectClose : 'swing'
			}

		});
	});
	$(function() {
		 $('#tablefix').tablefix({ height: 600, fixRows: 1});
	});
</script>
{/literal}