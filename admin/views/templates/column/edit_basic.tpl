{include file="head.tpl"}
<script language="JavaScript">
<!--
{literal}

{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h4>コラム {if $mode=="add"}新規登録{else}【{$input_data.title}】{/if}</h4>
			←<a href="/column/list/">コラム一覧へ</a>
		<ul class="tab clearfix mt20">
			<li ><a href="javascript:void(0);" style="background-color:#656d78;color:#FFF;">基本情報</a></li>
			{if $input_data.id}
			<li><a href="/column/editDetail/?id={$input_data.id}">コラム詳細説明</a></li>
			{/if}
		</ul>
		<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
		{*include file="messages.tpl"*}
		<form action="" method="post"  enctype="multipart/form-data">
		<div class="left w50">
			<h5 class="project_image">基本情報</h5>
			<table class="w100">
				<tr>
					<th>コラムNo</th>
					<td>{$input_data.id}</td>
					<input type="hidden" name="id" value="{$input_data.id}"  />
					<input type="hidden" name="lang" value="{$input_data.lang}"  />
				</tr>
				<tr>
					<th>タイトル<span class="red">※</span></th>
					<td>
						{if $result_messages.title}
							<span class="red">{$result_messages.title}</span><br />
						{/if}
						<input class="long" type="text" name="title" value="{$input_data.title}"/>
					</td>
				</tr>
				<tr>
					<th>カテゴリ<span class="red">※</span></th>
					<td>
						{if $result_messages.column_category_no}
						<span class="red">{$result_messages.column_category_no}</span><br />
						{/if}
						{html_options name=column_category_no options=$CategoryArr selected=$input_data.column_category_no}
					</td>
				</tr>
				<tr>
					<th>meta keywords<span class="red">※</span></br>(for SEO)</th>
					<td>
						{if $result_messages.seo_keywords}
							<span class="red">{$result_messages.seo_keywords}</span><br />
						{/if}
						<input class="long" type="text" name="seo_keywords" value="{$input_data.seo_keywords}"/>
					</td>
				</tr>
				<tr>
					<th>meta description<span class="red">※</span></br>(for SEO)</th>
					<td>
						{if $result_messages.seo_description}
							<span class="red">{$result_messages.seo_description}</span><br />
						{/if}
						<input class="long" type="text" name="seo_description" value="{$input_data.seo_description}"/>
						</br>(推奨100文字程度)
					</td>
				</tr>
				<tr>
					<th>タグ <span class="red">任意</span></th>
					<td>
						{if $result_messages.tag_list}
							<span class="red">{$result_messages.tag_list}</span><br />
						{/if}
						<input class="long" type="text" name="tag_list" value="{$input_data.tag_list}"/>
						</br>半角カンマ区切りでタグを記述してください。
					</td>
				</tr>
				<tr>
					<th>登録日時</th>
					<td>
						{if $input_data.insert_time>0}{$input_data.insert_time|date_format:"%Y-%m-%d %H:%M:%S"}{/if}
					</td>
				</tr>
				<tr>
					<th>更新日時</th>
					<td>
						{if $input_data.update_time>0}{$input_data.update_time|date_format:"%Y-%m-%d %H:%M:%S"}{/if}
					</td>
				</tr>
			</table>
		</div>
		<div class="right w50">
			<h5 class="project_image">トップの画像</h5>
			<table class="w100">
				<tr>
				<th >画像<span class="red">※</span></th>
				<td>
					{if $result_messages.cover_img}
						<span class="red">{$result_messages.cover_img}</span><br />
					{/if}
					{if $smarty.session.TMP_FUP4!=""}
						<img src="{$smarty.const.URL_IMG_TMP}{$smarty.session.TMP_FUP4}" width=540 height=204 />
					{elseif $input_data.cover_img.file_name!=""}
						<img src="{$smarty.const.URL_IMG_COLUMN}{$input_data.cover_img.file_name}?{$smarty.now}" width=540 height=204 />
						<input type="hidden" name="cover_img[file_name]" value="{$input_data.cover_img.file_name}" />
						<input type="hidden" name="cover_img[image_no]" value="{$input_data.cover_img.image_no}" />
					{/if}
					<input type="file" name="up_file4"  size="30"/>
					<br />トップの画像(600px x 227pxが標準サイズです)
				</td>
				</tr>
			</table>
		</div>
		<div class="clear"> </div>
		{*if $finish_flg!=1*}
			<div class="submit center">
				<input type="submit"  name="submit" value="保存" />
			</div>
		{*/if*}
		</div>
		</form>
	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
