{include file="head.tpl"}
<script language="JavaScript">
<!--
{literal}


{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>コメント編集</h4>

		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
{include file="messages.tpl"}

		←<a href="/comment/list/?bk=1">コメント一覧へ</a>

<form action="" method="post"  >
      <table class="w01">
        <tr>
          <th>No</th>
          <td>{$input_data.no}</td>
        </tr>
        <tr>
          <th>プロジェクト名</th>
          <td>{$input_data.project_name}</td>
        </tr>
        <tr>
          <th>ニックネーム</th>
          <td>{$input_data.nickname}</td>
        </tr>
		<tr>
			<th>コメント</th>
			<td>
			<textarea name="comment" cols="100" rows="5">
{$input_data.comment}
			</textarea></td>
		</tr>
	</table>

      <div class="submit">
        <input type="submit"  name="modify" value="更新" />
        <input type="hidden" name="no" value="{$input_data.no}">
        <input type="hidden" name="project_name" value="{$input_data.project_name}">
        <input type="hidden" name="nickname" value="{$input_data.nickname}">
      </div>
    </div>
</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
