{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#project_no").val("");
	$("#project_name").val("");
	$("#actress_no").val("");
	$("#actress_name").val("");
}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>プロジェクト一覧</h4>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="project/list">
		<table class="search">
		<tr>
			<th>プロジェクトNo：</th>
			<td>
				<input type="text" name="project_no" id="project_no"  value="{$search.project_no}" size="4" />
			</td>
			<th>プロジェクト名：</th>
			<td>
				<input type="text" name="project_name" id="project_name"  value="{$search.project_name}" size="20" />
			</td>
		</tr>
		<tr>
			<th>起案者No：</th>
			<td>
				<input type="text" name="actress_no" id="actress_no"  value="{$search.actress_no}" size="4" />
			</td>
			<th>起案者名：</th>
			<td>
				<input type="text" name="actress_name" id="actress_name"  value="{$search.actress_name}" size="20" />
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>
		<a class="btn" href="/project/editBasic/?lang={$lang_code}&actress_no={$search.actress_no}">新規作成</a>
		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="right red">※支援されているプロジェクトを削除することはできません。</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="project/list/">

			<table class="admins clear">
			<tr>
				<th width="20">no</th>
				<th width="120">プロジェクト名</th>
				<th width="200">起案者名</th>
				<th width="30">受付終了日</th>
				<th width="50">目標金額</th>
				<th width="50">現在の支援金額</th>
				<th width="50">ステータス</th>
				<th width="60">支援コース</th>
				<th width="60">コメント</th>
				<th width="60">支援</th>
				<th width="40">削除</th>
			</tr>
			{foreach from=$project_list item="project"}
				<tr>
					<td><a href="project/editBasic/?no={$project.no}&lang={$lang_code}">{$project.no}</a></td>
					<td>{$project.public_title}</td>
					<td><a href="/actress/edit/?actress_no={$project.project_owner}&lang={$lang_code}">{$project.project_owner}:{$project.actress_name}</a></td>
					<td>{$project.invest_limit|date_format:"%Y/%m/%d"}</td>
					<td style="text-align: right">{$project.wish_price|number_format}円</td>
					<td style="text-align: right">
						{$project.now_summary|number_format}円
					</td>
					<td>
						{*if $project.status<=1*}
<!--
							{assign var="project_no" value=$project.project_no}
							{html_options name=status[$project_no] options=$statusArr selected=$project.status}
         					<input type="hidden" name="status_old[{$project_no}]" value="{$input_data.status}"  /></td>
 -->
  						{*else*}
							{assign var="status" value=$project.status}
							{$statusArr.$status}
						{*/if*}
					</td>
					<td>
					<a href="project/present/?no={$project.no}&lang={$lang_code}">一覧</a>
					</td>
					<td>
					<a href="/comment/list/?project_no={$project.no}">一覧</a>
					</td>
					<td>
					{if $project.status>=1}
					<a href="project/invest/?no={$project.no}&lang={$lang_code}">{$project.now_supporter}件</a>
					{/if}
					</td>
					<td style="text-align: center">
					<input type="checkbox" name="delete_dummy[]" value="{$project.no}" {if $project.now_supporter>0}disabled="disabled"{/if}/>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="11">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			<div class="center">
				<input type="submit" name="sbm_update" value="一覧を更新する" onClick="return confirm('更新します。削除チェックの場合は、プロジェクトとその関連情報を全て削除します。');">
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

