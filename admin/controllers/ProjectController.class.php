<?php
class ProjectController extends AppRootController {
	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * プロジェクト一覧表示・検索
	 */
	public function listAction() {
		$search = null;
		$search_flag = false;
		$limit=ADMIN_V_CNT;

		$dao = new ProjectDao();
		$commonDao = new CommonDao();

		$page = $_REQUEST["page"];
		$search[actress_no]=$_GET[actress_no];

		//言語コード
		$lang_code=MAIN_LANGUAGE;
		$this->view->assign("lang_code", $lang_code);

		// 検索送信ボタンが押下されているか？
		if (isset($_POST["sbm_search"])) {

			$search[no]=$_POST[no];
			$search[project_name]=$_POST[project_name];
			$search[actress_no]=$_POST[actress_no];
			$search[actress_name]=$_POST[actress_name];
			$search[chk_status]=$_POST[s_chk_status];
			$search[status]=$_POST[s_status];

			$_SESSION[search_condition]=$search;
		}
		// ページ番号が渡されたか？
		else if ($page) {
			// ページングによる遷移
			$search = $_SESSION["search_condition"];
			$search[page]=$this->request->getParam("page");

		}else if($_POST[sbm_update]){	//一括更新
			//ステータス変更
			if($_POST[status]){

				foreach($_POST[status] as $key=>$val){
					if($val!=$_POST[status_old][$key]){
						//ステータス更新
						$ret=$commonDao->updateData("sf_project","status",$val,"no",$key);
						if(!$ret){
							$delFlg=1;
						}
					}
				}
				if($delFlg==1){
					$this->addMessage("info","更新エラーがあります。");
				}
				else{
					$this->addMessage("info","ステータスを更新しました");
				}
			}
			//削除処理
			if($_POST[delete_dummy]){

				foreach($_POST[delete_dummy] as $key=>$val){
					//削除
					$ret=$dao->delete($val);
					if(!$ret){
						$delFlg=1;
					}
				}
				if($delFlg==1){
					$this->addMessage("info","プロジェクト削除エラーがあります。");
				}
				else{
					$this->addMessage("info","チェックしたプロジェクトを削除しました");
				}
			}

		}
		else {
			// sessionに検索条件が保存されている場合
			if($_SESSION["search_condition"]) unset($_SESSION["search_condition"]);
			$search[page]=1;
		}

		$total_cnt=$dao->searchCount($search,$lang_code);
		if($total_cnt>$limit){
			$page_navi = get_page_navi2($total_cnt, $limit, $search[page], "/project/list/");
		}
	//	print_r_with_pre($search);
		$project_list=$dao->search($search,"create_date desc",$limit,$lang_code);
		$entryArr=CommonArray::$entry_type_array;	//登録タイプ
		$this->view->assign("entryArr", $entryArr);
		$statusArr=CommonArray::$project_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);
		$chk_statusArr=CommonArray::$check_status_array;	//審査ステータス
		$this->view->assign("chk_statusArr", $chk_statusArr);

		$s_statusArr=CommonArray::$s_project_status_array;	//ステータス（検索用）
		$this->view->assign("s_statusArr", $s_statusArr);
		$s_chk_statusArr=CommonArray::$s_check_status_array;	//審査ステータス（検索用）
		$this->view->assign("s_chk_statusArr", $s_chk_statusArr);

		$this->view->assign("project_list", $project_list);
		$this->view->assign("total_cnt", $total_cnt);
		$this->view->assign("navi", $page_navi);
		$this->view->assign("search", $search);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("project/list.tpl");
		return;
	}

	/**
	 * プロジェクト基本情報更新
	 */
	public function editBasicAction() {

		$dao = new ProjectDAO();
		$ActressDao = new ActressDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);

		$project_no=$_REQUEST[no];
		$lang_code=$_REQUEST[lang];
		$this->view->assign("lang_code", $lang_code);

		if($_POST[submit]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//print_r_with_pre($input_data);
			//print_r_with_pre($_FILES);

			//サムネイルファイルアップ
			if(is_uploaded_file($_FILES["up_file4"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."_".$_FILES["up_file4"]["name"];//
				$_SESSION[TMP_FUP4]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				$ret_crop=make_crop($_FILES["up_file4"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname,608,435,2);
				//copy($_FILES["up_file4"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
				$input_data[new_cover_img]=$temp_up_fname;
			}elseif($_SESSION[TMP_FUP4]!=""){
				$input_data[new_cover_img]=$_SESSION[TMP_FUP4];
			}
			//---------------- 入力チェック ---------------------------
			//基本事項
			$baseData=CommonChkArray::$projectRegistBasicCheckData;
			$this->check($input_data,$baseData);

			//ステータスチェック
			if($input_data[status]==1){
				//支援受付にする場合は、支援コースの登録が必須
				//コース一覧
				$present_list=$commonDao->get_data_tbl("sf_prj_present",array("project_no","lang"),array($project_no,$lang_code),"sort");
				if(count($present_list)==0){
					$this->addMessage("status","支援コースが登録されていないため、ステータスを「掲載中」に変更することはできません。");
				}
			}
			//ステータスチェック
			if($input_data[chk_status]==2){
				//※修正を依頼する時必須
				if($input_data[recommend_text]==""){
					$this->addMessage("recommend_text","修正依頼内容を入力してください。");
				}
			}
			//-------------- ここまで -----------------------------------
			if (count($this->getMessages()) >0) {
				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}
				$this->view->assign("result_messages", $result_messages);
			}else{
				//基本事項
				foreach($baseData[dbstring] as $key=>$val){
					$dkey[]=$key;
					$dval[]=$input_data[$key];
				}
				//print_r_with_pre($input_data);die;
				if($input_data[no]){//修正
					if($input_data[status]!=$input_data[org_status]){	//ステータス変更時
						if($input_data[org_status]==0 && $input_data[status]==1){//掲載前→掲載中
							$input_data[project_record_date]=date("Y-m-d H:i:s");
							$dkey[]="project_record_date";	//公開日をセット
							$dval[]=date("Y-m-d H:i:s");
							$open_flg=1;	//公開に切り替えたときのフラグをセット
						}elseif($input_data[status]>1 ){	//成立・不成立の場合は後でメールを送るのでメール番号をセット
							if($input_data[status]==3){	//成立
								$input_data[project_success_date]=date("Y-m-d H:i:s");
								$dkey[]="project_success_date";	//成立日をセット
								$dval[]=date("Y-m-d H:i:s");
								$input_data[invest_status]=2;
								$send_mail_type=1;
							}else{
								$dkey[]="project_success_date";	//成立日をクリア
								$dval[]=0;
								$input_data[invest_status]=3;
								$send_mail_type=2;
							}
						}
						$input_data[org_status]=$input_data[status];
					}
					if($input_data[status]==1){//掲載中
							$dkey[]="chk_status";	//審査ステータス
							$dval[]=3;//審査完了
					}
					//会員情報取得
					//$ret=$commonDao->get_data_tbl("sf_member","user_id",$input_data[project_owner]);
					$dkey[]="user_no";
					$dval[]=$input_data[user_no];
					$ret=$dao->upItemData($dkey,$dval,"no",$input_data[no],$input_data);

					if($ret){
//						//====== 画像 正式アップ & 画像リサイズ作業 ===================
						//サムネイル
						if($_SESSION[TMP_FUP4]){
							$file_name=$input_data[no]."_sn_".$_SESSION[TMP_FUP4];
							copy(DIR_IMG_TMP.$_SESSION[TMP_FUP4],DIR_IMG_PROJECT.$file_name);
							//古い画像を削除
							if($input_data[cover_img]){
								unlink(DIR_IMG_PROJECT.$input_data[cover_img]);
							}
						}
						//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
						if($_SESSION[TMP_FUP4]){
							unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP4]);
							$input_data[cover_img]=$input_data[no]."_sn_".$_SESSION[TMP_FUP4];
						}
						//セッションクリア
						$_SESSION[TMP_FUP4]="";

						//メール送信 起案者に、入稿ページが再度修正可能になった事を通知する
						if($input_data[chk_status]=='2'){//修正依頼
								$ret=$commonDao->get_data_tbl("sf_member","user_no",$input_data[user_no]);
								$user_info=$ret[0];
								$this->view->assign("user_info", $user_info);
								$this->view->assign("p_data", $input_data);

								$subject = getMailSubject(4, $user_info[org_lang]);
								$mailBody = $this->view->fetch("mail/{$user_info[org_lang]}/project_change.tpl");	//成立メール
								send_mail($user_info[email], MAIL_FROM, MAIL_FROM_NAME, $subject, $mailBody,"",ADMIN_EMAIL);
						}
						//成立・不成立の時は支援ユーザーにメールを送る
						if($send_mail_type){
							//支援情報の取得
							$invest_list=$commonDao->get_data_tbl("sf_invest",array("project_no","status"),array("$input_data[no]",$input_data[invest_status]));
							for($c=0;$c<count($invest_list);$c++){

								//ユーザー情報の取得
								$ret=$commonDao->get_data_tbl("sf_member","user_no",$invest_list[$c][member_id]);
								$user_info=$ret[0];

							 	//メール送信
								$this->view->assign("user_info", $user_info);
								$this->view->assign("p_data", $input_data);
								$this->view->assign("i_data", $invest_list[$c]);

								$subject = getMailSubject($send_mail_type, $user_info[org_lang]);
								if($send_mail_type==1){
									$mailBody = $this->view->fetch("mail/{$user_info[org_lang]}/project_success.tpl");	//成立メール
								}else{
									$mailBody = $this->view->fetch("mail/{$user_info[org_lang]}/project_fail.tpl");	//失敗メール
								}
								send_mail($user_info[email], MAIL_FROM, MAIL_FROM_NAME, $subject, $mailBody,"",ADMIN_EMAIL);
							}
						}
						//ユーザー情報の取得
						$ret=$commonDao->get_data_tbl("sf_member","user_no",$input_data[user_no]);
						$user_info_p=$ret[0];
						$this->view->assign("user_info_p", $user_info_p);

						$this->addMessage("info","プロジェクト基本情報を保存しました");
						if($open_flg){
							//公開に切り替えたので、OGPのキャッシュをクリア
/*	  						$url_fb=ROOT_URL."ja/project_".$input_data[no].".php";
							$json = file_get_contents('https://graph.facebook.com/?scrape=true&id=' . $url_fb );
					        $array = json_decode($json, true);
*/					     //   print_r_with_pre($array);
							//仕様言語の取得
							$array_lang=CommonArray::$array_lang;

					  	  	foreach($array_lang as $lang_cd => $value){
						  		$url_fb=ROOT_URL.$lang_cd."/project_".$input_data[no].".php";
		  						$data = http_build_query(array(
								    'id'     => $url_fb,
								    'scrape' => 'true',
								));

								$context = array(
								    'http' => array(
								        'method' => 'POST',
								        'header' => implode("\r\n", array(
								            'Content-Type: application/x-www-form-urlencoded',
								            'Content-Length: '.strlen($data),
								        )),
								        'content' => $data,
								    ),
								);

								$fb_ret=json_decode(file_get_contents(
								    'https://graph.facebook.com',
								    false,
								    stream_context_create($context)
								), true);
								//print_r_with_pre($fb_ret);
					  	  	}
						}
					}
					else{
						$this->addMessage("error","プロジェクト基本情報の保存エラーです");
					}
				}
				else{
					//----------- 新規登録 ------------
					//会員情報取得
					$ret=$commonDao->get_data_tbl("sf_member","user_id",$input_data[project_owner]);

					$dkey[]="create_date";
					$dval[]=date("Y-m-d H:i:s");
					$dkey[]="user_no";
					$dval[]=$ret[0][user_no];
					//$dkey[]="hope_url";
					//$dval[]=md5(uniqid());

					$ret=$dao->InsertItemData($dkey, $dval,$input_data);

					if($ret){
						$input_data[no]=$ret;
						//====== 画像 正式アップ & 画像リサイズ作業 ===================
						//サムネイル
						if($_SESSION[TMP_FUP4]){
							$file_name=$input_data[no]."_sn_".$_SESSION[TMP_FUP4];
							copy(DIR_IMG_TMP.$_SESSION[TMP_FUP4],DIR_IMG_PROJECT.$file_name);
							//古い画像を削除
							if($input_data[cover_img]){
								unlink(DIR_IMG_PROJECT.$input_data[cover_img]);
							}
						}
						//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
						if($_SESSION[TMP_FUP4]){
							unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP4]);
							$input_data[cover_img]=$input_data[no]."_sn_".$_SESSION[TMP_FUP4];
						}
						//セッションクリア
						$_SESSION[TMP_FUP4]="";

						$this->addMessage("info","プロジェクト基本情報を登録しました");
					}
					else{
						$this->addMessage("error","プロジェクト基本情報の登録エラーです");
					}
				}
				$this->view->assign("finish_flg", 1);
				foreach($this->getMessages() as $edit_msg){
					$edit_messages[$edit_msg->getMessageLevel()]=$edit_msg->getMessageBody();
				}
				$this->view->assign("edit_messages", $edit_messages);
			}
		}
		else if($project_no){
			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("sf_project","no",$project_no);

			//ユーザー情報の取得
			$ret=$commonDao->get_data_tbl("sf_member","user_no",$tmp[0][user_no]);
			$user_info_p=$ret[0];
			$this->view->assign("user_info_p", $user_info_p);

			if($tmp){
				$input_data=$tmp[0];
				$db_data=$tmp[0];

				//サムネイル
				//$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("PROJECT_COVER",$input_data[no]));
				//$input_data[cover_img]=$tmp[0];

				//変更前のステータスを保存
				$input_data[org_status]=$input_data[status];
				$input_data[org_chk_status]=$input_data[chk_status];

				//支援金額調整額の取得
				//$input_data[now_admin_summary]=$input_data[now_summary]+$input_data[admin_summary];
/*
				//ツイート数・いいね数の取得
				$array_lang=CommonArray::$array_lang;

				 //いいね数の取得
		  		$like_cnt = 0;
		  	  	foreach($array_lang as $lang_cd => $value){
			  		$url_fb=ROOT_URL.$lang_cd."/project_".$input_data[no].".php";
			  		$url_en=urlencode($url);
			        $json = file_get_contents('http://graph.facebook.com/' . $url_fb ,true);
			        $array = json_decode($json, true);
			        //print_r_with_pre($array);

					if(!isset($array['shares'])){
						$like_cnt += 0;
					}else{
						$like_cnt += $array['shares'];
					}
				}
			 //ツイート数の取得
		 		$twitter_cnt=0;
		 	  	foreach($array_lang as $lang_cd => $value){
					$url_tw=ROOT_URL.$lang_cd."/detail-project.php?p_no=".$input_data[no];
					$json = @file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url_tw . '');
					$array = json_decode($json,true);
			        //print_r_with_pre($array);
					$twitter_cnt += $array['count'];
		 	  	}
				$input_data[like_cnt]=$like_cnt;
				$input_data[twitter_cnt]=$twitter_cnt;
				$input_data[sum_share]=$input_data[like_cnt]+$input_data[twitter_cnt];
				$input_data[sum_admin_share]=$input_data[sum_share]+$input_data[admin_share];
*/
			}
			else{
				$this->addMessage("error","該当するプロジェクトはありません");
        		$this->setTemplatePath("error.tpl");
				return;
			}

			$_SESSION[TMP_FUP4]="";
		}
		else{
			//新規登録
			//デフォルト
			$input_data[lang]=MAIN_LANGUAGE;
			$input_data[status]=0;
			$input_data[project_owner]=$_GET[actress_no];
			$input_data[now_summary]=0;
			$input_data[now_admin_summary]=0;
			$input_data[like_cnt]=0;
			$input_data[twitter_cnt]=0;
			$input_data[sum_share]=0;
			$input_data[sum_admin_share]=0;
			$_SESSION[TMP_FUP4]="";
					}
		//起案者リスト
		$ActressArr=$ActressDao->getActressOptionArr();
		$this->view->assign("ActressArr", $ActressArr);

		//カテゴリリスト
		$CategoryArr=$dao->getCategoryOptionArr();
		$this->view->assign("CategoryArr", $CategoryArr);


		//config
		$prefArr=CommonArray::$pref_text_array;
		$this->view->assign("prefArr", $prefArr);	//都道府県
		$statusArr=CommonArray::$project_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);
		$chk_statusArr=CommonArray::$check_status_array;	//審査ステータス
		$this->view->assign("chk_statusArr", $chk_statusArr);

		$s_statusArr=CommonArray::$s_project_status_array;	//ステータス（検索用）
		$this->view->assign("s_statusArr", $s_statusArr);
		$s_chk_statusArr=CommonArray::$s_check_status_array;	//審査ステータス（検索用）
		$this->view->assign("s_chk_statusArr", $s_chk_statusArr);

		$movietypeArr=CommonArray::$project_movietype_array;	//動画種類
		$this->view->assign("movietypeArr", $movietypeArr);

//		print_r_with_pre($input_data);
//		print_r_with_pre($_SESSION[TMP_FUP]);

		if($input_data[no]){
			$this->view->assign("mode", "edit");
			$this->view->assign("mode_str", "編集");
		}else{
			$this->view->assign("mode", "add");
			$this->view->assign("mode_str", "新規登録");
		}

		$input_data[lang]=$lang_code;

		$this->view->assign("input_data", $input_data);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("project/edit_basic.tpl");


		return;
	}

	/**
	 * プロジェクト詳細説明更新
	 */
	public function editDetailAction() {

		$dao = new ProjectDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);


		$project_no=$_REQUEST[no];
		$lang_code=$_REQUEST[lang];
		$this->view->assign("lang_code", $lang_code);

		if($_POST[exec]=="save"){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//print htmlspecialchars($input_data[project_text]) ;
			//---------------- 入力チェック ---------------------------
			//基本事項


			//-------------- ここまで -----------------------------------
			if (count($this->getMessages()) >0) {

				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}

				$this->view->assign("result_messages", $result_messages);
			}
			else {


				$ret=$commonDao->updateData("sf_project_detail","project_text",$input_data[project_text],array("project_no","lang"),array($input_data[project_no],$input_data[lang]));
				//$ret=$dao->updateProjectDetail($input_data[project_text],$input_data[project_no],$input_data[lang]);
				if($ret){
					$this->addMessage("info","プロジェクト詳細説明を保存しました");
				}
				else{
					$this->addMessage("error","プロジェクト詳細説明の保存エラーです");
				}
				$this->view->assign("finish_flg", 1);
				foreach($this->getMessages() as $edit_msg){
					$edit_messages[$edit_msg->getMessageLevel()]=$edit_msg->getMessageBody();
				}

				$this->view->assign("edit_messages", $edit_messages);

				//画像情報取得
				//画像
				$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("PROJECT_DETAIL",$input_data[project_no]));
				$input_data[image_list]=$tmp;

			}
		}
		else if($_POST[exec]=="delete"){	//削除
			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];

			$image_data=$commonDao->get_data_tbl("general_images","image_no",$_POST[image_no]);
			$ret=$commonDao->del_Data("general_images","image_no",$_POST[image_no]);
			//画像を削除
			unlink(DIR_IMG_PROJECT.$image_data[0][file_name]);
			//$this->addMessage("info","画像を削除しました");

			//画像情報取得
			//画像
			$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("PROJECT_DETAIL",$input_data[project_no]));
			$input_data[image_list]=$tmp;

		}
		else if($project_no){
			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("sf_project_detail",array("project_no","lang"),array($project_no,$lang_code));

			if($tmp){
				$input_data=$tmp[0];
				$input_data[project_text]=htmlspecialchars_decode($input_data[project_text]);
				$db_data=$tmp[0];

				//画像情報取得
				//画像
				$tmp=$commonDao->get_data_tbl("general_images",array("image_type","parent_no"),array("PROJECT_DETAIL",$input_data[project_no]));
				$input_data[image_list]=$tmp;

			}
			else{
				//新規言語？
				$this->addMessage("error","該当するプロジェクトはありません");
        		$this->setTemplatePath("error.tpl");
				return;

			}
			if($_GET[ibk] && $_SESSION[project_text]!=""){
				$input_data[project_text]=$_SESSION[project_text];
			}
			$_SESSION[project_text]="";
			$_SESSION["input_data"]="";
		}
		else{
			//新規登録

			//デフォルト
			$input_data[lang]=MAIN_LANGUAGE;
			$input_data[status]=0;
			$_SESSION[TMP_FUP]="";
			$_SESSION[TMP_FUP2]="";
			$_SESSION[project_text]="";
			$_SESSION["input_data"]="";		}

//		print_r_with_pre($input_data);
//		print_r_with_pre($_SESSION[TMP_FUP]);
/*
		if($input_data[no]){
			$this->view->assign("mode", "edit");
			$this->view->assign("mode_str", "編集");
		}else{
			$this->view->assign("mode", "add");
			$this->view->assign("mode_str", "新規登録");
		}
*/
		//print_r_with_pre($input_data);
		$this->view->assign("input_data", $input_data);


		//プロジェクト情報
		$project_info=$commonDao->get_data_tbl("sf_project","no",$project_no);
		$project_info[0][now_summary]=$project_info[0][now_summary]+$project_info[0][admin_summary];
		$this->view->assign("project_info", $project_info[0]);
		$statusArr=CommonArray::$project_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);
		$chk_statusArr=CommonArray::$check_status_array;	//審査ステータス
		$this->view->assign("chk_statusArr", $chk_statusArr);

		$s_statusArr=CommonArray::$s_project_status_array;	//ステータス（検索用）
		$this->view->assign("s_statusArr", $s_statusArr);
		$s_chk_statusArr=CommonArray::$s_check_status_array;	//ステータス（検索用）
		$this->view->assign("s_chk_statusArr", $s_chk_statusArr);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("project/edit_detail.tpl");

		return;
	}

	/**
	 * プロジェクト詳細用画像編集
	 */
	public function editImageAction() {

		$dao = new ProjectDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$login_admin = $this->getAdminSession();
		$this->view->assign("login_admin", $login_admin);

		$project_no=$_POST[project_no];
		$lang_code=$_POST[lang];
		$image_no=$_POST[image_no];
		$this->view->assign("project_no", $project_no);
		$this->view->assign("lang_code", $lang_code);

		if($_POST[sbm_save]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];
			//---------------- 入力チェック ---------------------------
			//基本事項
			//-------------- ここまで -----------------------------------
			//ファイルアップ
			if(is_uploaded_file($_FILES["up_file"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."_".$_FILES["up_file"]["name"];//
				$_SESSION[TMP_FUP]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				copy($_FILES["up_file"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
			}

			//入力チェック
			if(!$_SESSION[TMP_FUP] && $input_data[file_name]==""){
				$this->addMessage("file","画像をアップしてください");
			}else{
				//編集の場合で画像を選択していないので一覧に戻る
				header("Location:  /project/editDetail/?no=$project_no&lang=$lang_code&ibk=1");

			}
			if (count($this->getMessages()) >0) {

				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}

				$this->view->assign("result_messages", $result_messages);
			}
			else {
				//ファイル名の指定
				$file_name=$input_data[project_no]."_dtl_".$_SESSION[TMP_FUP];

				//スライド登録
				if(!$image_no){
					//新規登録
					$ikey[]="image_type";
					$ival[]="PROJECT_DETAIL";
					$ikey[]="parent_no";
					$ival[]=$input_data[project_no];
					$ikey[]="parent_type";
					$ival[]="project";
					$ikey[]="sort";
					$ival[]=1;
					$ikey[]="dir";
					$ival[]="DIR_IMG_PROJECT";
					$ikey[]="file_name";
					$ival[]=$file_name;

					$ikey[]="insert_date";
					$ival[]=date("Y-m-d H:i:s");

					$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

				}
				else{
					//変更処理
					$ret=$commonDao->updateData("general_images", "file_name", $file_name, "image_no", $input_data[image_no]);
				}

				$upErrFlg="1";
				if($ret && $_SESSION[TMP_FUP]){
					//画像正式アップ(幅740より大きい場合はリサイズする）
					$max_width="740";
					// 画像のサイズとタイプを取得
					list($s_width,$s_height, $type) = getimagesize(DIR_IMG_TMP.$_SESSION[TMP_FUP]);
					if($s_width>$max_width){
						$moto=DIR_IMG_TMP.$_SESSION[TMP_FUP];
						$newDir=DIR_IMG_PROJECT.$file_name;
						//画像をリサイズして正式アップ
						resize_image2($moto,$newDir,$max_width);

					}else{
						copy(DIR_IMG_TMP.$_SESSION[TMP_FUP],DIR_IMG_PROJECT.$file_name);
					}
					unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP]);
					//古い画像を削除
					if($input_data[file_name]!=""){
						unlink(DIR_IMG_PROJECT.$input_data[file_name]);

					}
					$this->addMessage("info","画像を保存しました");
					header("Location:  /project/editDetail/?no=$project_no&lang=$lang_code&ibk=1");
				}else if(!$ret){

					$upErrFlg=99;

				}

				if($_SESSION[TMP_FUP]) unset($_SESSION[TMP_FUP]);


			}
		}
		else if($image_no){
			//DBに登録されている情報取得
			$tmp=$commonDao->get_data_tbl("general_images","image_no",$image_no);

			if($tmp){
				$input_data=$tmp[0];

			}
			else{
				$this->addMessage("error","該当する画像はありません");
        		$this->setTemplatePath("error.tpl");
				return;

			}

			$input_data[project_no]=$project_no;
			$input_data[lang]=$lang_code;
			$_SESSION[project_text]=$_POST[project_text];
			$_SESSION[TMP_FUP]="";

		}
		else{
			//新規登録
			$input_data[project_no]=$project_no;
			$input_data[lang]=$lang_code;
			$_SESSION[project_text]=$_POST[project_text];
			$_SESSION[TMP_FUP]="";
		}


		if($input_data[image_no]){
			$this->view->assign("mode", "edit");
			$this->view->assign("mode_str", "編集");
		}else{
			$this->view->assign("mode", "add");
			$this->view->assign("mode_str", "新規登録");
		}

		$this->view->assign("input_data", $input_data);

		//プロジェクト情報
		$project_info=$commonDao->get_data_tbl("sf_project","no",$project_no);
		$project_info[0][now_summary]=$project_info[0][now_summary]+$project_info[0][admin_summary];
		$this->view->assign("project_info", $project_info[0]);
		$statusArr=CommonArray::$project_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);
		$chk_statusArr=CommonArray::$check_status_array;	//審査ステータス
		$this->view->assign("chk_statusArr", $chk_statusArr);

		$s_statusArr=CommonArray::$s_project_status_array;	//ステータス（検索用）
		$this->view->assign("s_statusArr", $s_statusArr);
		$s_chk_statusArr=CommonArray::$s_check_status_array;	//審査ステータス（検索用）
		$this->view->assign("s_chk_statusArr", $s_chk_statusArr);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("project/edit_image.tpl");

		return;
	}

	/**
	 * 支援コース一覧表示
	 */
	public function presentAction() {

		$dao = new ProjectDao();
		$InvestDao = new InvestDao();
		$commonDao=new CommonDao();
		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();

		$project_no=$_REQUEST[no];
		$lang_code=$_REQUEST[lang];
		$this->view->assign("lang_code", $lang_code);

		$mode = "add";

		if(isset($_POST[sbm_save])){

			$mode = $_REQUEST["mode"];
			$op = "新規保存";

			$present_obj=$_POST;

			//入力チェック
			$checkData=CommonChkArray::$presetRegistCheckData;

			//入力チェック
			$ret=!$this->check($present_obj,$checkData);

			if (count($this->getMessages()) >0) {
				$this->setSessionMessage();

				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}
				$this->view->assign("result_messages", $result_messages);
			}
			else {

				foreach($checkData[dbstring] as $key=>$val){
					$ins[$key]=$present_obj[$key];
				}

				if($mode=="add"){
					//新規登録
					$commonDao->InsertItemData2("sf_prj_present",$ins);

				}
				else{
					$op = "保存";
					$present_no = $_REQUEST["present_no"];
					//$present_obj = $dao->getAdmin($admin_no);

					$uwhere[present_no]=$present_no;
					$commonDao->updateData2("sf_prj_present",$ins,$uwhere);
				}

				$mode="add";
				$present_obj=array();
				$present_obj[project_no]=$project_no;
				$present_obj[lang]=$lang_code;

				$this->addMessage("info", "支援コースを" . $op . "しました");
				$this->margeMessages($dao->getMessages());
				$this->setSessionMessage();
				$this->view->assign("finish", 1);
				foreach($this->getMessages() as $edit_msg){
					$edit_messages[$edit_msg->getMessageLevel()]=$edit_msg->getMessageBody();
				}

				$this->view->assign("edit_messages", $edit_messages);

			}
		}
		else if(isset($_GET["present_no"])){

			$ret = $commonDao->get_data_tbl("sf_prj_present","present_no",$_GET["present_no"]);
			$present_obj=$ret[0];
			$mode = "update";
		}
		else{

			$present_obj[project_no]=$project_no;
			$present_obj[lang]=$lang_code;

		}

		//コース一覧
		$present_list=$commonDao->get_data_tbl("sf_prj_present",array("project_no","lang"),array($project_no,$lang_code),"sort");
		//支援数の取得
		for($c=0;$c<count($present_list);$c++){
			$cnt=$InvestDao->getInvestCount($project_no,$present_list[$c][present_no]);
			$present_list[$c][invest_cnt]=$cnt;
		}
		$this->view->assign("present_list", $present_list);


		//プロジェクト情報
		$project_info=$commonDao->get_data_tbl("sf_project","no",$project_no);
		$project_info[0][now_summary]=$project_info[0][now_summary]+$project_info[0][admin_summary];
		$this->view->assign("project_info", $project_info[0]);
		$statusArr=CommonArray::$project_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);
		$chk_statusArr=CommonArray::$check_status_array;	//審査ステータス
		$this->view->assign("chk_statusArr", $chk_statusArr);

		$s_statusArr=CommonArray::$s_project_status_array;	//ステータス（検索用）
		$this->view->assign("s_statusArr", $s_statusArr);
		$s_chk_statusArr=CommonArray::$s_check_status_array;	//審査ステータス（検索用）
		$this->view->assign("s_chk_statusArr", $s_chk_statusArr);


		$this->view->assign("input_data", $present_obj);
		$this->view->assign("mode", $mode);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("project/present.tpl");
		return;
	}

	public function bulkUpdateAction() {

		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();

		$commonDao=new CommonDao();


		//削除
		if($_POST[delete_dummy]){
			foreach($_POST[delete_dummy] as $key=>$val){
				$commonDao->del_Data("sf_prj_present", "present_no",$val);
			}
		}

		$this->addMessage(SYSTEM_MESSAGE_INFO, "支援コースを削除しました");
		$this->setSessionMessage();
		$this->redirect("/project/present/?no=$_POST[project_no]&lang=$_POST[lang]");
		return;
	}

	public function investAction() {

		$dao = new ProjectDao();
		$commonDao=new CommonDao();
		// ログイン中のAdminを取得
		$admin = $this->getAdminSession();

		$project_no=$_REQUEST[no];
		$lang_code=$_REQUEST[lang];

		$this->view->assign("project_no", $project_no);
		$this->view->assign("lang_code", $lang_code);

		if($_POST[delete]){	//削除ボタン
			//削除
			$ret=$dao->DeleteInvest($_POST[invest_no],$_POST[project_no]);
			$this->addMessage(SYSTEM_MESSAGE_INFO, "支援を削除しました");

		}elseif($_POST[credit]){
			//決済情報の取得
			$ret=$commonDao->get_data_tbl("sf_invest","no",$_POST[invest_no]);
			$invest_info=$ret[0];

			//ユーザー情報の取得
			$ret=$commonDao->get_data_tbl("sf_member","user_no",$invest_info[member_id]);
			$user_info=$ret[0];

			//プロジェクト情報の取得
			$ret=$commonDao->get_data_tbl("sf_project","no",$invest_info[project_no]);
			$project_info=$ret[0];

			$str_send = "https://secure.credix-web.co.jp/cgi-bin/secure.cgi?"
			  	."clientip=".$invest_info[clientip]
			  	."&send=cardsv&cardnumber=8888888888888882"
			  	."&expyy=00&expmm=00"
			  	."&money=".$invest_info[invest_amount]
			  	."&telno=0000000000"
			  	."&email=".$user_info[email]
			  	."&sendid=".$invest_info[auth_no]
			  	."&sendpoint=2";
			 //print $str_send."<br>";

			 $ret = file_get_contents($str_send);
			 //print $ret."<br>";

			 if ($ret=="Success_order"){
			 	//決済成功
			 	//支援情報を更新
				$data[status]=5;
				$where[no]=$invest_info[no];
			 	$ret=$commonDao->updateData2("sf_invest",$data,$where);

			 	//メール送信
				$this->view->assign("user_info", $user_info);
				$this->view->assign("p_data", $project_info);
				$this->view->assign("i_data", $invest_info);

				$subject = getMailSubject(3, $user_info[org_lang]);
				$mailBody = $this->view->fetch("mail/{$user_info[org_lang]}/credit_success.tpl");
				send_mail($user_info[email], MAIL_FROM, MAIL_FROM_NAME, $subject, $mailBody,"",ADMIN_EMAIL);

			 }else{
			 	//決済失敗
			 	//支援情報を更新
				$data[status]=91;
				$data[credit_fail_memo]=$ret;
				$where[no]=$invest_info[no];
			 	$ret=$commonDao->updateData2("sf_invest",$data,$where);

			 }

		}

		//支援一覧
		$invest_list=$dao->getInvestList("i.project_no=".$project_no. " and i.status>0");
		$this->view->assign("invest_list", $invest_list);
		//print_r_with_pre($invest_list);

		//プロジェクト情報
		$project_info=$commonDao->get_data_tbl("sf_project","no",$project_no);
		$project_info[0][now_summary]=$project_info[0][now_summary]+$project_info[0][admin_summary];
		$this->view->assign("project_info", $project_info[0]);
		$statusArr=CommonArray::$project_status_array;	//ステータス
		$this->view->assign("statusArr", $statusArr);
		$chk_statusArr=CommonArray::$check_status_array;	//審査ステータス
		$this->view->assign("chk_statusArr", $chk_statusArr);

		$s_statusArr=CommonArray::$s_project_status_array;	//ステータス（検索用）
		$this->view->assign("s_statusArr", $s_statusArr);
		$s_chk_statusArr=CommonArray::$s_check_status_array;	//審査ステータス（検索用）
		$this->view->assign("s_chk_statusArr", $s_chk_statusArr);
		//ステータスリスト
		$invest_statusArr=CommonArray::$invest_status_array;
		$this->view->assign("invest_statusArr", $invest_statusArr);

		$this->view->assign("input_data", $present_obj);
		$this->view->assign("mode", $mode);

		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("project/invest.tpl");
		return;
	}
}
?>

