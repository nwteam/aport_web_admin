{$user_info.member_name}様

このたびは、「{$p_data.public_title}」に支援登録をしていただきありがとうございました。
ご支援いただいたプロジェクトが成立いたしましたのでご連絡差し上げます。

■プロジェクト名：{$p_data.public_title}
■支援金額：{$i_data.invest_amount|number_format}円


支援金額は、支援登録時に認証されたカード情報をもとにクレジット決済させていただきます。

よろしくお願いいたします。



Thank　you!!
-----------------------------

aport
URL:{$smarty.const.ROOT_URL}

----------------------------
