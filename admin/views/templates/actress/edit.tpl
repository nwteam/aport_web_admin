{include file="head.tpl"}
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h4>起案者情報 {$mode_str} {if $mode=="edit"}【{$input_data.public_name}】{/if}</h4>
		{if $result_messages}
			<div class="header_msg_error">エラー箇所があります。下記赤字の欄を確認してください</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
		←<a href="/actress/list">起案者一覧へ</a>
		<form action="" method="post"  enctype="multipart/form-data">
			<div class="left w50">
				<h5 class="project_image">基本情報</h5>
				<table class="w100">
					<tr>
						<th>起案者No</th>
						<td>{$input_data.actress_no}</td>
							<input type="hidden" name="actress_no" value="{$input_data.actress_no}"  />
						</td>
						<input type="hidden" name="lang" value="{$input_data.lang}"  /></td>
					</tr>
					<tr>
						<th>起案者名<span class="red">※</span></th>
						<td>
							{if $result_messages.public_name}
								<span class="red">{$result_messages.public_name}</span><br />
							{/if}
							<input type="text" name="name" value="{$input_data.name}"  /></td>
							<input type="hidden" name="public_name" value="{$input_data.public_name}"  />
						</td>
					</tr>
					<tr>
						<th>Facebookアカウント</th>
						<td>
							{if $result_messages.fb_url}
								<span class="red">{$result_messages.fb_url}</span><br />
							{/if}
							<input class="long" type="text" name="fb_url" value="{$input_data.fb_url}"  /></td>
						</td>
					</tr>
					<tr>
						<th>メールアドレス<span class="red">※</span></th>
						<td>
							{if $result_messages.email}
								<span class="red">{$result_messages.email}</span><br />
							{/if}
							<input class="long" type="text" name="email" value="{$input_data.email}"  />
						</td>
					</tr>
					<tr>
						<th>会員のメールアドレス<br><span class="red">※採用の時必須</span></th>
						<td>
							{if $result_messages.mem_email}
								<span class="red">{$result_messages.mem_email}</span><br />
							{/if}
							{if $input_data.mem_email==""}
									<input class="long" type="text" name="mem_email" value="{$input_data.email}"  />
							{else}
									<input class="long" type="text" name="mem_email" value="{$input_data.mem_email}"  />
							{/if}
						</td>
					</tr>
					<tr>
						<th>ステータス<span class="red">※</span></th>
						<td>
							{if $result_messages.status}
								<span class="red">{$result_messages.status}</span><br />
							{/if}
							{html_options name=status options=$statusArr selected=$input_data.status}
						</td>
					</tr>
				</table>
			</div>
			<div class="right w50">
				<h5 class="project_image">起案者からのメッセージ</h5>
				<table class="w100">
					<tr>
						<th>プロジェクト名</th>
						<td>
							{if $result_messages.project_name}
								<span class="red">{$result_messages.project_name}</span><br />
							{/if}
							<input class="long" type="text" name="project_name" value="{$input_data.project_name}"  />
						</td>
					</tr>
					<tr>
						<th>カテゴリ</th>
						<td>
							{if $result_messages.category_no}
								<span class="red">{$result_messages.category_no}</span><br />
							{/if}
							{html_options name=category_no options=$catArr selected=$input_data.category_no}
						</td>
					</tr>
					<tr>
						<th>想定目標金額</th>
						<td>
							{if $result_messages.wish_price}
								<span class="red">{$result_messages.wish_price}</span><br />
							{/if}
							<input type="text" name="wish_price" value="{$input_data.wish_price}"  />円
						</td>
					</tr>
					<tr>
						<th>プロジェクト概要</th>
						<td>
						{if $result_messages.project_text}
							<span class="red">{$result_messages.project_text}</span><br />
						{/if}
						<textarea name="project_text" style="width: 486px; height:300px;" id="project_text">{$input_data.project_text}</textarea>
					</tr>
					<tr>
						<th>想定リターン</th>
						<td>
						{if $result_messages.comment_text}
							<span class="red">{$result_messages.comment_text}</span><br />
						{/if}
						<textarea name="comment_text" style="width: 486px; height:300px;" id="comment_text">{$input_data.comment_text}</textarea>
					</tr>
				</table>
			</div>
			<div class="clear"> </div>
			<br/>
			<div class="submit center">
				<input type="submit"  name="submit" value="保存" />
			</div>
		</div>
		</form>
	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
