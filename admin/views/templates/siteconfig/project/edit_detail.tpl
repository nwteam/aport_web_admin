{include file="head.tpl"}
<script type="text/javascript">
{literal}
	bkLib.onDomLoaded(function() {
		// nicEditors.allTextAreas()
		 new nicEditor({maxHeight : 600}).panelInstance('project_text');

	});
{/literal}
</script>
<script language="JavaScript">
<!--
{literal}
//詳細説明保存
function save()
{
	document.regForm.exec.value = "save";
	document.regForm.submit();
}

//編集
function edit( image_no )
{
	document.regForm.action = "/project/editImage/";
	document.regForm.image_no.value = image_no;
	document.regForm.submit();
}
function del( image_no )
{
	if( !confirm( "指定の画像を削除します。\r\nよろしいですか？" ) )
	{
		return false;
	}
	document.regForm.image_no.value = image_no;
	document.regForm.exec.value = "delete";
	document.regForm.submit();
}

function addImage()
{
	document.regForm.action = "/project/editImage/";
	document.regForm.submit();
}


{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		{assign var="id" value=$project_info.status}
       <h4>プロジェクト 【{$project_info.public_title}】　<span style="font-size: 14px;">《ステータス：{$statusArr.$id}》　《支援者数：{$project_info.now_supporter}人》　《支援総額：{$project_info.now_summary|number_format}円》</span></h4>
		←<a href="/project/list/">プロジェクト一覧へ</a>

<ul class="tab clearfix mt20">
	<li ><a href="/project/editBasic/?no={$input_data.project_no}&lang={$input_data.lang}">基本情報</a></li>
	<li><a href="javascript:void(0);" style="background-color:#656d78;color:#FFF;">プロジェクト詳細説明</a></li>
	<li><a href="/project/present/?no={$input_data.project_no}&lang={$input_data.lang}">支援コース</a></li>
	<li><a href="/project/invest/?no={$input_data.project_no}&lang={$input_data.lang}" >支援一覧</a></li>
	<li><a href="{$smarty.const.ROOT_URL}{$input_data.lang}/detail-project.php?p_no={$input_data.project_no}&test_view=1"  target="_blank" >プレビュー</a></li>
</ul>
<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
{*include file="messages.tpl"*}


<form name="regForm" action="/project/editDetail/?no={$input_data.project_no}&lang={$input_data.lang}" method="post"  >
    <input type="hidden" name="no" value="{$input_data.project_no}"  /></td>
    <input type="hidden" name="project_no" value="{$input_data.project_no}"  /></td>
    <input type="hidden" name="lang" value="{$input_data.lang}"  /></td>
    <input type="hidden" name="image_no" value=""  /></td>
    <input type="hidden" name="exec" value=""  /></td>

	<div class="left " style="width: 400px;">
    <div>
    	<p>■□プロジェクト詳細説明への画像の挿入方法□■</p>
    	<ol>
    	<li>1.画像リストに使用したい画像を追加する</li>
    	<li>2.追加した画像のURLをコピーする</li>
    	<li>3.プロジェクト詳細説明編集欄の画像を挿入する箇所をクリックする</li>
    	<li>4.<image src="images/btn_insert_image.JPG" />をクリックする</li>
   		<li>5.URLにコピーしたURLを貼り付ける</li>
     	 <li>6.必要事項を入力し、[画像挿入]ボタンをクリックする</li>
    	</ol>
    </div>

        <h5 class="project_image">画像リスト</h5>
		<input type="button" value="新規画像追加" onclick="addImage();" />
			{foreach from=$input_data.image_list item="image"}

 			<table class="w100">
	        <tr >
	          <th >画像</th>
	          <td>
				<img src="{$smarty.const.URL_IMG_PROJECT}{$image.file_name}?{$smarty.now}" height=100 />
	 		</td>
	       </tr>
	        <tr >
	          <th >URL</th>
	          <td>
		 		{$smarty.const.URL_IMG_PROJECT}{$image.file_name}
		 		</td>
	       </tr>
	       <tr>
	       	<td colspan="2" align="right">
	       	<input type="button" value="変更" onclick="edit({$image.image_no});">
	       	<input type="button" value="削除" onclick="del({$image.image_no});">
	       	</td>
	       </tr>
		{foreachelse}
			<div>現在登録されている画像はありません。</div>
		{/foreach}
	       </table>
		<input type="button" value="新規画像追加" onclick="addImage();" />


	</div>
		<div class="right" style="width: 800px; height:700px;">
        <h5 class="project_image">プロジェクト詳細説明</h5>
 		<textarea name="project_text" style="width: 780px; height:600px;" id="project_text">{$input_data.project_text}</textarea>
 		<br/>
		{*if $finish_flg!=1*}

	      <div class="submit center">
	        <input type="button"  name="sbm_save" value="保存" onclick="save();"/>
	      </div>
	    {*/if*}

		</div>
	<div class="clear"> </div>
   </div>
</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
