{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#project_no").val("");
	$("#project_name").val("");
	$("#user_no").val("");
	$("#nickname").val("");
}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>コメント一覧</h4>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="comment/list">
		<table class="search">
		<tr>
			<th>プロジェクトNo：</th>
			<td>
				<input type="text" name="project_no" id="project_no"  value="{$search.project_no}" size="4" />
			</td>
			<th>プロジェクト名：</th>
			<td>
				<input type="text" name="project_name" id="project_name"  value="{$search.project_name}" size="20" />
			</td>
		</tr>
		<tr>
			<th>会員No：</th>
			<td>
				<input type="text" name="user_no" id="user_no"  value="{$search.user_no}" size="4" />
			</td>
			<th>ニックネーム：</th>
			<td>
				<input type="text" name="nickname" id="nickname"  value="{$search.nickname}" size="20" />
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>

		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="comment/list/">

			<table class="admins clear">
			<tr>
				<th width="20">No</th>
				<th width="120">プロジェクト名</th>
				<th width="60">投稿者ニックネーム</th>
				<th width="200">コメント</th>
				<th width="60">登録日</th>
				<th width="40">削除</th>
			</tr>
			{foreach from=$comment_list item="comment"}
				<tr>
					<td><a href="comment/edit/?no={$comment.no}">{$comment.no}</a></td>
					<td>{$comment.project_name}</td>
					<td>{$comment.nickname}</td>
					<td>{$comment.comment|nl2br}</td>
					<td>{$comment.create_date|date_format:"%Y/%m/%d"}</td>
					<td><input type="checkbox" name="delete_dummy[{$comment.no}]" value="{$comment.no}" /></td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="12">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			<div class="center">
				<input type="submit" name="sbm_update" value="一覧を更新する" onClick="return confirm('更新します。削除チェックの場合は、コメントを削除します。');">
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

