<?php
class CommentController extends AppRootController {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();

		require_once sprintf("%s/dao/CommentDao.class.php", MODEL_PATH);

	}

	/**
	 * コメント一覧表示・検索
	 */
	public function listAction() {

		$search = null;
		$search_flag = false;
		$limit=ADMIN_V_CNT;

		$dao = new CommentDao();
		$commonDao = new CommonDao();

		$page = $_REQUEST["page"];

		$search[project_no]=$_GET[project_no];
		$search[user_no]=$_GET[user_no];

		// 検索送信ボタンが押下されているか？
		if (isset($_POST["sbm_search"])) {

			$search[project_no]=$_POST[project_no];
			$search[project_name]=$_POST[project_name];
			$search[user_no]=$_POST[user_no];
			$search[nickname]=$_POST[nickname];
			$search[page]=1;

			$_SESSION[search_condition]=$search;


		}
		// ページ番号が渡されたか？
		else if ($page) {
			// ページングによる遷移
			$search = $_SESSION["search_condition"];
			$search[page]=$this->request->getParam("page");
		}
		// 詳細から戻ってきたか？
		else if ($_GET[bk]) {
			$search = $_SESSION["search_condition"];

		}else if($_POST[sbm_update]){	//一括更新
			//凍結/開始処理
			if($_POST[stop_dummy]){

				foreach($_POST[stop_dummy] as $key=>$val){
					//削除
					$ret=$commonDao->updateData("member","stat",$val,"member_id",$key);
					if(!$ret){
						$delFlg=1;
					}
				}
				if($delFlg==1){
					$this->addMessage("info","更新エラーがあります。");
				}
				else{
					$this->addMessage("info","更新しました");
				}
			}

			//削除処理
			if($_POST[delete_dummy]){

				foreach($_POST[delete_dummy] as $key=>$val){
					//削除
					//削除
					$ret=$dao->delete($val);
					if(!$ret){
						$delFlg=1;
					}

				}
				if($delFlg==1){
					$this->addMessage("info","コメント削除エラーがあります。");
				}
				else{
					$this->addMessage("info","チェックしたコメントを削除しました");
				}

			}
		}

		else {
			// sessionに検索条件が保存されている場合

			if($_SESSION["search_condition"]) unset($_SESSION["search_condition"]);
			$search[page]=1;

		}

		$total_cnt=$dao->searchCount($search);
		if($total_cnt>$limit){
			$page_navi = get_page_navi2($total_cnt, $limit, $search[page], "/comment/list/");
		}
		$comment_list=$dao->search($search,"create_date desc",$limit);


		$this->view->assign("comment_list", $comment_list);
		$this->view->assign("total_cnt", $total_cnt);
		$this->view->assign("navi", $page_navi);
		$this->view->assign("search", $search);


		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("comment/list.tpl");
		return;
	}

	/**
	 * コメント情報更新
	 */
	public function editAction() {

		$dao = new CommentDAO();
		$commonDao = new CommonDAO();

		// ログイン中のadmin情報を取得
		$admin = $this->getAdminSession();
		$no=$_REQUEST[no];


		if($_POST[modify]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];

			//---------------- 入力チェック ---------------------------
			//基本事項
			//$baseData=CommonChkArray::$memberRegistCheckData;
			//$this->check($input_data,$baseData);
			//-------------- ここまで -----------------------------------

			if (count($this->getMessages()) >0) {

				foreach($this->getMessages() as $err_msg){
					$result_messages[$err_msg->getMessageLevel()]=$err_msg->getMessageBody();
				}

				$this->view->assign("result_messages", $result_messages);
			}
			else {

				$ret=$commonDao->updateData("sf_comment","comment",$input_data[comment],"no",$input_data[no]);
				if($ret){
					$this->addMessage("info","コメントを更新しました");
				}
				else{
					$this->addMessage("info","コメントの更新エラーです");
				}
			}
		}
		else{

			//DBに登録されている情報取得
			$tmp=$dao->getCommentData($no);
			$input_data=$tmp[0];
		}




		$this->view->assign("msg", $msg);
		$this->view->assign("input_data", $input_data);


		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

        // テンプレート表示
        $this->setTemplatePath("comment/edit.tpl");
		return;
	}



}
?>

