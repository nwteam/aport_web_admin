{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#column_no").val("");
	$("#column_name").val("");
	$("#actress_no").val("");
	$("#actress_name").val("");
}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>コラム一覧</h4>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="column/list">
		<table class="search">
		<tr>
			<th>コラムNo：</th>
			<td>
				<input type="text" name="column_id" id="column_id"  value="{$search.column_id}" size="4" />
			</td>
			<th>タイトル：</th>
			<td>
				<input type="text" name="column_title" id="column_title"  value="{$search.column_title}" size="100" />
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>
		<a class="btn" href="/column/editBasic/">新規作成</a>
		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="column/list/">

			<table class="admins clear">
			<tr>
				<th width="20">No</th>
				<th width="50">カテゴリ</th>
				<th width="200">タイトル</th>
				<th width="60">登録日</th>
				<th width="60">更新日</th>
				<th width="40">削除</th>
			</tr>
			{foreach from=$column_list item="column"}
				<tr>
					<td><a href="column/editBasic/?id={$column.id}">{$column.id}</a></td>
					<td>{$column.cat_name}</td>
					<td>{$column.title}</td>
					<td>{$column.insert_time}</td>
					<td>{$column.update_time}</td>
					<td style="text-align: center"><input type="checkbox" name="delete_dummy[]" value="{$column.id}" /></td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="11">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			<div class="center">
				<input type="submit" name="sbm_update" value="一覧を更新する" onClick="return confirm('更新します。削除チェックの場合は、コラムとその関連情報を全て削除します。');">
			</div>
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

