{include file="head.tpl"}
<script language="JavaScript">
<!--
{literal}
$(function(){
	$("#invest_limit").datepicker({
		dateFormat: "yy/mm/dd"
	});
	$("#project_record_date").datepicker({
		dateFormat: "yy/mm/dd"
	});
	$("#project_success_date").datepicker({
		dateFormat: "yy/mm/dd"
	});
});

{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		{assign var="id" value=$input_data.status}
		<h4>プロジェクト {if $mode=="add"}新規登録{else}【{$input_data.public_title}】　<span style="font-size: 14px;">《ステータス：{$statusArr.$id}》　《支援者数：{$input_data.now_supporter}人》　《支援総額：{$input_data.now_summary|number_format}円》{/if}</span></h4>
			←<a href="/project/list/">プロジェクト一覧へ</a>

		<ul class="tab clearfix mt20">
			<li ><a href="javascript:void(0);" style="background-color:#656d78;color:#FFF;">基本情報</a></li>
			{if $input_data.no}
			<li><a href="/project/editDetail/?no={$input_data.no}&lang={$input_data.lang}">プロジェクト詳細説明</a></li>
			<li><a href="/project/present/?no={$input_data.no}&lang={$input_data.lang}">支援コース</a></li>
			<li><a href="/project/invest/?no={$input_data.no}&lang={$input_data.lang}" >支援一覧</a></li>
			<li><a href="{$smarty.const.ROOT_URL}{$input_data.lang}/detail-project.php?p_no={$input_data.no}&test_view=1"  target="_blank" >プレビュー</a></li>
			{/if}
		</ul>
		<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
		{*include file="messages.tpl"*}
		<form action="" method="post"  enctype="multipart/form-data">
		<div class="left w50">
			<h5 class="project_image">基本情報</h5>
			<table class="w100">
				<tr>
				  <th>プロジェクトNo</th>
				  <td>{$input_data.no}</td>
				  <input type="hidden" name="no" value="{$input_data.no}"  /></td>
				  <input type="hidden" name="user_no" value="{$input_data.user_no}"  /></td>
				  <input type="hidden" name="lang" value="{$input_data.lang}"  /></td>
				</tr>
				<tr>
				  <th>プロジェクトタイプ</th>
				  <td>
						{if $result_messages.project_type}
							<span class="red">{$result_messages.project_type}</span><br />
						{/if}
						<input type="radio" name="project_type" id="project_type" value="1"  {if $input_data.project_type=='1'}checked="checked"{else}{/if}/>達成時実行型（All or Nothing）
						<input type="radio" name="project_type" id="project_type" value="2"  {if $input_data.project_type=='2'}checked="checked"{else}{/if}/>実行確約型
				</tr>
				<tr>
				  <th>タイトル<span class="red">※</span></th>
				  <td>
						{if $result_messages.public_title}
							<span class="red">{$result_messages.public_title}</span><br />
						{/if}
				    <input class="long" type="text" name="public_title" value="{$input_data.public_title}"  /></td>
				</tr>
				<tr>
				  <th>カテゴリ<span class="red">※</span></th>
				  <td>
						{if $result_messages.category_no}
							<span class="red">{$result_messages.category_no}</span><br />
						{/if}
						{html_options name=category_no options=$CategoryArr selected=$input_data.category_no}
				</tr>
				<tr>
				  <th>起案者名<span class="red">※</span></th>
				  <td>
						{if $result_messages.project_owner}
							<span class="red">{$result_messages.project_owner}</span><br />
						{/if}
						{html_options name=project_owner options=$ActressArr selected=$input_data.project_owner}
				</tr>
				<tr>
				  <th>達成条件　支援額</th>
				  <td>
						{if $result_messages.wish_price}
							<span class="red">{$result_messages.wish_price}</span><br />
						{/if}
				  <input class="short ime_off" type="text" name="wish_price" value="{$input_data.wish_price}"  />円</td>
				</tr>
				<tr>
				  <th>メイン動画</th>
				  <td>
						{if $result_messages.movie_type}
							<span class="red">{$result_messages.movie_type}</span><br />
						{/if}
					<input type="radio" name="movie_type" value="1" {if $input_data.movie_type=='1'}checked="checked"{else}{/if} />YouTube
					<input type="radio" name="movie_type" value="2"  {if $input_data.movie_type=='2'}checked="checked"{else}{/if} />Vimemo
					<input type="radio" name="movie_type" value="0"  {if $input_data.movie_type=='0'}checked="checked"{else}{/if} />なし
				</tr>
				<tr>
				  <th>動画のURL</th>
				  <td>
						{if $result_messages.movie_url}
							<span class="red">{$result_messages.movie_url}</span><br />
						{/if}
					<input class="long" type="text" name="movie_url" value="{$input_data.movie_url}"  />
				</tr>
				<tr>
				  <th>サポート募集期限</th>
				  <td>
						{if $result_messages.invest_limit}
							<span class="red">{$result_messages.invest_limit}</span><br />
						{/if}
						<input id="invest_limit" class="long ime_off " type="text" name="invest_limit" value="{$input_data.invest_limit|date_format:"%Y/%m/%d"}"  /></td>
				</tr>
				<tr>
				  <th>プロジェクト公開日</th>
				  <td>
						{if $result_messages.project_record_date}
							<span class="red">{$result_messages.project_record_date}</span><br />
						{/if}
						<input id="project_record_date" class="long ime_off " type="text" name="project_record_date" value="{$input_data.project_record_date|date_format:"%Y/%m/%d"}"  /></td>
					</td>
				</tr>
				<tr>
				  <th>プロジェクト成立日</th>
				  <td>
						{if $input_data.project_success_date>0}{$input_data.project_success_date|date_format:"%Y/%m/%d"}{/if}
							<input type="hidden" name="project_success_date" value="{$input_data.project_success_date}" />
					</td>
				</tr>
				<tr>
				  <th>希望のURL</th>
				  <td>
						{if $result_messages.hope_url}
							<span class="red">{$result_messages.hope_url}</span><br />
						{/if}
						{$smarty.const.URL_SSL}projects/<input class="short" type="text" name="hope_url" value="{$input_data.hope_url}"  />
				</tr>
				<tr>
				  <th>ステータス<span class="red">※</span></th>
				  <td>
						{if $result_messages.status}
							<span class="red">{$result_messages.status}</span><br />
						{/if}
						{if $mode=="add"}
							掲載前
							<input type="hidden" name="status" value=0 />
						{else}
							{html_options name=status options=$statusArr selected=$input_data.status}
						{/if}
							<input type="hidden" name="org_status" value="{$input_data.org_status}" />
					</td>
				</tr>
				<tr>
				  <th>審査ステータス<span class="red">※</span></th>
				  <td>
						{if $result_messages.chk_status}
							<span class="red">{$result_messages.chk_status}</span><br />
						{/if}
						{if $mode=="add"}
							入稿中
							<input type="hidden" name="chk_status" value=0 />
						{else}
							{html_options name=chk_status options=$chk_statusArr selected=$input_data.chk_status}
						{/if}
							<input type="hidden" name="org_chk_status" value="{$input_data.org_chk_status}" />
					</td>
				</tr>
			</table>
			<h5 class="project_image">修正依頼内容</h5>
			<table class="w100">
				<tr>
					<th width="159px;">修正依頼内容<br><span class="red">※修正を依頼する時必須</span></th>
					<td>
					{if $result_messages.recommend_text}
						<span class="red">{$result_messages.recommend_text}</span><br />
					{/if}
					<textarea name="recommend_text" style="width: 384px; height:200px;" id="recommend_text">{$input_data.recommend_text}</textarea>
				</tr>
			</table>
			<h5 class="project_image">金融機関情報</h5>
			<table class="w100">
				<tr><th width="159px;">金融機関名</th><td>{$user_info_p.bank_name}</td></tr>
				<tr><th width="159px;">支店名</th><td>{$user_info_p.branch_bank}</td></tr>
				<tr><th width="159px;">支店コード</th><td>{$user_info_p.branch_bank_code}</td></tr>
				<tr><th width="159px;">口座種別</th><td>{if $user_info_p.card_type=='1'}普通{/if}{if $user_info_p.card_type=='2'}当座{/if}{if $user_info_p.card_type=='3'}貯蓄{/if}</td></tr>
				<tr><th width="159px;">口座番号</th><td>{$user_info_p.card_number}</td></tr>
				<tr><th width="159px;">口座名義人（カナ）</th><td>{$user_info_p.card_owner}</td></tr>
				<input type="hidden" name="bank_name" value="{$input_data.bank_name}">
				<input type="hidden" name="branch_bank" value="{$input_data.branch_bank}">
				<input type="hidden" name="branch_bank_code" value="{$input_data.branch_bank_code}">
				<input type="hidden" name="card_type" value="{$input_data.card_type}">
				<input type="hidden" name="card_number" value="{$input_data.card_number}">
				<input type="hidden" name="card_owner" value="{$input_data.card_owner}">
			</table>
			<h5 class="project_image">支援状況</h5>
			<table class="w100">
				<tr>
				  <th width="159px;">支援金額</th>
				  <td>
					支援金額：{$input_data.now_summary|number_format}円
				  </td>
					<input type="hidden" name="now_summary" value="{$input_data.now_summary}">
				</tr>
			</table>
		</div>
		<div class="right w50">
			<h5 class="project_image">サムネイル</h5>
			<table class="w100">
				<tr>
				  <th >サムネイル</th>
				  <td>
					{if $result_messages.profile_image}
						<span class="red">{$result_messages.profile_image}</span><br />
					{/if}
					{if $smarty.session.TMP_FUP4!=""}
						<img src="{$smarty.const.URL_IMG_TMP}{$smarty.session.TMP_FUP4}" width=608 height=435 />
					{elseif $input_data.cover_img!=""}
						<img src="{$smarty.const.URL_IMG_PROJECT}{$input_data.cover_img}" width=608 height=435 />
						<input type="hidden" name="cover_img" value="{$input_data.cover_img}" />
					{/if}
					<input type="file" name="up_file4"  size="30"/>
					<br />画像サイズ 608 × 435px
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"> </div>
		{*if $finish_flg!=1*}
			<div class="submit center">
			<input type="submit"  name="submit" value="保存" />
			</div>
		{*/if*}
		</div>
		</form>
	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
