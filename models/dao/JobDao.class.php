<?php
class JobDAO extends DAOBase {


	/**
	 * 登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt="'".htmlspecialchars($dt, ENT_QUOTES)."'";
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into job_offer($ins) values($valu)";
		$this->db->beginTransaction();
		try {

			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'company'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;

	}
	/**
	 * 会員 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$item="") {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".addslashes($wdt[$i])."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".addslashes($wdt)."'";
		}

		$sql="update job_offer set $ins $where";
//echo $sql;
		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'company'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);
		$sql="SELECT count(distinct o.job_no) as cnt FROM job_offer as o ,company as c ".$tbl." where o.company_id=c.company_id ".$where;
		$tmp=$this->executeQuery($sql);
		return $tmp[0][cnt];
	}

	/**
	 * 応募　検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchApplyCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);
		$sql="select count(distinct job_no) AS cnt from member_apply as a where a.del_flg=0 ".$where;

		$tmp=$this->executeQuery($sql);
		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//勤務場所
		if($search["place"]){
			$whTmp[]="(a.attr_key='place' and a.attr_value1='".addslashes($search["place"])."') and o.job_no=a.job_no";
			$tblTmp[attr]="job_ext_attrs as a";
		}
		$jflg=0;
		//雇用形態
		if($search["employ"]){
			$whTmp[]="j.employ='".addslashes($search["employ"])."'";
			$tblTmp[jyoken]="job_jyoken as j";
			$jflg=1;
		}
		//
		//給与形態
		if($search["salary"] && $search["salary_d"]){
			$whTmp[]="j.salary_flg='".addslashes($search["salary"])."'";
			$tblTmp[jyoken]="job_jyoken as j";
			$jflg=1;
		}
		//給与詳細金額
		if($search["salary_d"]){
			$whTmp[]="j.salary='".addslashes($search["salary_d"])."'";
			$tblTmp[jyoken]="job_jyoken as j";
			$jflg=1;
		}

		if($jflg==1){
			$whTmp[]="j.job_no=o.job_no";
		}

		//job_no
		if($search["job_no"]){
			$whTmp[]="o.job_no='".addslashes($search["job_no"])."'";
		}

		//企業ID
		if($search["company_id"]){
			$whTmp[]="o.company_id='".addslashes($search["company_id"])."'";
		}

		//新着
		if($search["newJob"]){
			//2週間以内
			$whTmp[]="o.view_start>='".date("Y-m-d H:i:s",strtotime("-2 week"))."'";
		}
		//掲載終了
		if($search["endJob"]){
			//2週間以内
			$whTmp[]="o.view_end<'".date("Y-m-d")."'";
		}

		//会員
		if($search["member_id"]){
			$whTmp[]="a.member_id='".addslashes($search["member_id"])."'";
		}


		//掲載開始日
		if($search["view_start"]){
			$whTmp[]="o.view_start<='".addslashes($search["view_start"])."'";
		}

		//承認非承認
		if(isset($search["admit_flg"])){
			$whTmp[]="admit_flg='".addslashes($search["admit_flg"])."'";
		}

		if($whTmp){
			$where=" and ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		return array($where,$tbl);

	}


	/**
	 * 検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {
			 if(!$search["page"]) $search["page"]=1;
			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}


		// $sql="SELECT distinct o.*,c.company_name FROM job_offer as o ,company as c ".$tbl." where o.company_id=c.company_id ".$where.$ord.$limit_str;
		$sql="SELECT distinct o.*,c.company_name,c.fax FROM job_offer as o ,company as c ".$tbl." where o.company_id=c.company_id ".$where.$ord.$limit_str;
//echo $sql;
		$prodArr=$this->executeQuery($sql);


		//属性情報を取得する
		$commonDao=new CommonDao();
		$jyokenArr=CommonChkArray::$jobJyokenCheckData;
		for($i=0;$i<count($prodArr);$i++){
			$sql="select * from job_ext_attrs where job_no=".$prodArr[$i][job_no]." order by attr_key , seq";

			$tmp=$this->executeQuery($sql);
			for($j=0;$j<count($tmp);$j++){
				$attr_key=$tmp[$j][attr_key];
				if($tmp[$j][attr_value1]){
					$prodArr[$i][$attr_key][$j]=$tmp[$j][attr_value1];
				}
				else{
					$prodArr[$i][$attr_key][$j]=$tmp[$j][attr_value3];
				}
			}

			//雇用条件を取得する
			$tmp=$commonDao->get_data_tbl("job_jyoken","job_no",$prodArr[$i][job_no]," no asc");
			for($j=0;$j<count($tmp);$j++){
				foreach($jyokenArr[dbstring] as $key=>$val){
					$prodArr[$i][$key][$j]=$tmp[$j][$key];
				}
			}

			//画像
			$tmp=$commonDao->get_data_tbl("job_images","job_no",$prodArr[$i][job_no]);
			for($j=0;$j<count($tmp);$j++){
				$seq=$tmp[$j][seq];
				$prodArr[$i][img][$seq]=$tmp[$j][file_name];
				$prodArr[$i][img_comment][$seq]=$tmp[$j][comment];
			}


		}

		return $prodArr;

	}


	/**
	 * 検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchApply($search="",$orderby="",$limit="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {
			 if(!$search["page"]) $search["page"]=1;
			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}


		$sql="select distinct job_no from member_apply as a where a.del_flg=0 ".$where.$ord.$limit_str;

		$appTmp=$this->executeQuery($sql);


		$commonDao=new CommonDao();
		$jyokenArr=CommonChkArray::$jobJyokenCheckData;

		for($i=0;$i<count($appTmp);$i++){

			$job_no=$appTmp[$i][job_no];

			$sql="SELECT distinct o.*,c.company_name FROM job_offer as o ,company as c
					where o.company_id=c.company_id and o.job_no=".$job_no;

			$prodTmp=$this->executeQuery($sql);

			if($prodTmp){
				$prodArr[$i]=$prodTmp[0];
				$prodArr[$i][apply_no]=$appTmp[$i][apply_no];

				//属性情報
				$sql="select * from job_ext_attrs where job_no=".$prodArr[$i][job_no]." order by attr_key , seq";
				$tmp=$this->executeQuery($sql);
				for($j=0;$j<count($tmp);$j++){
					$attr_key=$tmp[$j][attr_key];
					if($tmp[$j][attr_value1]){
						$prodArr[$i][$attr_key][$j]=$tmp[$j][attr_value1];
					}
					else{
						$prodArr[$i][$attr_key][$j]=$tmp[$j][attr_value3];
					}
				}

				//雇用条件を取得する
				$tmp=$commonDao->get_data_tbl("job_jyoken","job_no",$prodArr[$i][job_no]," no asc");
				for($j=0;$j<count($tmp);$j++){
					foreach($jyokenArr[dbstring] as $key=>$val){
						$prodArr[$i][$key][$j]=$tmp[$j][$key];
					}
				}

				//画像
				$tmp=$commonDao->get_data_tbl("job_images","job_no",$prodArr[$i][job_no]);
				for($j=0;$j<count($tmp);$j++){
					$seq=$tmp[$j][seq];
					$prodArr[$i][img][$seq]=$tmp[$j][file_name];
					$prodArr[$i][img_comment][$seq]=$tmp[$j][comment];
				}
			}
		}

		return $prodArr;

	}


	//削除
	/**
	 * 法人削除　関連データも全て削除
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 * @return
	 */
	public function delData($job_no) {


		//------------- 求人企業登録と求人 -------------
		$commonDao=new CommonDao();

		//job_offer
		$commonDao->del_Data("job_offer", "job_no", $job_no);

		//job_jyoken
		$commonDao->del_Data("job_jyoken", "job_no", $job_no);


		//job_images
		$tmp=$commonDao->get_data_tbl("job_images","job_no", $job_no);
		for($i=0;$i<count($tmp);$i++){
			if(is_file(DIR_IMG_JOB.$tmp[$i][file_name])) unlink(DIR_IMG_JOB.$tmp[$i][file_name]);
		}
		$commonDao->del_Data("job_images", "job_no", $job_no);

		//job_ext_attrs
		$commonDao->del_Data("job_ext_attrs", "job_no", $job_no);

		//job_bookmark
		$commonDao->del_Data("job_bookmark", "job_no", $job_no);



	}

}
?>