{include file="head.tpl"}
<script type="text/javascript">
</script>
<script language="JavaScript">
<!--
{literal}
//編集
function editcancel(p_no,lang)
{
	location.href="project/editDetail/?no="+p_no+"&lang="+lang+"&ibk=1";
}

function imagePreview(ele) {

    if (ele.value==""){
    	alert("ファイル未選択");
    	return;  // ファイル未選択
    }
    if(!/(\.gif|\.jpg|\.jpeg|\.png)$/i.test(ele.value)) {
    	 	    alert("イメージ形式のファイルを選択してください。");
	    return;
	}
    document.getElementById('preview').innerHTML ="";

    if (window.FileReader ){
        var file = ele.files[0];
	    var fr = new FileReader();
	    fr.onload = function(event) {
	    	var loadedImageUri = event.target.result;
	       document.getElementById('preview').innerHTML = '<img src="' + loadedImageUri + '" width="160" />';
	    }
	    fr.readAsDataURL(file);  // 画像読み込み
	}else{
	   	document.getElementById('preview').style.background = "url('./images/sample_ok.png') no-repeat";
        document.getElementById('preview').innerHTML =ele.value;
	}
 }
{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
    <h4>プロジェクト 【{$project_info.public_title}】　<span style="font-size: 14px;">《ステータス：{$statusArr.$id}》　《支援者数：{$project_info.now_supporter}人》　《支援総額：{$project_info.now_summary|number_format}円》</span></h4>

<ul class="tab clearfix mt20">
	<li ><a href="/project/editBasic/?no={$input_data.project_no}&lang={$input_data.lang}">基本情報</a></li>
	<li><a href="/project/editDetail/?no={$input_data.project_no}&lang={$input_data.lang}" style="background-color:#656d78;color:#FFF;">プロジェクト詳細説明</a></li>
	<li><a href="/project/present/?no={$input_data.project_no}&lang={$input_data.lang}">支援コース</a></li>
	<li><a href="/project/invest/?no={$input_data.project_no}&lang={$input_data.lang}">支援一覧</a></li>
	<li><a href="{$smarty.const.ROOT_URL}{$input_data.lang}/detail-project.php?p_no={$input_data.project_no}&test_view=1"  target="_blank" >プレビュー</a></li>
</ul>
<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
        <h5>プロジェクト詳細 画像{$mode_str}：{$project_info.public_title}</h5>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
{*include file="messages.tpl"*}


<form name="regForm" action="" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="project_no" value="{$input_data.project_no}"  /></td>
    <input type="hidden" name="lang" value="{$input_data.lang}"  /></td>
    <input type="hidden" name="target_imageno" value=""  /></td>
	<div class="left " style="width: 500px;">
 			<table class="w100">
	        <tr >
	          <th >画像</th>
	          <td>
		 		{if $result_messages.file}
					<span class="red">{$result_messages.file}</span><br />
				{/if}
				{if $smarty.session.TMP_FUP!=""}
					<img src="{$smarty.const.URL_IMG_TMP}{$smarty.session.TMP_FUP}" width=300 />
				{elseif $input_data.file_name!=""}
					<img src="{$smarty.const.URL_IMG_PROJECT}{$input_data.file_name}?{$smarty.now}" width=300 />
					<input type="hidden" name="file_name" value="{$input_data.file_name}" />
					<input type="hidden" name="image_no" value="{$input_data.image_no}" />
				{/if}
		 		<input type="file" name="up_file"  size="30"/>
	 		</td>
	       </tr>


	       </table>
	      <div class="submit center">
	        <input type="submit"  name="sbm_save" value="保存" />
			<input name="cancel" type="button" value="キャンセル" onclick="editcancel({$project_no},'{$lang_code}');" />
	      </div>


	</div>
	<div class="clear"> </div>
   </div>
</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
