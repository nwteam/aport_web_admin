<?php
class ColumnDAO extends DAOBase {

	/**
	 * コラム情報取得
	 * @param $where:条件
	 *        $limit:件数
	 *        $order:並べ替え
	 * @return 取得したデータ
	 */

	public function getColumnList($where="", $limit="", $order="") {
		if ($order == "") $order = " ORDER BY a.insert_time ASC ";
		$sql = "SELECT
		            a.*
				FROM
					sf_column  a
				{$where}
				{$order}
				{$limit}
			 ";

		$column_src = $this->executeQuery($sql);
		return $column_src;
	}

	/**
	 * コラム登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into sf_column($ins) values($valu)";

		$this->db->beginTransaction();
		try {
			//print $sql."<br>";
			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();
			//print "ID=".$lastInsertId."<br>";

			//メイン言語の詳細データの作成
//			$ikey[]="column_id";
//			$ival[]=$lastInsertId;
//			$ikey[]="lang";
//			$ival[]=MAIN_LANGUAGE;
//			$ikey[]="column_title";
//			$ival[]=$item[title];
//			$ikey[]="insert_time";
//			$ival[]=date("Y-m-d H:i:s");

//			$ret=$commonDao->InsertItemData("sf_column",$ikey,$ival);

			//トップの画像
			if($item[new_cover_img]){
				$ikey=array();
				$ival=array();

				$ikey[]="image_type";
				$ival[]="COLUMN_COVER";
				$ikey[]="parent_no";
				$ival[]=$lastInsertId;
				$ikey[]="parent_type";
				$ival[]="column";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_COLUMN";
				$ikey[]="file_name";
				$ival[]=$lastInsertId."_cm_".$item[new_cover_img];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

			}

			// コミット
			$this->db->commit();

			$this->db->beginTransaction();
			if($item[new_cover_img]){
				$cover_img_name=URL_IMG_COLUMN.$lastInsertId."_cm_".$item[new_cover_img];
				$sql="update sf_column set cover_img='{$cover_img_name}' where id={$lastInsertId}";
				$ret=$commonDao->run_sql($sql);
			}
			// コミット
			$this->db->commit();

		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'sf_column'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;

	}
	/**
	 * コラム 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_column set $ins $where";

		$this->db->beginTransaction();
		try {
			//print $sql."<br>";
			// 実行
			$this->executeUpdate($sql);

			//トップの画像
			if($item[new_cover_img]){
				$ikey=array();
				$ival=array();

				//既存の画像の削除
				$ret=$commonDao->del_Data("general_images",array("image_type","parent_no"),array("COLUMN_COVER",$item[id]));

				$ikey[]="image_type";
				$ival[]="COLUMN_COVER";
				$ikey[]="parent_no";
				$ival[]=$item[id];
				$ikey[]="parent_type";
				$ival[]="column";
				$ikey[]="sort";
				$ival[]=1;
				$ikey[]="dir";
				$ival[]="DIR_IMG_COLUMN";
				$ikey[]="file_name";
				$ival[]=$item[id]."_cm_".$item[new_cover_img];

				$ikey[]="insert_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("general_images",$ikey,$ival);

			}

			// コミット
			$this->db->commit();

			$this->db->beginTransaction();
			if($item[new_cover_img]){
				$cover_img_name=URL_IMG_COLUMN.$item[id]."_cm_".$item[new_cover_img];
				$sql="update sf_column set cover_img='{$cover_img_name}' where id={$item[id]}";
				$ret=$commonDao->run_sql($sql);
			}
			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Column'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		$sql="select count(*) as cnt from sf_column as p
			inner join sf_column_category as a on p.column_category_no=a.cat_no ".$tbl.$where;

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//ID
		if($search["column_id"]){
			$whTmp[]="p.id =".addslashes($search["column_id"]);
		}
		//email
		if($search["email"]){
			$whTmp[]="email ='".addslashes($search["email"])."'";
		}
		//pass
		if($search["password"]){
			$whTmp[]="password ='".addslashes(to_hash($search["password"]))."'";
		}
		//名前
		if($search["name"]){
			$tmp[]="name like '%".addslashes($search["name"])."%'";
			$tmp[]="public_name like '%".addslashes($search["name"])."%'";

			$whTmp[]="(".implode(" or ",$tmp).")";
		}
		//コラムタイトル
		if($search["column_title"]){
			$whTmp[]="p.title like '%".addslashes($search["column_title"])."%'";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}


		return array($where,$tbl);

	}

	/**
	 * コラム検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="",$langcode="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {

			if(!$search["page"]) $search["page"]=1;

			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}
		$sql="select p.* , a.cat_name from sf_column as p
			inner join sf_column_category as a on p.column_category_no=a.cat_no "
			.$where.$ord.$limit_str;

		//print $sql."<br>";

		$rs=$this->executeQuery($sql);
		$Arr=array();
		for($i=0;$i<count($rs);$i++){
			foreach ($rs[$i] as $key => $val){
				$data{$key} = stripslashes(rtrim($val));
			}
			array_push($Arr,$data);

		}

		return $Arr;

	}

	/**
	 * Columnの削除
	 */
	public function delete($column_id) {
		$commonDao = new CommonDAO();

		$this->db->beginTransaction();
		try{

			$sql = "DELETE FROM sf_column WHERE id = {$column_id}";
			// 実行
			$this->executeUpdate($sql);

			//関連のあるものすべて削除

			// 画像の削除
			$tmp=$commonDao->get_data_tbl("general_images",array("parent_type","parent_no"),array("column",$column_id));

			$sql = "DELETE FROM general_images WHERE parent_type ='column' and parent_no = {$column_id}";
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MSG_LEVEL_ERROR, "Failed to delete 'Column'. " . $e);
			$this->db->rollBack();
			throw new Exception($e);
			return false;
		}
		return true;
	}

	/**
	 * コラム詳細説明の更新

	 */
	public function updateColumnDetail($data,$column_id,$lang) {
		$sql="update sf_column set content='".$data."' where id=".$column_id;

		//print $sql."<br>";
		try {
			$this->db->beginTransaction();

			// 実行
			//$this->executeUpdate($sql,$this->get_db());
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to Update." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}

	/**
	 * カテゴリ プルダウン用の配列取得

	 */

	public function getCategoryOptionArr() {
		$sql = "SELECT
		            cat_no,cat_name
				FROM
					sf_column_category
				WHERE del_flg=0
			 ";

		$ret = $this->executeQuery($sql);

		$Arr[""]="選択してください";
		if($ret){
			foreach($ret as $data){
				$Arr[$data[cat_no]]=$data[cat_name];
			}
		}
		return $Arr;
	}

}
?>