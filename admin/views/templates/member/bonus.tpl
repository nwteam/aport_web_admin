{include file="head.tpl"}
<script type="text/javascript">
{literal}


{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>ユーザー</h3>

<h4>いいね（ボーナス）履歴</h4>
{include file="messages.tpl"}

		←<a href="/member/list">ユーザー一覧へ</a>

		{* 検索結果 *}

{$memArr.name}さんのボーナス記録

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>

			<form name="fm_list" id="fm_list" method="POST" action="pay/history/">

			<table class="admins_m">
			<tr>
				<th width="80">日付</th>
				<th width="20">ポイント</th>
				<th width="80">状況</th>
				<th width="80">説明</th>
			</tr>
			{foreach from=$arr item="item"}
				<tr>
					<td>{$item.update_date}</td>
					<td>{$item.points}</td>
					<td>
					{if $item.stat==1}
						追加
					{elseif $item.stat==0}
						没収
					{else}
						削除
					{/if}

					</td>
					<td>{$item.notes|nl2br}</td>

				</tr>
			{foreachelse}
				<tr>
					<td colspan="4">ボーナス履歴はありません。</td>
				</tr>
			{/foreach}
			</table>

			</form>

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

