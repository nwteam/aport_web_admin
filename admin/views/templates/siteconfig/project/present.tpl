{include file="head.tpl"}
<script type="text/javascript">
{literal}

// 入力チェック
/*
function validate() {
	var msg = "";

	// ユーザーID
	if ($("").val() == "") {
		msg += "・ユーザーIDを入力してください\n";
	}
	// 管理者種別
	if ($("#manager_type").val() == "") {
		msg += "・管理者種別を選択してください\n";
	}
	// 名前
	if ($("#user_name").val() == "") {
		msg += "・名前を入力してください\n";
	}

	if ($("#mode").val() == "add") {
		if ($("#password").val() == "") {
			msg += "・パスワードを入力してください\n";
		}
	}
	else {
		if ($("#password").val() != "" && $("#mode").val() != "add") {
			if (!confirm("パスワードを変更してもよろしいですか？")) {
				return false;
			}
		}
	}

	if (msg != "") {
		alert(msg);
		return false;
	}

	$("#fm").submit();
	return true;
}
*/

function validateList() {
	$("#fm_list").submit();
}

function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		{assign var="id" value=$project_info.status}
       <h4>プロジェクト 【{$project_info.public_title}】　<span style="font-size: 14px;">《ステータス：{$statusArr.$id}》　《支援者数：{$project_info.now_supporter}人》　《支援総額：{$project_info.now_summary|number_format}円》</span></h4>
		←<a href="/project/list/">プロジェクト一覧へ</a>

<ul class="tab clearfix mt20">
	<li ><a href="/project/editBasic/?no={$input_data.project_no}&lang={$input_data.lang}">基本情報</a></li>
	<li><a href="/project/editDetail/?no={$input_data.project_no}&lang={$input_data.lang}" >プロジェクト詳細説明</a></li>
	<li><a href="javascript:void(0);" style="background-color:#656d78;color:#FFF;">支援コース</a></li>
	<li><a href="/project/invest/?no={$input_data.project_no}&lang={$input_data.lang}" >支援一覧</a></li>
	<li><a href="{$smarty.const.ROOT_URL}{$input_data.lang}/detail-project.php?p_no={$input_data.project_no}&test_view=1"  target="_blank" >プレビュー</a></li>
</ul>
<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<div class="header_msg_info">{$edit_messages.error}</div>
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
		{*include file="messages.tpl"*}

		<form id="fm" name="fm" method="post" action="">
		<input type="hidden" id="mode" name="mode" value="{$mode}"/>
		<input type="hidden" id="present_no" name="present_no" value="{$input_data.present_no}" />
		<input type="hidden" id="project_no" name="no" value="{$input_data.project_no}" />
		<input type="hidden" id="project_no" name="project_no" value="{$input_data.project_no}" />
		<input type="hidden" id="lang" name="lang" value="{$input_data.lang}" />
		<h5>支援コース登録・編集</h5>
	<!-- 	<div><button type="button" name="sbm_save" onclick="javascript:location.href='project/present/?no={$input_data.project_no}&lang={$input_data.lang}';">新規コースの追加</button></div> -->
	<div class="left w50">
 		<table class="w100">
		<tr>
			<th>コースNo</th>
			<td>{$input_data.present_no}</td>
		</tr>
		<tr>
			<th>タイトル</th>
			<td>
		 		{if $result_messages.title}
					<span class="red">{$result_messages.title}</span><br />
				{/if}
				<input type="text" id="title" name="title" class="long" value="{$input_data.title}" maxlength="100"/>
			</td>
		</tr>
		<tr>
			<th>最低支援金額</th>
			<td>
		 		{if $result_messages.min}
					<span class="red">{$result_messages.min}</span><br />
				{/if}
				<input type="text" id="min" name="min" class="short ime_off" value="{$input_data.min}" />円
			</td>
		</tr>
		<tr>
			<th>支援限度数</th>
			<td>
		 		{if $result_messages.invest_limit}
					<span class="red">{$result_messages.invest_limit}</span><br />
				{/if}
				<input type="text" id="invest_limit" name="invest_limit" class="short ime_off" value="{$input_data.invest_limit}" />
				<br/>※限度数を上限なしに設定する場合は、「9999」と入力してください
			</td>
		</tr>
		<tr>
			<th>表示順</th>
			<td>
		 		{if $result_messages.sort}
					<span class="red">{$result_messages.sort}</span><br />
				{/if}
				<input type="text" id="sort" name="sort" class="short ime_off" value="{$input_data.sort}" />
			</td>
		</tr>
		</table>
		</div>
		<div class="right w50">
		<table class="w100">
		<tr>
			<th>詳細</th>
			<td>
		 		{if $result_messages.text}
					<span class="red">{$result_messages.text}</span><br />
				{/if}
				<textarea name="text" style="width: 80% " id="text">{$input_data.text}</textarea>
			</td>
		</tr>
		</table>
			</div>
	<div class="clear"> </div>
		<br/>
		<div class="center">
			{if $mode=="add" }
				<button type="submit" name="sbm_save" >新規保存</button>
			{else}
				<button type="submit" name="sbm_save" >保存</button>
			{/if}
				<button type="button" name="sbm_save" onclick="javascript:location.href='project/present/?no={$input_data.project_no}&lang={$input_data.lang}';">キャンセル</button>
		</div>
		</form>

		<h5>支援コース一覧</h5>
		<form method="POST" action="/project/bulkUpdate/">
		<table class="admins">
		<tr>
			<th >コースNo</th>
			<th class="id">タイトル</th>
			<th class="id">詳細</th>
			<th >最低支援金額</th>
			<th >支援限度数</th>
			<th >支援数</th>
			<th >表示順</th>
			<th >削除</th>
		</tr>
		{foreach from=$present_list item="item"}
			<tr>
				<td><a href="project/present/?no={$item.project_no}&lang={$item.lang}&present_no={$item.present_no}">{$item.present_no}</a></td>
				<td><a href="project/present/?no={$item.project_no}&lang={$item.lang}&present_no={$item.present_no}">{$item.title}</a></td>
				<td>{$item.text|mb_truncate:20:'...'}</td>
				<td>{$item.min|number_format}円</td>
				<td>{$item.invest_limit}</td>
				<td>{$item.invest_cnt}件</td>
				<td>{$item.sort}</td>
				<td><input type="checkbox" name="delete_dummy[]" id="delete_dummy_{$item.present_no}" value="{$item.present_no}" onclick="clickDeleteChk(this)"/></td>
			</tr>
		{foreachelse}
		<tr><td colspan="8">支援コースは登録されていません</td></tr>
		{/foreach}
		</table>
		<div class="center">
			<input type="submit" value="削除" onclick="return confirm('選択した支援コースを削除します。良いですか？')">
			<input type="hidden" id="project_no" name="project_no" value="{$input_data.project_no}" />
			<input type="hidden" id="lang" name="lang" value="{$input_data.lang}" />
		</div>
		</form>
	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

