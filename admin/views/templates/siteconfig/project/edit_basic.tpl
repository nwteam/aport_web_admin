{include file="head.tpl"}
<script language="JavaScript">
<!--
{literal}

$(function(){
	$("#invest_limit").datepicker({
		dateFormat: "yy/mm/dd"
	});
	$("#project_success_date").datepicker({
		dateFormat: "yy/mm/dd"
	});


});
	/* 調整額変更処理 */
	function ChangeAdminSummary(){
		if($.isNumeric($("#admin_summary").val())){
			var admin_now_summary=parseInt($("#admin_summary").val())  +parseInt($("#now_summary").val());
			var admin_now_summary_view = admin_now_summary.toString().replace(/(\d)(?=(\d\d\d)+$)/g , '$1,');
			$("#now_admin_summary_view").text(admin_now_summary_view);
			$("#now_admin_summary").val(admin_now_summary);
		}else{
			alert("数値を入力してください");
		}
	    //sum_prices();
	}

	/* 調整シェア数変更処理 */
	function ChangeAdminShare(){
		if($.isNumeric($("#admin_share").val())){
			var admin_now_share=parseInt($("#admin_share").val())  +parseInt($("#sum_share").val());
			var sum_admin_share_view = admin_now_share.toString().replace(/(\d)(?=(\d\d\d)+$)/g , '$1,');
			$("#sum_admin_share_view").text(sum_admin_share_view);
			$("#sum_admin_share").val(admin_now_share);
		}else{
			alert("数値を入力してください");
		}
	    //sum_prices();
	}


{/literal}
//-->
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		{assign var="id" value=$input_data.status}
		<h4>プロジェクト {if $mode=="add"}新規登録{else}【{$input_data.public_title}】　<span style="font-size: 14px;">《ステータス：{$statusArr.$id}》　《支援者数：{$input_data.now_supporter}人》　《支援総額：{$input_data.now_admin_summary|number_format}円》{/if}</span></h4>
			←<a href="/project/list/">プロジェクト一覧へ</a>

		<ul class="tab clearfix mt20">
			<li ><a href="javascript:void(0);" style="background-color:#656d78;color:#FFF;">基本情報</a></li>
			{if $input_data.no}
			<li><a href="/project/editDetail/?no={$input_data.no}&lang={$input_data.lang}">プロジェクト詳細説明</a></li>
			<li><a href="/project/present/?no={$input_data.no}&lang={$input_data.lang}">支援コース</a></li>
			<li><a href="/project/invest/?no={$input_data.no}&lang={$input_data.lang}" >支援一覧</a></li>
			<li><a href="{$smarty.const.ROOT_URL}{$input_data.lang}/detail-project.php?p_no={$input_data.no}&test_view=1"  target="_blank" >プレビュー</a></li>
			{/if}
		</ul>
		<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
		{*include file="messages.tpl"*}
		<form action="" method="post"  enctype="multipart/form-data">
		<div class="left w50">
			<h5 class="project_image">基本情報</h5>
			<table class="w100">
				<tr>
				  <th>プロジェクトNo</th>
				  <td>{$input_data.no}</td>
				  <input type="hidden" name="no" value="{$input_data.no}"  /></td>
				  <input type="hidden" name="lang" value="{$input_data.lang}"  /></td>
				</tr>
				<tr>
				  <th>タイトル<span class="red">※</span></th>
				  <td>
						{if $result_messages.public_title}
							<span class="red">{$result_messages.public_title}</span><br />
						{/if}
				    <input class="long" type="text" name="public_title" value="{$input_data.public_title}"  /></td>
				</tr>
				<tr>
				  <th>カテゴリ<span class="red">※</span></th>
				  <td>
						{if $result_messages.category_no}
							<span class="red">{$result_messages.category_no}</span><br />
						{/if}
						{html_options name=category_no options=$CategoryArr selected=$input_data.category_no}
				</tr>
				<tr>
				  <th>起案者名<span class="red">※</span></th>
				  <td>
						{if $result_messages.project_owner}
							<span class="red">{$result_messages.project_owner}</span><br />
						{/if}
						{html_options name=project_owner options=$ActressArr selected=$input_data.project_owner}
				</tr>
				<tr>
				  <th>達成条件　支援額<span class="red">※</span></th>
				  <td>
						{if $result_messages.wish_price}
							<span class="red">{$result_messages.wish_price}</span><br />
						{/if}
				  <input class="short ime_off" type="text" name="wish_price" value="{$input_data.wish_price}"  />円</td>
				</tr>
				<tr>
				  <th>サポート募集期限<span class="red">※</span></th>
				  <td>
						{if $result_messages.invest_limit}
							<span class="red">{$result_messages.invest_limit}</span><br />
						{/if}

				  <input id="invest_limit" class="long ime_off " type="text" name="invest_limit" value="{$input_data.invest_limit|date_format:"%Y/%m/%d"}"  /></td>
				</tr>
				<tr>
				<th>運営調整　支援額<span class="red">※</span></th>
				  <td>
						{if $result_messages.admin_summary}
							<span class="red">{$result_messages.admin_summary}</span><br />
						{/if}
				  <input class="short ime_off" type="text" name="admin_summary" id="admin_summary" value="{$input_data.admin_summary}" onChange="ChangeAdminSummary();" />円</td>
				</tr>
				<tr>
				  <th>プロジェクト公開日</th>
				  <td>
						{if $input_data.project_record_date>0}{$input_data.project_record_date|date_format:"%Y/%m/%d"}{/if}
							<input type="hidden" name="project_record_date" value="{$input_data.project_record_date}" />
					</td>
				</tr>
				<tr>
				  <th>プロジェクト成立日</th>
				  <td>
						{if $input_data.project_success_date>0}{$input_data.project_success_date|date_format:"%Y/%m/%d"}{/if}
							<input type="hidden" name="project_success_date" value="{$input_data.project_success_date}" />
					</td>
				</tr>
				<tr>
				  <th>ステータス<span class="red">※</span></th>
				  <td>
						{if $result_messages.status}
							<span class="red">{$result_messages.status}</span><br />
						{/if}
						{if $mode=="add"}
							掲載前
							<input type="hidden" name="status" value=0 />
						{else}
							{html_options name=status options=$statusArr selected=$input_data.status}
						{/if}
							<input type="hidden" name="org_status" value="{$input_data.org_status}" />
					</td>
				</tr>
			</table>
			<h5 class="project_image">支援状況</h5>
			<table class="w100">
				<tr>
				  <th>支援金額</th>
				  <td>
					支援金額：{$input_data.now_summary|number_format}円	<br/>
					調整後 支援金額:<span id="now_admin_summary_view"> {$input_data.now_admin_summary|number_format}</span>円
							<input type="hidden" name="now_summary" id="now_summary" value="{$input_data.now_summary}" />
							<input type="hidden" name="now_admin_summary" id="now_admin_summary" value="{$input_data.now_admin_summary}" />
				  </td>
				</tr>
			</table>
		</div>
		<div class="right w50">
			<h5 class="project_image">Top動画</h5>
			<table class="w100">
				<tr>
					<th>動画種類<span class="red">※</span></th>
					<td>
						{if $result_messages.movie_type}
							<span class="red">{$result_messages.movie_type}</span><br />
						{/if}
						{foreach from=$movietypeArr item="movie_type" key="id" }
							<label>
								<input id="movie_type" type="radio" name="movie_type" value="{$id}" {if $input_data.movie_type==$id}checked{/if} onChange="movie_type_change({$id})"/>{$movie_type}
							</label>
							&nbsp;
						{/foreach}
						<label>
							<input id="movie_type" type="radio" name="movie_type" value="0" {if $input_data.movie_type==0}checked{/if} onChange="movie_type_change(0)"/>指定しない
						</label>
					</td>
				</tr>
				<tr id="movie1" {if $input_data.movie_type!=1} style="display: none;"{/if}>
				  <th >ビデオ動画<span class="red">※</span></th>
				  <td>
					{if $result_messages.movie1}
						<span class="red">{$result_messages.movie1}</span><br />
					{/if}
					<div>動画ファイル：</div>
						{if $smarty.session.TMP_FUP.1!=""}
							{$smarty.session.TMP_FUP.1}<br/>
						{elseif $input_data.movie1.file_name!=""}
							{$input_data.movie1.file_name}<br/>
							<input type="hidden" name="movie1[file_name]" value="{$input_data.movie1.file_name}" />
							<input type="hidden" name="movie1[image_no]" value="{$input_data.movie1.image_no}" />
						{/if}
						<input type="file" name="up_file1"  size="30"/>
						<div>静止画：</div>
						{if $smarty.session.TMP_FUP2!=""}
							<img src="{$smarty.const.URL_IMG_TMP}{$smarty.session.TMP_FUP2}" width=300 />
						{elseif $input_data.movie1_2.file_name!=""}
							<img src="{$smarty.const.URL_IMG_PROJECT}{$input_data.movie1_2.file_name}?{$smarty.now}" width=300 />
							<input type="hidden" name="movie1_2[file_name]" value="{$input_data.movie1_2.file_name}" />
							<input type="hidden" name="movie1_2[image_no]" value="{$input_data.movie1_2.image_no}" />
						{/if}
						<input type="file" name="up_file1_2"  size="30"/>
					</td>
				</tr>
				<tr id="movie2" {if $input_data.movie_type!=2} style="display: none;"{/if}>
				  <th >Youtube<span class="red">※</span></th>
				  <td>
						{if $result_messages.movie2}
							<span class="red">{$result_messages.movie2}</span><br />
						{/if}
							URL：
					<!--    <input type="text" class="long" name="movie2[file_name]" value="{$input_data.movie2.file_name}"  /> -->
					<textarea name="movie2[file_name]">{$input_data.movie2.file_name}</textarea>
					</br>動画のサイズを740×416に指定した埋め込みコードを登録してください。
							<input type="hidden" name="movie2[image_no]" value="{$input_data.movie2.image_no}" />
				      </td>
				</tr>
				<tr id="movie3" {if $input_data.movie_type!=3} style="display: none;"{/if}>
				  <th >アニメーションGIF<span class="red">※</span></th>
				  <td>
						{if $result_messages.movie3}
							<span class="red">{$result_messages.movie3}</span><br />
						{/if}
						{if $smarty.session.TMP_FUP.3!=""}
							<img src="{$smarty.const.URL_IMG_TMP}{$smarty.session.TMP_FUP.3}" width=300 />
						{elseif $input_data.movie3.file_name!=""}
							<img src="{$smarty.const.URL_IMG_PROJECT}{$input_data.movie3.file_name}?{$smarty.now}" width=300 />
							<input type="hidden" name="movie3[file_name]" value="{$input_data.movie3.file_name}" />
							<input type="hidden" name="movie3[image_no]" value="{$input_data.movie3.image_no}" />
						{/if}
						<input type="file" name="up_file3"  size="30"/>
						</td>
							<input type="hidden" name="new_movie" value="{$input_data.new_movie}" />
				</tr>
			</table>
			<h5 class="project_image">サムネイル</h5>
			<table class="w100">
				<tr>
				  <th >サムネイル<span class="red">※</span></th>
				  <td>
					{if $result_messages.profile_image}
						<span class="red">{$result_messages.profile_image}</span><br />
					{/if}
					{if $smarty.session.TMP_FUP4!=""}
						<img src="{$smarty.const.URL_IMG_TMP}{$smarty.session.TMP_FUP4}" width=587 height=419 />
					{elseif $input_data.cover_img.file_name!=""}
						<img src="{$smarty.const.URL_IMG_PROJECT}{$input_data.cover_img.file_name}?{$smarty.now}" width=587 height=419 />
						<input type="hidden" name="cover_img[file_name]" value="{$input_data.cover_img.file_name}" />
						<input type="hidden" name="cover_img[image_no]" value="{$input_data.cover_img.image_no}" />
					{/if}
					<input type="file" name="up_file4"  size="30"/>
					<br />1000px以上の正方形の画像をご用意ください
					</td>
				</tr>
			</table>
		</div>
		<div class="clear"> </div>
		{*if $finish_flg!=1*}
			<div class="submit center">
			<input type="submit"  name="submit" value="保存" />
			</div>
		{*/if*}
		</div>
		</form>
	</div>
</div>
{include file="footer.tpl"}
</body>
</html>
