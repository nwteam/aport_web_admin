{include file="head.tpl"}
<script type="text/javascript">
{literal}


function validateList() {
	$("#fm_list").submit();
}

function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		{assign var="id" value=$project_info.status}
       <h4>プロジェクト 【{$project_info.public_title}】　<span style="font-size: 14px;">《ステータス：{$statusArr.$id}》　《支援者数：{$project_info.now_supporter}人》　《支援総額：{$project_info.now_summary|number_format}円》</span></h4>
		←<a href="/project/list/">プロジェクト一覧へ</a>

<ul class="tab clearfix mt20">
	<li ><a href="/project/editBasic/?no={$project_no}&lang={$lang_code}">基本情報</a></li>
	<li><a href="/project/editDetail/?no={$project_no}&lang={$lang_code}" >プロジェクト詳細説明</a></li>
	<li><a href="/project/present/?no={$project_no}&lang={$lang_code}">支援コース</a></li>
	<li><a href="javascript:void(0);" style="background-color:#656d78;color:#FFF;">支援一覧</a></li>
	<li><a href="{$smarty.const.ROOT_URL}{$lang_code}/detail-project.php?p_no={$project_no}&test_view=1"  target="_blank" >プレビュー</a></li>
</ul>
<span class="red">※データを変更した場合は、他のタブに切り替える前に必ず[保存]を実行してください。</span>
		{if $result_messages}
			<div class="header_msg_error">入力エラーがあります</div>
		{/if}
		{if $edit_messages.info}
			<div class="header_msg_info">
				<div>{$edit_messages.info}</div>
			</div>
		{/if}
		{if $edit_messages.error}
			<center><span class="red">{$edit_messages.error}</span></center>
		{/if}
		{include file="messages.tpl"}

		<table class="admins">
		<tr>
			<th >支援No</th>
			<th >支援ユーザー</th>
			<th >支援金額</th>
			<th >支援コース</th>
			<th >メッセージ</th>
			<th >ステータス</th>
			<th >支援登録日</th>
			<th >決済ID</th>
			<th >決済処理</th>
			<th >削除</th>
		</tr>
		{foreach from=$invest_list item="item"}
			<tr>
				<td>{$item.invest_no}</td>
				<td><a href="/member/edit/?user_no={$item.member_id}">{$item.member_id}:{$item.member_name}</a></td>
				<td>{$item.invest_amount|number_format}円</td>
				<td><a href="/project/present/?no={$item.project_no}&lang=ja&present_no={$item.present_no}">{$item.present_no}</td>
				<td>{$item.comment|nl2br}</td>
				<td>
					{assign var="id" value=$item.invest_status}
					{$invest_statusArr.$id}
				</td>
				<td>{$item.invest_date|date_format:"%Y/%m/%d"}</td>
				<td>{$item.auth_no}</td>
				<td>
					{if $project_info.status==3 && $item.invest_status==2}
					<form action="" method="POST" >
					<input type="hidden" name="invest_no" value="{$item.invest_no}" />
					<input type="submit" name="credit" value="決済実行" />
					</form>
					{/if}
				</td>
				<td>
					<form action="" method="POST">
					<input type="hidden" name="invest_no" value="{$item.invest_no}" />
					<input type="hidden" name="project_no" value="{$item.project_no}" />
					<input type="submit" name="delete" value="削除" onclick="return confirm('支援を削除します。良いですか？')"/>
					</form>
				</td>
			</tr>
		{foreachelse}
		<tr><td colspan="10">支援データーはありません</td></tr>
		{/foreach}
		</table>
<!--
		<div class="center">
			<input type="submit" value="削除" onclick="return confirm('選択した支援を削除します。良いですか？')">
			<input type="hidden" id="project_no" name="project_no" value="{$input_data.project_no}" />
			<input type="hidden" id="lang" name="lang" value="{$input_data.lang}" />
		</div>
 -->
 	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

