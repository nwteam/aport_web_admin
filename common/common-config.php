<?php
/**
 * common.config.php
 * @version 0.1
 * @author Suzuki
 * @since 2013/08/
 */
//ini_set("display_errors", 1);
ini_set("display_errors", "OFF");
// 内部エンコーディング
define("INTERNAL_ENCODING", "UTF-8");
// ライブラリのディレクトリパス
define("LIB_PATH", realpath(dirname(__FILE__) . "/../libs"));
// modelのディレクトリパス
define("MODEL_PATH", realpath(dirname(__FILE__) . "/../models"));

// 公開サイトのURLとドキュメントルート(※スラッシュで終えること)
define("TEMPLATE_ROOT", "/var/www/aport_admin/admin/views/templates/");
define("DOCUMENT_ROOT", "/var/www/aport/httpdocs/");
define("SHELL_ROOT", "/var/www/aport/httpdocs/");


// 管理サイトのURLとドキュメントルート(※スラッシュで終えること)
define("ROOT_URL_ADMIN", "http://admin.aport-asahi.com/");
define("DOCUMENT_ROOT_ADMIN", "/var/www/aport_admin/admin/htdocs/");


//URL
define("ROOT_URL", "http://aport-asahi.com/");
define("FACEBOOK_REDIRECT_MEMBER", ROOT_URL."mypage/facebookRedirct/");
define("FACEBOOK_REDIRECT_OWNER", ROOT_URL."owner/facebookRedirct/");

define("URL_NOSSL", "http://aport-asahi.com/");
define("URL_SSL", "https://aport-asahi.com/");


//サイト名
// define("SITE_TITLE", "facebook 会員管理");
define("SITE_TITLE", "A-port");

//サイト間共通セッション名
define("COMMON_SESSION", "aport-asahi.com");

// DB設定
define("DSN", "mysql:dbname=aport;host=aport.cid07kswul69.ap-northeast-1.rds.amazonaws.com");
define("DB_USER", "www");
define("DB_PASS", "arp2015");


//1p 表示件数
define("V_CNT", "10");

define("ADMIN_V_CNT", "20");

//管理者のメールアドレス
define("SITE_ADMIN_EMAIL", "info@a-port-mail.com");


//お問い合わせ用メールアドレス
define("INQ_ADMIN_EMAIL", "info@a-port-mail.com");

// メール差出人設定
define("MAIL_FROM", "info@a-port-mail.com");
define("MAIL_FROM_NAME", "A-port運営事務局");
define("ADMIN_EMAIL", "info@a-port-mail.com");

/*
 *
 define("MAIL_FROM", "support@aport-asahi.com");
 define("ADMIN_EMAIL", "info@aport-asahi.com");

 */
// メールCC設定
define("MAIL_CC", "info@a-port-mail.com");	//送信しない場合はブランクにする

//クレジットカード決済 CREDIX

//テスト用
//define("CDREDIX_CLIENTIP_JA", "1019000495");
//define("CDREDIX_CLIENTIP_OTHER", "1019000496");
//本番用

define("CDREDIX_CLIENTIP_JA", "1011003684");
define("CDREDIX_CLIENTIP_OTHER", "1011003685");
 
	// メッセージレベル
define("SYSTEM_MESSAGE_DEBUG", 1);
define("SYSTEM_MESSAGE_INFO", 2);
define("SYSTEM_MESSAGE_ALERT", 3);
define("SYSTEM_MESSAGE_VALID", 4);
define("SYSTEM_MESSAGE_WARN", 5);
define("SYSTEM_MESSAGE_ERROR", 6);
define("SYSTEM_MESSAGE_FATAL", 7);

// 会員のログイン情報保持セッション名
define("MEMBER_SESSION_NAME", "member_session");

// 店舗管理者のログイン情報保持セッション名
define("SHOP_MANAGER_SESSION_NAME", "shop_manager_session");

// サイト管理者のログイン情報保持セッション名
define("SITE_ADMIN_SESSION_NAME", "site_admin_session");

// 会員のプロフィール画像の最大サイズ(バイト)
define("MEMBER_PROFILE_IMAGE_MAX_SIZE", 153600);

// 会員の仮登録→本登録までの猶予(時間)
define("MEMBER_REGIST_APPLY_LIMIT_HOUR", 24);

// 画像URL
define("URL_IMG_TMP", ROOT_URL ."image-upload/tmp/");
//define("URL_IMG_MEMBER", ROOT_URL ."image-upload/img_member/");
//define("URL_IMG_NEWS", ROOT_URL ."image-upload/img_news/");
//define("URL_IMG_LOGO", ROOT_URL ."image-upload/img_logo/");
//define("URL_IMG_JOB", ROOT_URL ."image-upload/img_job/");
define("URL_IMG_TMP", ROOT_URL ."image-upload/tmp/");
define("URL_IMG_AD", ROOT_URL ."image-upload/banner/");
define("URL_IMG_SLIDE", ROOT_URL ."image-upload/slide/");
define("URL_IMG_ACTRESS", ROOT_URL ."image-upload/actress/");
define("URL_IMG_PROJECT", ROOT_URL ."image-upload/project/");
define("URL_IMG_PROJECT_CONTENT", ROOT_URL."image-upload/project/content/");
define("URL_IMG_COLUMN", ROOT_URL ."image-upload/column/");


// 画像ディレクトリ
define("DIR_IMG_TMP", DOCUMENT_ROOT . "image-upload/tmp/");
//define("DIR_IMG_MEMBER", DOCUMENT_ROOT . "image-upload/img_member/");
//define("DIR_IMG_NEWS", DOCUMENT_ROOT . "image-upload/img_news/");
//define("DIR_IMG_LOGO", DOCUMENT_ROOT . "image-upload/img_logo/");
//define("DIR_IMG_JOB", DOCUMENT_ROOT . "image-upload/img_job/");
define("DIR_IMG_AD", DOCUMENT_ROOT . "image-upload/banner/");
define("DIR_IMG_SLIDE", DOCUMENT_ROOT . "image-upload/slide/");
define("DIR_IMG_ACTRESS", DOCUMENT_ROOT . "image-upload/actress/");
define("DIR_IMG_PROJECT", DOCUMENT_ROOT . "image-upload/project/");
define("DIR_IMG_PROJECT_CONTENT", DOCUMENT_ROOT . "image-upload/project/content/");
define("DIR_IMG_COLUMN", DOCUMENT_ROOT . "image-upload/column/");


//法人登録の承認作業
define("COMPANY_ADMIT", 0);//承認作業ありの場合は0　承認作業無しの場合は1 (この値が登録時のadmit_flgに入る）



define("NORMAL_POINT_FLG", 0);//いいねフラグ番号
define("LOGIN_POINT_FLG", 1);//ログインフラグ番号
define("BORNAS_POINT_FLG", 2);//ボーナスフラグ番号
define("ADMIN_POINT_FLG", 3);//運営者からのフラグ
define("BILL_POINT_FLG", 4);//請求


//多言語対応
define("MAIN_LANGUAGE", "ja");//メイン言語

//
// Spreaders
//
//ポイント
define("STD_LIKE_POINT", 15);// いいね一回で取得できるポイント。デフォルトは15
define("LOGIN_POINT", 5);//ログインポイント
define("BORNAS_POINT", 5);//ボーナスポイント

// １ポイントの請求額。デフォルトは1ポイント60円。
define("LIKE_EXCHANGE_RATE", 60);

//オーナーの支配期限　月末からxx後にするか
define("PAY_DAY", 10);




// 共通配列クラス
class CommonArray {

//アプリの情報を$configに格納
/*
	public static $facebook_config = array(
    				'appId' => "589506687805257",
	    			'secret' => "a73513d45a785f062969257f2c004745",
					'redirect' => FACEBOOK_REDIRECT
		);
*/

  // 使用言語
  public static $array_lang = array(
	"ja" => "日本語",
	"tw" => "中文(繁体)",
  	"cn" => "中文(簡体)",
	"en" => "English",
  );

  // 使用言語
  public static $array_lang_p = array(
	"public" => "共通",
  	"ja" => "日本語",
	"tw" => "中文(繁体)",
  	"cn" => "中文(簡体)",
	"en" => "English",
  );

        // 本番用とりあえずアピス作成のFBアプリアカウント
        public static $facebook_config = array(
                                'appId' => "789342891084857",
                                'secret' => "04f3932b2ce045366dc689dc409ea1d5",
                                        'redirect' => FACEBOOK_REDIRECT_MEMBER
                );




	//会員ステータス
	public static $member_status_array = array(
		"0" => "仮登録",
		"1" => "本登録",
		"8" => "退会希望",
		"9" => "退会済",
	);

	//起案者登録タイプ
	public static $entry_type_array = array(
		"1" => "公募",
		"2" => "プロダクション",
	);

	//起案者ステータス
	public static $actress_status_array = array(
		"0" => "新規申請(仮登録)",
		"1" => "採用",
		"2" => "登録済み",
		"99" => "不採用",
	);


	// 起案者カップ
	public static $actress_cup_array = array(
		"" => "",
		"A" => "A",
		"B" => "B",
		"C" => "C",
		"D" => "D",
		"E" => "E",
		"F" => "F",
		"G" => "G",
		"H" => "H",
		"I" => "I",
		"J" => "J",
	);

	// 起案者テイスト
	public static $actress_taste_array = array(
		"" => "",
		"S" => "S",
		"M" => "M",
		"秘密" => "秘密",
	);

	//プロジェクトステータス
	public static $project_status_array = array(
		"0" => "掲載前",
		"1" => "掲載中",
		"3" => "達成",
		"9" => "未達成",
	);
	//審査ステータス
	public static $check_status_array = array(
		"0" => "入稿中",
		"1" => "審査待",
		"2" => "修正依頼",
		"3" => "審査完了",
	);
	//プロジェクトステータス
	public static $s_project_status_array = array(
		"" => "",
		"0" => "掲載前",
		"1" => "掲載中",
		"3" => "達成",
		"9" => "未達成",
	);
	//審査ステータス
	public static $s_check_status_array = array(
		"" => "",
		"0" => "入稿中",
		"1" => "審査待",
		"2" => "修正依頼",
		"3" => "審査完了",
	);

	//プロジェクト動画種類
	public static $project_movietype_array = array(
		"1" => "ビデオ動画(mp4形式)",
		"2" => "Youtube",
		"3" => "アニメーションGIF",
	);

	//支援ステータス
	public static $invest_status_array = array(
		"0" => "カード認証前",
		"1" => "支援予約完了",
		"2" => "出資成立",
		"3" => "出資不成立",
		"5" => "決済完了",
		"91" => "決済失敗",
		"92" => "カード認証失敗",
		"99" => "不採用",
	);

	// 支払方法種別
	public static $pay_type_array = array(
		"1" => "銀行振り込み",
		"2" => "クレジットカード決済",
	);

	// 画像種別
	public static $image_type_array = array(
		"PROFILE_IMG" => "起案者プロフィール用画像",
		"2" => "クレジットカード決済",
	);

	//画像サイズ

	public static $photosize_array = array(
		"profile_img" => array(
					"w" => "250",
					"h" => "250",
				),
		"other" => array(
					"w" => "250",
					"h" => "250",
				),
		);

	//サムネイル画像サイズ
	public static $coversize_array = array(
		"cover_img" => array(
					"w" => "587",
					"h" => "419",
				),
	);
	//コラムTOP画像サイズ
	public static $topimgsize_array = array(
		"top_img" => array(
					"w" => "600",
					"h" => "227",
				),
	);
	//お問い合わせフォームの目的
	public static $inq_array = array(
		"1" => "ご質問",
		"2" => "ご要望",
		"3" => "サービス関連",
		"4" => "取材依頼関連",
		"5" => "その他",
	);


/*
	//画像名とサイズ
	public static $photo_array = array("L","S");

	public static $photoW_tate_array = array("270","240");
	public static $photoH_tate_array = array("360","320");

	public static $photoW_yoko_array = array("328","240");
	public static $photoH_yoko_array = array("246","180");
*/


	//携帯ドメイン
	public static $mobile_array = array(
		"1" => "@docomo.ne.jp",
		"2" => "@ezweb.ne.jp",
		"3" => "@d.vodafon.ne.jp",
		"4" => "@h.vodafon.ne.jp",
		"5" => "@t.vodafon.ne.jp",
		"6" => "@c.vodafon.ne.jp",
		"7" => "@k.vodafon.ne.jp",
		"8" => "@r.vodafon.ne.jp",
		"9" => "@n.vodafon.ne.jp",
		"10" => "@s.vodafon.ne.jp",
		"11" => "@q.vodafon.ne.jp",
		"12" => "@i.softbank.jp",
		"13" => "@pdx.ne.jp",
		"14" => "@di.pdx.ne.jp",
		"15" => "@dj.pdx.ne.jp",
		"16" => "@dk.pdx.ne.jp",
		"17" => "@wm.pdx.ne.jp",
		"18" => "@disney.ne.jp",
		"19" => "@willcom.com",

	);

	//最終学歴
	public static $lastEdu_array = array(
		"3" => "卒業",
		"4" => "卒業見込み",
		"2" => "中退",
	);

	//学歴区分
	public static $edu_array = array(
		"1" => "入学",
		"3" => "卒業",
		"4" => "卒業見込み",
		"2" => "中退",

	);

	//スキルシート 経験項目
	public static $skill_array = array(
		"0" => "なし",
		"1" => "独学",
		"2" => "実務～１年",
		"3" => "実務１～２年",
		"4" => "実務２年～",
	);



	//退会理由
	public static $resign_array = array(
		"1" => "当サイト経由で就職先が決定したから",
		"2" => "他媒体経由で就職が決定したから",
		"3" => "希望した職種に合った求人がなかったから",
		"4" => "希望した勤務地に合った求人がなかったから",
		"5" => "その他",
	);



	// 曜日配列
	public static $weekday_array = array( "日", "月", "火", "水", "木", "金", "土" );

	//エリア配列
	public static $area_array = array(
		"1" => "北海道",
		"2" => "東北",
		"3" => "関東",
		"4" => "北陸・甲信越",
		"5" => "東海",
		"6" => "近畿",
		"7" => "中国",
		"8" => "四国",
		"9" => "九州・沖縄",
		"10" => "その他"
		);


	// 都道府県配列
	public static $prefs_array = array(
		"北海道" => array("1" => "北海道"),
		"東北" => array(
						"2" => "青森県",
						"3" => "岩手県",
						"4" => "宮城県",
						"5" => "秋田県",
						"6" => "山形県",
						"7" => "福島県"),
		"関東" => array(
						"8" => "茨城県",
						"9" => "栃木県",
						"10" => "群馬県",
						"11" => "埼玉県",
						"12" => "千葉県",
						"13" => "東京都",
						"14" => "神奈川県"),
		"北陸・甲信越" => array(
						"15" => "新潟県",
						"16" => "富山県",
						"17" => "石川県",
						"18" => "福井県",
						"19" => "山梨県",
						"20" => "長野県"),
		"東海"		  => array(
						"21" => "岐阜県",
						"22" => "静岡県",
						"24" => "三重県",
						"23" => "愛知県"),
		"近畿" => array(

						"25" => "滋賀県",
						"26" => "京都府",
						"27" => "大阪府",
						"28" => "兵庫県",
						"29" => "奈良県",
						"30" => "和歌山県"),
		"中国" => array(
						"31" => "鳥取県",
						"32" => "島根県",
						"33" => "岡山県",
						"34" => "広島県",
						"35" => "山口県"),
		"四国" => array(
						"36" => "徳島県",
						"37" => "香川県",
						"38" => "愛媛県",
						"39" => "高知県"),
		"九州・沖縄" => array(
						"40" => "福岡県",
						"41" => "佐賀県",
						"42" => "長崎県",
						"43" => "熊本県",
						"44" => "大分県",
						"45" => "宮崎県",
						"46" => "鹿児島県",
						"47" => "沖縄県"),
		"その他" =>array(""=>"")
	);


	// 都道府県テーブル(前の番号に合わせる)
	public static $pref_text_array = array(
		"" => "選択してください",
		"1" => "北海道",
		"2" => "青森県",
		"3" => "岩手県",
		"4" => "宮城県",
		"5" => "秋田県",
		"6" => "山形県",
		"7" => "福島県",
		"8" => "茨城県",
		"9" => "栃木県",
		"10" => "群馬県",
		"11" => "埼玉県",
		"12" => "千葉県",
		"13" => "東京都",
		"14"=> "神奈川県",
		"15" => "新潟県",
		"16" => "富山県",
		"17" => "石川県",
		"18" => "福井県",
		"19" => "山梨県",
		"20" => "長野県",
		"21" => "岐阜県",
		"22" => "静岡県",
		"23" => "愛知県",
		"24" => "三重県",
		"25" => "滋賀県",
		"26" => "京都府",
		"27" => "大阪府",
		"28" => "兵庫県",
		"29" => "奈良県",
		"30" => "和歌山県",
		"31" => "鳥取県",
		"32" => "島根県",
		"33" => "岡山県",
		"34" => "広島県",
		"35" => "山口県",
		"36" => "徳島県",
		"37" => "香川県",
		"38" => "愛媛県",
		"39" => "高知県",
		"40" => "福岡県",
		"41" => "佐賀県",
		"42" => "長崎県",
		"43" => "熊本県",
		"44" => "大分県",
		"45" => "宮崎県",
		"46" => "鹿児島県",
		"47" => "沖縄県",
	);

	// 都道府県テーブル(前の番号に合わせる)
	public static $array_category = array(
  "" => "お選びください",
	"1" => "アート",
	"2" => "本",
	"3" => "映画",
	"4" => "ゲーム",
	"5" => "音楽",
	"6" => "パフォーマンス",
	"7" => "写真",
	"8" => "テクノロジー",
	"9" => "コミュニティ",
	"10" => "ファッション",
	"11" => "フード",
	"12" => "ジャーナリズム",
	"13" => "プロダクト",
	"14" => "アニメ",
	"15" => "演劇・ダンス",
	"16" => "スポーツ",
	"17" => "伝統工芸",
	"18" => "旅",
  );

}

?>
