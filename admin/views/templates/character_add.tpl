{include file="head.tpl"}
<script type="text/javascript">
{literal}

function editcancel()
{
	document.regForm.action="master/character/";
	document.regForm.submit();
}

{/literal}
</script>

<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>こだわり条件マスタ</h3>

{if $exec=="middle"}
	<h2>こだわり条件追加（中カテゴリー）	</h2>

{elseif $exec=="sub"}
	<h2>こだわり条件追加（小カテゴリー）	</h2>

{elseif $exec=="main"}
	<h2>こだわり条件追加（メインカテゴリー）</h2>
{else}
	<h2>こだわり条件編集</h1>
{/if}

{if $msg }

	{if $exec=="middle"}
		こだわり条件中カテゴリーを追加しました。
	{elseif $exec=="sub"}
		こだわり条件小カテゴリーを追加しました。
	{elseif $exec=="main"}
		こだわり条件大カテゴリーを追加しました。
	{else}
		こだわり条件カテゴリーを編集しました。
	{/if}

{else}
<form name="regForm" method="post" action="master/characteredit">
<input type="hidden" name="ctitle" value="{$ctitle}" />
<input type="hidden" name="id" value="{$id}" />
<input type="hidden" name="parentid" value="{$parentid}" />
<input type="hidden" name="exec" value="{$exec}" />
<input type="hidden" name="cflag" value="{$cflag}" />
<table class="admin_list01">
</td>
</tr>
<tr>
<th>※こだわり条件カテゴリー名</th>
{if $errmsg==1}
<td class="red">
こだわり条件は必須項目です。<br />
{else}
<td>
{/if}
<input type="text" name="name" value="{$name}" style="ime-mode:active;width:350px" />
</td>
</tr>

</table>
<p class="center">
	<input type="submit" name="regist" value="OK" />
	<input name="cancel" type="button" value="キャンセル" onclick="editcancel();" />
</p>
</form>

{/if}
</body>
</html>
