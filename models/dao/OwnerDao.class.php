<?php
class OwnerDAO extends DAOBase {


	/**
	 * オーナー登録
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return int 直近の挿入行ID
	 */
	public function InsertItemData($fi,$dt,$item=array()) {

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
			    	$tmp1[]=$fi[$i];
					$va=trim($dt[$i]);
					$tmp2[]="'".htmlspecialchars($va, ENT_QUOTES)."'";

		    	}
		}else if($fi){
		    	$tmp1[]=$fi;
			$dt=trim($dt);
			$dt=htmlspecialchars($dt, ENT_QUOTES);
			$tmp2[]="'".$dt."'";
		}
		$ins=implode(",",$tmp1);
		$valu=implode(",",$tmp2);

		$sql="insert into owner($ins) values($valu)";
		//var_dump($sql);

		$this->db->beginTransaction();
		try {

			// 実行
			$this->executeUpdate($sql);
			// 直近の挿入行IDを取得
			$lastInsertId = $this->db->lastInsertId();

			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'owner'." . $e);
			$this->db->rollBack();
			return false;
		}

		return $lastInsertId;;

	}

	/**
	 * オーナー会員 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $wfi:条件フィールド
	 *        $wdt：条件値
	 *        $freeze_set:凍結処理実行フラグ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$freeze_set=-1) {
		$commonDao = new CommonDao();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".addslashes($wdt[$i])."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".addslashes($wdt)."'";
		}

		$sql="update owner set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);
			//print "freeze_set:".$freeze_set."<br>";
			if($freeze_set>=0){
				//登録ページの凍結設定・解除処理を実行
				$result = $commonDao->updateData("fbpage","freeze_flg",$freeze_set,"owner_id",$wdt);

			}
			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'company'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

		/**
	 * 検索 件数
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function searchCount($search="") {

		list($where,$tbl)=$this->makeSearch($search);

		$sql="select count(*) as cnt from owner ".$tbl.$where;

		$tmp=$this->executeQuery($sql);

		return $tmp[0][cnt];

	}

	public function makeSearch($search) {

		//------- 検索条件 --------------------
		$where="";
		$tbl="";

		//名
		if($search["owner_name"]){
			$whTmp[]="owner_name like '%".addslashes($search["owner_name"])."%'";
		}
		//アドレス
		if($search["email"]){
			$whTmp[]="email like '%".addslashes($search["email"])."%'";
		}
		//キーワード
		if($search["keyword"]){
			$keyTmp=array();
			$keyTmp[]="nickname like '%".addslashes($search["keyword"])."%'";

			$whTmp[]="(".implode(" or ",$keyTmp).")";
		}

		//企業ID
		if(isset($search["owner_id"])){
			$whTmp[]="owner_id like '%".addslashes($search["owner_id"])."%'";
		}

		if($whTmp){
			$where=" where ".implode(" and ",$whTmp);
		}
		if($tblTmp){
			$tbl=" , ".implode(" , ",$tblTmp);
		}

		return array($where,$tbl);

	}

	/**
	 * 会員検索
	 * @param $search:検索条件
	 * @return 検索結果
	 */
	public function search($search="",$orderby="",$limit="") {


		list($where,$tbl)=$this->makeSearch($search);

		//ソート
		if($orderby<>""){
			if(is_array($orderby)){
		            for ($i=0;$i<count($orderby);$i++){
			    	$tmpo[]=$orderby[$i];
			    }

			    $ord="order by ".implode(",",$tmpo);

			}else{
				$ord=" order by $orderby $desc";
			}
		}

		//リミット
		if (!$limit) {
			$limit_str = "";
		} else {
			 if(!$search["page"]) $search["page"]=1;
			 $limit = (int)$limit;
			 $offset = ((int)$search["page"]  - 1) * $limit;
			 $limit_str = " LIMIT {$limit} OFFSET {$offset} ";
		}

		$sql="select * from owner".$where.$ord.$limit_str;


		$prodArr=$this->executeQuery($sql);

/*
		//属性情報を取得する
		$commonDao=new CommonDao();
		for($i=0;$i<count($prodArr);$i++){
			$sql="select * from company_ext_attrs where company_id=".$prodArr[$i][company_id]." order by attr_key , seq";
			$tmp=$this->executeQuery($sql);
			for($j=0;$j<count($tmp);$j++){
				$attr_key=$tmp[$j][attr_key];
				if($tmp[$j][attr_value1]){
					$prodArr[$i][$attr_key]=$tmp[$j][attr_value1];
				}
				else{
					$prodArr[$i][$attr_key]=$tmp[$j][attr_value3];
				}
			}
		}
*/
		return $prodArr;

	}

	//削除
	/**
	 * 法人削除　関連データも全て削除
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 * @return
	 */
	public function delData($owner_id) {


		$this->dbConnection();
		try {

			$this->db->beginTransaction();

			$sql="delete from owner where owner_id=".$owner_id;
			$this->executeUpdate($sql);
			$sql="delete from owner_billing where owner_id=".$owner_id;
			$this->executeUpdate($sql);
			$sql="delete from fbpage where owner_id=".$owner_id;
			$this->executeUpdate($sql);


			// コミット
			$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to delete." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;


	}




}
?>

