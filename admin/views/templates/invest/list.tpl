{include file="head.tpl"}
<script type="text/javascript">
{literal}

function clearSearchForm() {
	$("#project_no").val("");
	$("#project_name").val("");
	$("#user_no").val("");
	$("#nickname").val("");
}


function clickDisableChk(obj) {
	var id = $(obj).attr("id").replace("disabled_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#disabled_" + id).val("t");
	}
	else {
		$("#disabled_" + id).val("f");
	}
}

function clickDeleteChk(obj) {
	var id = $(obj).attr("id").replace("delete_dummy_", "");
	if ($(obj).attr("checked") == "checked") {
		$("#delete_" + id).val("t");
	}
	else {
		$("#delete_" + id).val("f");
	}
}

{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
	{include file="sidebar.tpl"}
	<div id="main_content">
<h4>支援一覧</h4>
	←<a href="/member/list/">会員一覧へ</a>

		{include file="messages.tpl"}
		{if $result_messages}
			<center><span class="red">{$result_messages}</span></center>
		{/if}

		<form method="post" name="fm_search" action="invest/list">
		<table class="search">
		<tr>
			<th>プロジェクトNo：</th>
			<td>
				<input type="text" name="project_no" id="project_no"  value="{$search.project_no}" size="4" />
			</td>
			<th>プロジェクト名：</th>
			<td>
				<input type="text" name="project_name" id="project_name"  value="{$search.project_name}" size="20" />
			</td>
		</tr>
		<tr>
			<th>会員No：</th>
			<td>
				<input type="text" name="user_no" id="user_no"  value="{$search.user_no}" size="4" />
			</td>
			<th>ニックネーム：</th>
			<td>
				<input type="text" name="nickname" id="nickname"  value="{$search.nickname}" size="20" />
			</td>
		</tr>
		</table>
		<div>
			<button type="submit" name="sbm_search" >検索</button>&nbsp;
			<button type="button" onClick="clearSearchForm()">クリア</button>
		</div>
		</form>
<hr>

		{* 検索結果 *}

			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="right red">※決済完了後の支援は削除することはできません。</div>
			</div>
			<br/>
			<form name="fm_list" id="fm_list" method="POST" action="comment/list/">

			<table class="admins clear">
			<tr>
				<th width="20">支援No</th>
				<th width="120">プロジェクト名</th>
				<th >支援ユーザー</th>
				<th >支援金額</th>
				<th style="width:120px;">支援コース</th>
				<th width="120">メッセージ</th>
				<th >ステータス</th>
				<th >決済ID</th>
				<th >支援登録日</th>
				<th width="40">削除</th>
			</tr>
			{foreach from=$invest_list item="item"}
				<tr>
				<!-- 	<td><a href="invest/edit/?no={$item.no}">{$item.no}</a></td> -->
					<td>{$item.no}</td>
					<td>{$item.project_name}</td>
					<td><a href="/member/edit/?user_no={$item.member_id}">{$item.member_id}:{$item.member_name}</a></td>
					<td>{$item.invest_amount|number_format}円</td>
					<td><a href="/project/present/?no={$item.project_no}&lang=ja&present_no={$item.present_no}">{$item.present_no}</td>
					<td>{$item.comment}</td>
					<td>
						{assign var="id" value=$item.status}
						{$invest_statusArr.$id}
					</td>
					<td>{$item.auth_no}</td>
					<td>{$item.create_date|date_format:"%Y/%m/%d"}</td>
					<td>
						{if $item.status==5}
						<p class="red">削除不可</p>
						{else}
						<form action="" method="POST">
						<input type="hidden" name="invest_no" value="{$item.invest_no}" />
						<input type="hidden" name="project_no" value="{$item.project_no}" />
						<input type="submit" name="delete" value="削除" onclick="return confirm('支援を削除します。良いですか？')"　/>
						</form>
						{/if}
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="10">指定された検索条件では一致するデータがありませんでした。</td>
				</tr>
			{/foreach}
			</table>
			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="right">{$navi}</div>
				<div class="end"></div>
			</div>
			<!--
			<div class="center">
				<input type="submit" name="sbm_update" value="一覧を更新する" onClick="return confirm('更新します。削除チェックの場合は、会員を削除します。');">
			</div>
			-->
			</form>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

