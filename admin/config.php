<?php
// サイトタイトル
define("STR_SITE_TITLE", SITE_TITLE.": 運営者管理画面");
define("FOOTER_COPYRIGHT", "&#169; 2015　A-port");

// サイトのルートディレクトリパス
define("ROOT_PATH", realpath(dirname(__FILE__) . "/."));

// Log4PHPの設定ファイル
define("LOG4PHP_CONFIGURATION", "/var/www/aport_admin/admin/log4php.properties");


?>