{include file="head.tpl"}
<script type="text/javascript">
{literal}


{/literal}
</script>
<body>
<div id="wrapper">
	{include file="header.tpl"}
{include file="sidebar.tpl"}
	<div id="main_content">
		<h3>ユーザー</h3>

<h4>{$memArr.name}さんの換金履歴</h4>
{include file="messages.tpl"}

		←<a href="/member/list">ユーザー一覧へ</a>

		{* 検索結果 *}


			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>


			<table class="admins_m clear">
			<tr>
				<th width="20">ポイント数</th>
				<th width="80">金額</th>
				<th width="80">換金依頼日</th>
				<th width="80">支払日</th>
			</tr>
			{foreach from=$arr item="item"}
				<tr>
					<td>{$item.point|number_format}pt</td>
					<td>{$item.point|number_format}円</td>
					<td>{$item.insert_date}</td>
					<td>{if $item.stat==0}未払い{else}{$item.paid_date}{/if}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="4">換金履歴はありません。</td>
				</tr>
			{/foreach}
			</table>


			<div class="paging">
				<div class="left"><b>{$total_cnt}</b>件のデータが見つかりました。</div>
				<div class="left">{$page_navi}</div>
				<!--<div class="end"></div>-->
			</div>

	</div>
</div>
{include file="footer.tpl"}
</body>
</html>

