<?php
class SlideController extends AppRootController {

	/**
	 * コンストラクタ
	 */
	public function __construct() {
		parent::__construct();

	}

	/**
	 * 広告TOP
	 */
	public function displayAction() {
		// HTTPレスポンスヘッダ情報出力
		$this->outHttpResponseHeader();

		$this->setTemplatePath("slide/ad_top.tpl");
		return;

	}
	/**
	 * 一覧表示・検索
	 */
	public function listAction() {

		$dao=new slideDao();
		$commonDao=new CommonDao();
		$exec = $_POST[ "exec" ];

		//メインカテゴリー
		if($exec=="mainup"){

			$targetId = $_POST[ "slide_no" ];//クリックしたID
			$order = $_POST[ "value" ];

			$ret=$dao->getCategoryList(" where a.v_order < " . $order,"limit 1","order by a.v_order desc");
			if($ret){

//				$up=new Employ();
				//選択したIDよりもひとつ小さい番号を一つだけ取得
				//その取得したカテゴリーをクリックした表示番号（$orderにする）

				//$up->setOrder($order);
				//$tmpObj=$ret[0];
				$slide_no=$ret[0][slide_no];

				//$dao->update($up,$tmpObj->getId());

				$commonDao->updateData("mst_slide", "v_order", $order, "slide_no", $slide_no);

				//最初に選択したカテゴリーは一つ上（表示順を一つ上にする）
//				$up=new Employ();
				$changeOrder=$order-1;
//				$tmpObj=$ret[0];

				//$dao->update($up,$targetId);
				$commonDao->updateData("mst_slide", "v_order", $changeOrder, "slide_no", $targetId);
			}

		}
		else if($exec=="maindown"){
			$targetId = $_POST[ "slide_no" ];
			$order = $_POST[ "value" ];

			$ret=$dao->getCategoryList(" where a.v_order > " . $order,"limit 1");
			if($ret){

				//選択したIDよりもひとつ小さい番号を一つだけ取得
				//その取得したカテゴリーをクリックした表示番号（$orderにする）

				//$up->setOrder($order);
				//$tmpObj=$ret[0];
				//$dao->update($up,$tmpObj->getId());

				$slide_no=$ret[0][slide_no];
				$commonDao->updateData("mst_slide", "v_order", $order, "slide_no", $slide_no);


				//最初に選択したカテゴリーは一つ上（表示順を一つ上にする）
				//$up=new Employ();
				//$up->setOrder($order+1);
				//$tmpObj=$ret[0];
				//$dao->update($up,$targetId);

				$changeOrder=$order+1;
				$commonDao->updateData("mst_slide", "v_order", $changeOrder, "slide_no", $targetId);


			}
		}
		else if( $exec == "delete" ){
			$targetId = $_POST[ "slide_no" ];
			$slide=$commonDao->get_data_tbl("mst_slide", "slide_no", $targetId);
			$commonDao->del_Data("mst_slide", "slide_no", $targetId);

			//画像を削除
			//unlink(DIR_IMG_SLIDE.$slide[0][img_name]);

			//ソート順を変更する
			$retArr=$commonDao->get_data_tbl("mst_slide","","","v_order asc");
			$v_order=0;
			foreach($retArr as $item){
				$v_order++;
				$commonDao->updateData("mst_slide", "v_order", $v_order, "slide_no", $item[slide_no]);
			}

			$msg="削除しました。";

		}

		$cateArr=$commonDao->get_data_tbl("mst_slide","",""," v_order asc");


		//親カテゴリーの数を数える
		$pidcount=count($cateArr);

		//親カテゴリーの調整
		$oya=0;
		foreach($cateArr as $key=>$val){

				$oya++;
				//$id->setMainup(1);
				//$id->setMaindown(1);
				$cateArr[$key][mainup]=1;
				$cateArr[$key][maindown]=1;
				if($oya==$pidcount){//ソート最後の親カテゴリ
					//$id->setMaindown(0);//▼を表示しない
					$cateArr[$key][maindown]=0;//▼を表示しない
				}
				if($oya==1){//ソート最初の親カテゴリ
					//$id->setMainup(0);//▲を表示しない
					$cateArr[$key][mainup]=0;;//▲を表示しない
				}

			//リンクプロジェクト名の取得
			$ProjArr=$commonDao->get_data_tbl("sf_project","no",$cateArr[$key][link]);
			$cateArr[$key][project_title]=$ProjArr[0][public_title];
			$cateArr[$key][cover_img]=$ProjArr[0][cover_img];

		}

		$this->view->assign("cateArr", $cateArr);
		$this->view->assign("pidcount", $pidcount);
		$this->view->assign("msg", $msg);

		//言語リスト
		$langArr=CommonArray::$array_lang_p;
		$this->view->assign("langArr", $langArr);

		$this->setTemplatePath("slide/list.tpl");
		return;
	}

	/**
	 * 登録・更新
	 */
	public function editAction() {

		$dao=new slideDao();
		$ProjectDao=new ProjectDAO();
		$commonDao=new CommonDao();
		$exec = $_POST[ "exec" ];

		//スライドNo
		$slide_no=$_REQUEST[slide_no];

		$checkData=CommonChkArray::$SlideCheckData;

		if($_POST[regist]){

			$_SESSION["input_data"]=$_POST;
			$input_data=$_SESSION["input_data"];

			//入力チェック
			$ret=!$this->check($input_data,$checkData);

			if (count($this->getMessages()) >0) {

				foreach($this->getMessages() as $msg){
					$result_messages[$msg->getMessageLevel()]=$msg->getMessageBody();
				}

				$this->view->assign("result_messages", $result_messages);
			}
			else {
				//フォーム入力用データ
				foreach($checkData[dbstring] as $key=>$val){
					$dkey[]=$key;
					$dval[]=$_SESSION["input_data"][$key];
				}

				//スライド登録
				if(!$slide_no){

				//新規登録

					//ソート順は一番後ろにする
					$ordTmp=$commonDao->get_data_tbl("mst_slide","","","v_order desc" ,1);
					$v_order=$ordTmp[0][v_order]+1;

					$dkey[]="v_order";
					$dval[]=$v_order;

					$dkey[]="insert_date";
					$dval[]=date("Y-m-d H:i:s");
					$dkey[]="update_date";
					$dval[]=date("Y-m-d H:i:s");

					$ret=$commonDao->InsertItemData("mst_slide",$dkey,$dval);
				}
				else{
					//変更処理
					$ret=$commonDao->updateData("mst_slide", $dkey, $dval, "slide_no", $slide_no);
					//リンクプロジェクト名の取得
					$ProjArr=$commonDao->get_data_tbl("sf_project","no",$input_data[link]);
					$project_title=$ProjArr[0][public_title];
					$edit_flg=1;
				}

				$upErrFlg="1";
				if($_SESSION["input_data"]) unset($_SESSION["input_data"]);
			}
		}
		else if($_REQUEST[slide_no]){

			if($_SESSION["input_data"]) unset($_SESSION["input_data"]);

			//データ取得
			$ret=$commonDao->get_data_tbl("mst_slide","slide_no",$slide_no);
			$input_data=$ret[0];

			//リンクプロジェクト名の取得
			$ProjArr=$commonDao->get_data_tbl("sf_project","no",$input_data[link]);
			$project_title=$ProjArr[0][public_title];
			$edit_flg=1;

		}
		else{
			//表示デフォルト
			$input_data[view_flg]=0;
			$input_data[view_start]=date("Y/m/d 00:00:00" );
 			$input_data[view_end]=date("Y/m/d 23:59:59" );


			if($_SESSION["input_data"]) unset($_SESSION["input_data"]);

		}

		//プロジェクトリスト
		$ProjectArr=$ProjectDao->getProjectOrderOptionArr();
		$this->view->assign("ProjectArr", $ProjectArr);


		//言語リスト
		$langArr=CommonArray::$array_lang_p;
		$this->view->assign("langArr", $langArr);

		$this->view->assign("upErrFlg", $upErrFlg);
		$this->view->assign("input_data", $input_data);
		$this->view->assign("edit_flg", $edit_flg);
		$this->view->assign("project_title", $project_title);

		$this->setTemplatePath("slide/edit.tpl");
		return;

	}
}
?>


